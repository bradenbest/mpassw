default:
	$(MAKE) -C src

list:
	@echo "default  make -C src"
	@echo "list     show this list"
	@echo "clean    remove generated files"
	@echo "manpages (requires ronn) generate manpages"

clean:
	rm -f man/*.1 man/*.7 man/*.html
	$(MAKE) -C src clean

manpages: man/*.ronn
	ronn $^

.PHONY: default list clean manpages
