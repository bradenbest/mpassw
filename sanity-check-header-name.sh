#!/bin/sh
# check header name against GNU and mingw preprocessors to make sure there isn't a library path match for it
# I made this after mingw forced me to rename io.h

gnupp=cpp
mingwpp=x86_64-w64-mingw32-cpp
headername="$1"
includestr=$(printf '%binclude %b%b%b' '#' '<' $headername '>')
gnufound=0
mingwfound=0

# get out of src to prevent matches with local files
cd ..

echo "Searching for header $headername..."

if echo $includestr | $gnupp 2> /dev/null > /dev/null; then
    gnufound=1
    echo "FOUND HEADER. DO NOT USE."
    echo "preprocessor: GCC ($(which $gnupp))"
fi
if echo $includestr | $mingwpp 2> /dev/null > /dev/null; then
    mingwfound=1
    echo "FOUND HEADER. DO NOT USE."
    echo "preprocessor: MINGW ($(which $mingwpp))"
fi

if test $gnufound -eq 0 && test $mingwfound -eq 0; then
    echo "Not found. This header name can be used."
fi

echo
