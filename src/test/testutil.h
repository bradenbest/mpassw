/* Usage:
 * TESTLIST(arrayname, TEST(fn, description), TEST(...), ...);
 * runtests(arrayname, length);
 *
 * fn: int(void)
 */

#ifndef TESTUTIL_H
#define TESTUTIL_H

#include <stdio.h>
#include <string.h>

#ifndef TEST
#define TEST(fn, desc) \
    { (fn), (desc), 0 }
#else
#error "A macro named TEST already exists"
#endif

#ifndef TESTMAIN
#define TESTMAIN(...) \
    int main(void){ \
        struct testutil__test testlist[] = { __VA_ARGS__ }; \
        return testutil__runtests(testlist, sizeof testlist / sizeof *testlist) ? 0 : 1; \
    }
#else
#error "A macro named TESTMAIN already exists"
#endif

typedef int (*testutil__testfn)(void);

struct testutil__test {
    testutil__testfn fn;
    char const *desc;
    int passed;
};

int testutil__runtests (struct testutil__test *tests, size_t total);

#endif // TESTUTIL_H
