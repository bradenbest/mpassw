#include <stdio.h>

#include "util/util.h"
#include "common.h"

#include "ui/ui.h"

void
ignore_unused_functions(void)
{
    IGNORE_FUNCTION(hexdump);
}

int
test_sanity_uistate_flag(void)
{
    if(UIFLAG_WARN_NARPAS == 0)
        goto exit_fail;

    if(UIFLAG_COMPACT == 0)
        goto exit_fail;

    if(UIFLAG_TUTORIAL == 0)
        goto exit_fail;

    return 1;

exit_fail:
    {
        LOGERR("Sanity check failed. One or more flag constants are zero.\n");
        return 0;
    }
}

int
test_setflag(void)
{
    if(ui_getflags() & UIFLAG_TUTORIAL)
        goto exit_canttest;

    ui_setflag(UIFLAG_TUTORIAL);
    return !!(ui_getflags() & UIFLAG_TUTORIAL);

exit_canttest:
    {
        LOGERR("UIFLAG_TUTORIAL is already set. Cannot sanely test.\n");
        return 0;
    }
}

int
test_unsetflag(void)
{
    if(!(ui_getflags() & UIFLAG_TUTORIAL))
        goto exit_canttest;

    ui_unsetflag(UIFLAG_TUTORIAL);
    return !(ui_getflags() & UIFLAG_TUTORIAL);

exit_canttest:
    {
        LOGERR("UIFLAG_TUTORIAL is not set. Cannot sanely test. Make sure this test comes *after* test_setflag.\n");
        return 0;
    }
}

int
test_toggleflag(void)
{
    int value;
    int valueafter;
    int valuefinal;

    value = !!(ui_getflags() & UIFLAG_TUTORIAL);
    ui_toggleflag(UIFLAG_TUTORIAL);
    valueafter = !!(ui_getflags() & UIFLAG_TUTORIAL);
    ui_toggleflag(UIFLAG_TUTORIAL);
    valuefinal = !!(ui_getflags() & UIFLAG_TUTORIAL);

    return (value == valuefinal && value != valueafter);
}

#include "testutil.h"

TESTMAIN(
    TEST(test_sanity_uistate_flag, "Sanity check flags"),
    TEST(test_setflag,             "Set a flag"),
    TEST(test_unsetflag,           "Unset a flag"),
    TEST(test_toggleflag,          "Toggle a flag"),
)
