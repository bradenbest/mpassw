#include <string.h>

#include "util/types.h"

static char const *
hexdump(u8 const *data)
{
    static char buffer[45];
    char snpbuf[6];

    for(int i = 0; i < 9; ++i){
        snprintf(snpbuf, 6, "%02x%02x ", data[2 * i], data[2 * i + 1]);
        memcpy(buffer + 5 * i, snpbuf, 5);
    }

    return buffer;
}
