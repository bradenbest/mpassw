#include <stdio.h>
#include <string.h>

#include "data/data.c"
#include "common.h"

void
datareset(void)
{
    data_from_password((u8 *)"000000000000000000000000");
}

void
datacopy(u8 *buffer)
{
    memcpy(buffer, data_get_bytes(), 18);
}

int
test_set_game_age(void)
{
    u8 before[18];
    u8 interim[18];
    u8 after[18];
    u32 age;

    datareset();
    datacopy(before);
    data_set_game_age(100000);

    if((age = data_get_game_age()) != 100000)
        goto exit_fail_valuesanitycheck;

    datacopy(interim);
    data_set_game_age(0);
    datacopy(after);

    for(int i = 0; i < 18; ++i){
        if(before[i] != after[i])
            goto exit_fail_unabletorestore;

        if(before[i] != interim[i] && i < 12 && i > 15)
            goto exit_fail_notconstrained;
    }

    return 1;

exit_fail_unabletorestore:
    {
        puts("FAIL: before and after do not match");
        printf("Before: %.45s\n", hexdump(before));
        printf("After:  %.45s\n", hexdump(after));
        return 0;
    }

exit_fail_notconstrained:
    {
        puts("FAIL: changes not constrained to age region bytes 12..15");
        printf("Before:  %.45s\n", hexdump(before));
        printf("Interim: %.45s\n", hexdump(interim));
        return 0;
    }

exit_fail_valuesanitycheck:
    {
        puts("FAIL: value sanity check failed, age != 100000");
        printf("age: %u\n", age);
        return 0;
    }
}

int
test_sets(void)
{
    static u8 expectedafter[18] = {
        0x80, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x03, 0x00, 0xc0, 0xa0,
        0x86, 0x01, 0x00, 0x00, 0x00, 0x00
    };
    u8 bak_default[18];
    u8 bak_age[18];
    u8 bak_loc[18];
    u8 bak_ammo[18];
    u8 bak_setbit[18];

    datareset();
    datacopy(bak_default);

    data_set_game_age(100000);
    datacopy(bak_age);

    data_set_location(DATALOC_TOURIAN);
    datacopy(bak_loc);

    data_set_missile_ammo(192);
    datacopy(bak_ammo);

    data_set_bit(0, 7);
    datacopy(bak_setbit);

    if(memcmp(expectedafter, data_get_bytes(), 18) == 0)
        return 1;

    puts("FAIL: data does not match expected value");
    printf("Default: %s\n", hexdump(bak_default));
    printf("Time=100000: %s\n", hexdump(bak_age));
    printf("loc=tourian: %s\n", hexdump(bak_loc));
    printf("ammo=192: %s\n", hexdump(bak_ammo));
    printf("0:7 = 1: %s\n", hexdump(bak_setbit));
    return 0;
}

int
test_gets(void)
{
    u32 age = data_get_game_age();
    u8 loc = data_get_location();
    u8 ammo = data_get_missile_ammo();
    u8 bit = data_get_bit(0, 7);

    if(age == 100000 && loc == DATALOC_TOURIAN && ammo == 192 && bit == 1)
        return 1;

    puts("FAIL: obtains values don't match expected values");
    printf("age: %u\n", age);
    printf("loc: %u (%s)\n", loc, data_get_location_str());
    printf("ammo: %u\n", ammo);
    printf("bit 0:7: %u\n", bit);
    return 0;
}

int
test_password_interop(u8 shift)
{
    u8 const *pw;
    u8 before[18];
    u8 after[18];

    printf("Generating password from data (key = %u)\n", shift);
    data_set_shiftkey(shift);
    datacopy(before);
    pw = data_to_password();
    printf("Password: %.6s %.6s %.6s %.6s\n", pw, pw + 6, pw + 12, pw + 18);
    puts("Decoding password back into data");
    data_from_password(pw);
    datacopy(after);
    printf("Result: %s\n", data_hexdump());

    for(int i = 0; i < 17; ++i)
        if(before[i] != after[i])
            goto exit_fail_unequal;

    return 1;

exit_fail_unequal:
    {
        puts("FAIL: before and after do not match. Data or password corrupted");
        return 0;
    }
}

int
test_password_interop_main(void)
{
    int testvalues[] = { 0, 42, 127, 255 };
    int passed = 1;

    for(int i = 0; i < 4; ++i)
        if(!test_password_interop(testvalues[i]))
            passed = 0;

    return passed;
}

#include "testutil.h"

TESTMAIN(
    TEST(test_set_game_age,          "Set game age without affecting other data"),
    TEST(test_sets,                  "Set various values"),
    TEST(test_gets,                  "Get various values"),
    TEST(test_password_interop_main, "Test password/data interop"),
);
