#include <stdio.h>
#include <string.h>
#include <time.h>

#include "common.h"
#include "data/codec.h"

static u8 data[] = {
    0xcc, 0xcc, 0x66, 0x66,
    0x33, 0x33, 0xc6, 0x3f,
    0x88, 0x88, 0x44, 0x44,
    0x22, 0x22, 0x84, 0x2f,
};

int
test_decode_encode(void)
{
    u8 ridley[24] = "ENGAGERIDLEYMOTHERFUCKER";
    u8 const *decoded = codec_base64decode(ridley);
    u8 const *encodedagain;

    printf("input:   %.24s\n", ridley);
    printf("decoded: %.45s\n", hexdump(decoded));
    encodedagain = codec_base64encode(decoded);
    printf("encoded: %.24s (expect ENGAGERIDLEYMOTHERFUCKER)\n", encodedagain);

    if(memcmp(encodedagain, "ENGAGERIDLEYMOTHERFUCKER", 24) == 0)
        return 1;

    return 0;
}

int
test_space(void)
{
    int success = 1;
    char *tests[] = {
        "Test password-----00000 ",
        "Test password-----00003-",
        "Test password-----00 000",
        "Test password-----00-000",
        " 2341 3412 4123 --------",
        NULL
    };
    char *expects[] = {
        "Test-password-----00003-",
        "Test-password-----00003-",
        "Test-password-----00-000",
        "Test-password-----00-000",
        "-2343-3413-4123---------",
    };

    for(int i = 0; tests[i] != NULL; ++i){
        u8 const *decoded = codec_base64decode( (u8 const *)(tests[i]) );
        u8 const *encodedagain = codec_base64encode(decoded);
        int result = memcmp(encodedagain, expects[i], 24) == 0;
        printf("test:    %.24s\n", tests[i]);
        printf("decoded: %.45s\n", hexdump(decoded));
        printf("encoded: %.24s\n", encodedagain);
        printf("result:  %c\n", "FP"[result]);

        if(!result)
            success = 0;
    }

    return success;
}

int
test_crypt(void)
{
    u8 shift = 127;
    u8 data_pre_crypt[18];
    u8 data_post_crypt[18];

    printf("Shift:   %u\n", shift);
    printf("Before:  %s\n", hexdump(data));
    data[16] = shift;
    memcpy(data_pre_crypt, data, 18);
    codec_shiftencrypt(data);
    printf("After:   %s\n", hexdump(data));
    codec_shiftdecrypt(data);
    memcpy(data_post_crypt, data, 18);
    printf("Decrypt: %s\n", hexdump(data));

    if(memcmp(data_pre_crypt, data_post_crypt, 18) == 0)
        return 1;

    return 0;
}

int
test_benchmark(void)
{
    struct timespec start;
    struct timespec stop;

    puts("Benchmarking 1,000,000 of encrypt(127)...");
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);
    data[16] = 127;

    for(int i = 0; i < 1000000; ++i)
        codec_shiftencrypt(data);

    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &stop);
    printf("Completed in %.6lf ms\n", (double)(stop.tv_nsec - start.tv_nsec) / 1000000);

    puts("Benchmarking 1 of encrypt(127)...");
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);
    codec_shiftencrypt(data);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &stop);
    printf("Completed in %lu nanoseconds\n", stop.tv_nsec - start.tv_nsec);

    puts("Benchmarking 1 of encrypt(1)...");
    data[16] = 1;
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);
    codec_shiftencrypt(data);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &stop);
    printf("Completed in %lu nanoseconds\n", stop.tv_nsec - start.tv_nsec);

    puts("Benchmarking 1 of encrypt(8)...");
    data[16] = 8;
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);
    codec_shiftencrypt(data);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &stop);
    printf("Completed in %lu nanoseconds\n", stop.tv_nsec - start.tv_nsec);

    return 1;
}

#include "testutil.h"

TESTMAIN(
    TEST(test_decode_encode, "base64 decode/encode"),
    TEST(test_space,         "base64 interpreting space in password"),
    TEST(test_crypt,         "shift encrypt"),
    TEST(test_benchmark,     "shift encrypt performance benchmark (always pass)"),
);
