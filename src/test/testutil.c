#include <stdio.h>

#include "testutil.h"

#define MIN(a, b) \
    ( (a) < (b) ? (a) : (b) )

#define TESTUTIL__TRUNCATE_LEN 63

static char const *testutil__failpassstr[] = { "FAIL", "PASS" };
static char const *testutil__bordertextsrc =
    "================"
    "================"
    "================"
    "================"
    "================"
    "================"
    "================"
    "================";

static char const *
testutil__truncate(char const *text, size_t len)
{
    static char buffer[TESTUTIL__TRUNCATE_LEN + 1];
    size_t elen = MIN(len, TESTUTIL__TRUNCATE_LEN);

    if(len == elen)
        return text;

    memcpy(buffer, text, TESTUTIL__TRUNCATE_LEN - 3);
    memcpy(buffer + TESTUTIL__TRUNCATE_LEN - 3, "...", 4);
    return buffer;
}

static void
testutil__sayheader(char const *desc)
{
    size_t len = strlen(desc);
    size_t effectivelen = MIN(len, TESTUTIL__TRUNCATE_LEN);

    fprintf(stdout, "======%.*s\n", (int)effectivelen, testutil__bordertextsrc);
    fprintf(stdout, "TEST: %.*s\n", (int)effectivelen, testutil__truncate(desc, len));
    fprintf(stdout, "======%.*s\n", (int)effectivelen, testutil__bordertextsrc);
}

int
testutil__runtests(struct testutil__test *tests, size_t total)
{
    int passed = 0;

    for(int i = 0; i < (int)total; ++i){
        testutil__sayheader(tests[i].desc);

        if(tests[i].fn()){
            ++passed;
            tests[i].passed = 1;
        }
    }

    fputs("", stderr);
    fprintf(stderr, "Test Summary: %u of %lu passed\n", passed, total);

    for(int i = 0; i < (int)total; ++i)
        fprintf(stderr, "[%s] %s\n", testutil__failpassstr[!!(tests[i].passed)], tests[i].desc);

    return passed == (int)total;
}
