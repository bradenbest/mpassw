// This is not a unit test, so it's not included in the default make target
// This is for manually reviewing the menu texts. All of them.
// To use this, a few things have to be modified:
// 1. in localization/, the headers need to have bindings added for the language being tested.
//    for example, if you're adding a spanish translation, then you'd add an ifdef check for
//    LANGUAGE_ES and a directory localization/es/ for the translated files
//
// 2. in test/config.mk, set the language to said macro
// 3. run make -B tlocalization here, in test/
//
// Remember to set the language back to english if you're going to commit changes

#include <stdio.h>

#include "menu/entryfn.h"
#include "menu/getterfn.h"
#include "menu/handlerfn.h"
#include "menu/entrydata.h"
#include "menu/getterdata.h"
#include "menu/handlerdata.h"
#include "menu/menu.h"
#include "ui/ui.h"
#include "util/util.h"

int
main ( void )
{
    fputs("Executing all menu pages.\n", stderr);
    fputs("If you see a waitcontinue prompt, hit ctrl+d or spam enter.\n", stderr);
    ui_waitcontinue();

#ifdef TLOCAL_USE_COMPACT
    ui_setflag(UIFLAG_COMPACT);
#endif

    for (int pageid = 0; pageid < MENUPAGE_END; ++pageid)
        {
            struct menu_page *selmenu = menu_get_page(pageid);

            selmenu->flags |= MPFLAG_NOCLEAR;
            ui_exec_menupage(selmenu);
        }

    puts("");
}
