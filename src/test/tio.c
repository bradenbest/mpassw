#include <stdio.h>
#include <string.h>

#include "ui/ui.h"

int
test_ui_print(void)
{
    puts("Unexpected gaps or lines \"riding\" other lines is a fail.");
    puts("");

    ui_puts("ui_puts\n");
    ui_qputs("ui_qputs (default)\n");

    ui_setflag(UIFLAG_INTERACTIVE);
    ui_qputs("ui_qputs (quiet mode explicit off)\n");

    ui_unsetflag(UIFLAG_INTERACTIVE);
    ui_puts("ui_qputs (quiet mode on)\n");
    ui_qputs("Failed.\n");

    ui_setflag(UIFLAG_INTERACTIVE);
    return 1;
}

int
test_ui_getline_with_null_buf(void)
{
    size_t nread = ui_getline(NULL, 0);

    if(nread == 0)
        return 1;

    puts("FAIL: non-zero return value with NULL buffer (undefined behavior highly likely)");
    return 0;
}

int
test_ui_getline_normal(void)
{
    size_t nread;
    char buffer[5];

    nread = ui_getline(buffer, 5);

    if(nread == 5 && memcmp(buffer, "12345", 5) == 0)
        return 1;

    puts("FAIL: buffer contents do not match expected values");
    return 0;
}

#define test_ui_getline_overflow test_ui_getline_normal

int
test_ui_getline_empty(void)
{
    size_t nread;
    char buffer[5];

    nread = ui_getline(buffer, 5);

    if(nread == 0)
        return 1;

    puts("FAIL: buffer is not empty");
    return 0;
}

int
test_ui_getline_exit_eof(void)
{
    ui_getline(NULL, 0);

    if(feof(stdin))
        return 1;

    puts("FAIL: EOF not detected");
    return 0;
}

#include "testutil.h"

TESTMAIN(
    TEST(test_ui_print,                 "ui_print variants (always pass) (requires human verification)"),
    TEST(test_ui_getline_with_null_buf, "ui_getline with NULL buffer"),
    TEST(test_ui_getline_overflow,      "ui_getline with excess characters"),
    TEST(test_ui_getline_normal,        "ui_getline normal operation"),
    TEST(test_ui_getline_empty,         "ui_getline with empty line"),
    TEST(test_ui_getline_exit_eof,      "ui_getline with EOF"),
);
