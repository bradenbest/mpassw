OBJ = ../version.o \
      ../data/codec.o \
      ../data/data.o \
      ../data/bitfieldmap.o \
      ../data/bitfieldrange.o \
      ../data/gameagetime.o \
      ../menu/menu.o \
      ../menu/entryfn.o \
      ../menu/entrydata.o \
      ../menu/getterfn.o \
      ../menu/getterdata.o \
      ../menu/handlerfn.o \
      ../menu/handlerdata.o \
      ../menu/valuefn.o \
      ../ui/arg.o \
      ../ui/ui.o \
      ../util/util.o \
      ../util/putsvfmt.o \

FEATURES = \
    -DFEATURE_DEBUG=1 \
    -DFEATURE_TUTORIAL=1 \
    -DFEATURE_NARPAS=1 \
    -DFEATURE_FANCYPROMPT=1 \
    -DFEATURE_COMPACT=1 \
    -DFEATURE_VERBOSEMENU=1 \
    -DFEATURE_ARGS=1 \
    -DFEATURE_COLOR=1 \

LANGUAGE = -DLANGUAGE_EN
INCLUDES = -I.. -I../include

BIN = tcodec tdata tio tui tbench_ui_print
CFLAGS = -Wall -Wextra -g $(INCLUDES) $(FEATURES) $(LANGUAGE)
