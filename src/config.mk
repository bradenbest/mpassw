VERSION = -DVERSION="1,14,1,0"

# FEATURE_DEBUG        extra checks and --debug-size
# FEATURE_TUTORIAL     tutorial page and --tutorial. If disabled, tutorial isn't compiled
# FEATURE_NARPAS       narpas warning. If disabled, narpas warning is permanently off
# FEATURE_FANCYPROMPT  fancy prompt. If disabled, prompt is just a '>'
# FEATURE_COMPACT      compact and --compact. If disabled, none of the related data is compiled in
# FEATURE_VERBOSEMENU  default menu view. If disabled, normal menu text is less wordy
# FEATURE_ARGS         arg parsing. If disabled, arguments will do absolutely nothing
# FEATURE_COLOR        ANSI colors. If disabled, all text is uncolored, does not affect ui_clearscreen()

FEATURES = \
    -DFEATURE_DEBUG=0 \
    -DFEATURE_TUTORIAL=1 \
    -DFEATURE_NARPAS=1 \
    -DFEATURE_FANCYPROMPT=1 \
    -DFEATURE_COMPACT=0 \
    -DFEATURE_VERBOSEMENU=1 \
    -DFEATURE_ARGS=1 \
    -DFEATURE_COLOR=0 \

LANGUAGE = -DLANGUAGE_EN

INCLUDES = -I. -Iinclude

OBJ = mpassw.o \
      version.o \
      data/codec.o \
      data/data.o \
      data/bitfieldmap.o \
      data/bitfieldrange.o \
      data/gameagetime.o \
      menu/menu.o \
      menu/entryfn.o \
      menu/entrydata.o \
      menu/getterfn.o \
      menu/getterdata.o \
      menu/handlerfn.o \
      menu/handlerdata.o \
      menu/valuefn.o \
      ui/arg.o \
      ui/ui.o \
      util/util.o \
      util/putsvfmt.o \

MINGWCC = x86_64-w64-mingw32-gcc
CC = gcc
CFLAGS = -Wall -Wextra -pedantic -std=c99 $(INCLUDES) $(FEATURES) $(LANGUAGE)
DEBUG_CFLAGS = -g -UFEATURE_DEBUG -DFEATURE_DEBUG=1 -DDEBUG -D_GLIBC_DEBUG -O1 -fno-omit-frame-pointer -fsanitize=address
DEBUG_LDFLAGS = -fsanitize=address
# sanitize options: address, leak, undefined
