#include <stdio.h>
#include <string.h>

#include "data/data.h"

#define VERSION "1.0"

static char const *usage_text[] = {
    "mpasswfix v" VERSION " - re-encode a broken password",
    "usage: mpasswfix <password>",
    "",
    "This software is public domain and is distributed without warranty.",
    NULL
};

static void
usage(void)
{
    for(int i = 0; usage_text[i] != NULL; ++i)
        puts(usage_text[i]);
}

int
main(int argc, char **argv)
{
    u8 buffer[24] = { 0 };
    u8 const *new_pass;

    if(argc < 2){
        usage();
        return 1;
    }

    memcpy(buffer, argv[1], strlen(argv[1]));
    data_from_password(buffer);
    new_pass = data_to_password();
    printf("%.6s %.6s\n%.6s %.6s\n", new_pass, new_pass + 6, new_pass + 12, new_pass + 18);
    return 0;
}
