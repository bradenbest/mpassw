static u8 gamedata[18];

static char const *dataloc_str[DATALOC_END] = {
    "Brinstar",
    "Norfair",
    "Kraid's Hideout",
    "Tourian",
    "Ridley's Hideout",
    "Invalid - 05 Reset",
    "Invalid - 06 Reset",
    "Invalid - 07 Reset",
    "Invalid - 08 Reset; ACE potential",
    "Invalid - 09 Reset",
    "Invalid - 0A Reset",
    "Invalid - 0B Crash",
    "Invalid - 0C Reset; ACE potential",
    "Invalid - 0D Reset",
    "Invalid - 0E Reset",
    "Invalid - 0F Crash; ACE potential",
};

#if FEATURE_VERBOSEMENU
static char const *dataend_str[DATA_ENDING_END] = {
    "Slowest (Shame Gesture)",
    "Slow (Full Armor)",
    "Average (Helmet Removed)",
    "Fast (Swimsuit)",
    "Fastest (Bikini)"
};
#endif // FEATURE_VERBOSEMENU

/* name:   reverse_bytes_u32
 * @value: value to reverse
 * return: reversed value
 * desc:   reverses the bytes in value for big-endian support. Under the
 *         best conditions, the compiler will inline this and optimize
 *         it to just the bswap instruction, while also optimizing away
 *         the endianness check and resulting branch.
 */
static inline u32
reverse_bytes_u32 ( u32 value )
{
    return
        ( (value & 0xff000000u) >> 24 ) |
        ( (value & 0x00ff0000u) >> 8  ) |
        ( (value & 0x0000ff00u) << 8  ) |
        ( (value & 0x000000ffu) << 24 ) ;
}
