static char const *alphabet =
    "0123456789ABC"
    "DEFGHIJKLMNOP"
    "QRSTUVWXYZabc"
    "defghijklmnop"
    "qrstuvwxyz?-"
    ;

/* name:     get_offset
 * @b64char: character from `alphabet`, NUL or space
 * return:   0..63 or OFF_INVALID or OFF_SPACE
 * context:  used in base64 decoding
 * desc:     does a simple array lookup, returns OFF_SPACE for space,
 *           OFF_INVALID for no match, and also maps NUL to 0 for
 *           partial passwords
 */
static inline u8
get_offset ( u8 b64char )
{
    char const *match;

    if (b64char == '\0')
        return 0;

    if (b64char == ' ')
        return OFF_SPACE;

    match = strchr(alphabet, b64char);

    if (match == NULL)
        return OFF_INVALID;

    return match - alphabet;
}

/* name:     encode_chunk
 * @dataptr: pointer to 3 bytes of data
 * return:   static buffer[4]
 * desc:     encodes a 3-byte chunk to its 4-byte base64 representation
 *
 *           illustration:
 *           XXXXXXXX XXXXXXXX XXXXXXXX
 *           AAAAAABB BBBBCCCC CCDDDDDD
 *           00AAAAAA 00BBBBBB 00CCCCCC 00DDDDDD
 *           A = [0]&FC>>2
 *           B = [0]&03<<4 | [1]&F0>>4
 *           C = [1]&0F<<2 | [2]&C0>>6
 *           D = [2]&3F
 */
static inline u8 const *
encode_chunk ( u8 const * dataptr )
{
    static u8 buf4[4];

    buf4[0] = alphabet[ (dataptr[0] & 0xFC) >> 2 ];
    buf4[1] = alphabet[ ((dataptr[0] & 0x03) << 4) | ((dataptr[1] & 0xF0) >> 4) ];
    buf4[2] = alphabet[ ((dataptr[1] & 0x0F) << 2) | ((dataptr[2] & 0xC0) >> 6) ];
    buf4[3] = alphabet[ dataptr[2] & 0x3F ];

    return buf4;
}

/* name:    decode_chunk
 * @b64ptr: pointer to 4-byte base64 chunk
 * return:  static buffer[3] or NULL
 * desc:    decodes 4-byte base64 chunk to 3-byte data chunk or returns
 *          NULL if unable to decode
 *
 *          illustration:
 *          00XXXXXX 00XXXXXX 00XXXXXX 00XXXXXX
 *            AAAAAA   AABBBB   BBBBCC   CCCCCC
 *          AAAAAAAA BBBBBBBB CCCCCCCC
 *          A = [0] << 2 | [1] >> 4
 *          B = [1] << 4 | [2] >> 2
 *          C = [2] << 6 | [3]
 *
 * 1. This is actually how spaces are supposed to be handled. Most
 *    password generators incorrectly OR the previous value with 3
 *    across the whole password string when actually this is supposed to
 *    be done per-chunk, i.e. a space in the first character does not
 *    influence the character before it.
 */
static inline u8 const *
decode_chunk ( u8 const * b64ptr )
{
    static u8 buf3[3];
    u8 b64offs[4];

    for (int i = 0; i < 4; ++i)
        {
            b64offs[i] = get_offset(b64ptr[i]);

            if (b64offs[i] == OFF_INVALID)
                return NULL;

            if (b64offs[i] == OFF_SPACE)
                {
                    b64offs[i] = 0x3F;

                    if(i > 0) // 1
                        b64offs[i - 1] |= 3;
                }
        }

    buf3[0] = b64offs[0] << 2 | b64offs[1] >> 4;
    buf3[1] = b64offs[1] << 4 | b64offs[2] >> 2;
    buf3[2] = b64offs[2] << 6 | b64offs[3];

    return buf3;
}

/* name:    shift_right
 * @data:   pointer to 16-byte data
 * context: mutates argument
 * desc:    shifts 16-byte data one bit to the right and wraps the
 *          rightmost bit over to the left side. Because this is done
 *          over 16 bytes of data, this is done in a loop.
 *
 *          Before:
 *          101101... (116 bits) ... 110101
 *          After:
 *          110110... (116 bits) ... 011010
 */
static inline void
shift_right ( u8 * data )
{
    u8 carrybit = data[15] & 1;

    for (int i = 0; i < 16; ++i)
        {
            u8 lowbit = data[i] & 1;

            data[i] >>= 1;
            data[i] |= (carrybit << 7);
            carrybit = lowbit;
        }
}
