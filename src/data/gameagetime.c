#include <stdio.h>

#include "data/data.h"

#define DATA_GAMEAGETIME_C
#include "data/gameagetime.h"

#if FEATURE_VERBOSEMENU
/* name:     gameagetime_settime
 * @gt:      gameagetime structure
 * @ticks:   tick value decoded from game age (see data_get_game_ticks)
 * @ticklen: tick length in real time (256 / TV refresh rate)
 * context:  mutates argument
 * desc:     encodes the time (in ticks) into a gameagetime structure
 *           this is one of the few places where a u64 is required, as
 *           without it, integer overflow will corrupt the result.
 */
void
gameagetime_settime (
    struct gameagetime * gt      ,
    u32                  ticks   ,
    double               ticklen )
{
    gt->time = ticklen * (double)ticks;
    gt->hours = gt->time / 3600;
    gt->minutes = ((u64)(gt->time) % 3600) / 60;
    gt->seconds = ((u64)(gt->time) % 60) + (gt->time - (u64)(gt->time));
}

/* name:           gameagetime_str
 * @tvrefreshrate: The TV refresh rate, which differs between NTSC
 *                 (60Hz) and PAL (50Hz)
 * return:         time representation as a string
 * context:        returns static buffer
 * desc:           returns a string representation of the current tick
 *                 amount from game data
 */
char const *
gameagetime_str ( double tvrefreshrate )
{
    static char buffer[64];
    struct gameagetime gt;

    gameagetime_settime(&gt, data_get_game_ticks(), 256.0 / tvrefreshrate);
    snprintf(buffer, 64, "%uh %02u'%06.3f\"", gt.hours, gt.minutes, gt.seconds);
    return buffer;
}
#endif // FEATURE_VERBOSEMENU
