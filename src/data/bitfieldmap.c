#include "data/data.h"

#define DATA_BITFIELDMAP_C
#include "data/bitfieldmap.h"

struct bitfieldmap bfmaps_items_taken[] = {
    { 0, 0 }, { 0, 6 }, { 1, 3 }, { 3, 0 }, { 3, 2 },
    BITFIELDMAP_TERMINATOR,
};

struct bitfieldmap bfmaps_items_owned[] = {
    { 9, 4 }, { 9, 0 }, { 9, 5 }, { 9, 1 }, { 9, 3 },
    { 9, 2 }, { 9, 6 }, { 9, 7 }, { 8, 7 },
    BITFIELDMAP_TERMINATOR,
};

struct bitfieldmap bfmaps_missiles[] = {
    { 0, 1 }, { 1, 0 }, { 1, 5 }, { 1, 6 }, { 2, 0 },
    { 2, 1 }, { 2, 2 }, { 2, 3 }, { 2, 4 }, { 2, 5 },
    { 2, 6 }, { 3, 3 }, { 3, 4 }, { 3, 7 }, { 4, 1 },
    { 4, 2 }, { 4, 7 }, { 5, 0 }, { 5, 3 }, { 5, 6 },
    { 6, 1 },
    BITFIELDMAP_TERMINATOR,
};

struct bitfieldmap bfmaps_etanks[] = {
    { 0, 4 }, { 1, 1 }, { 1, 4 }, { 3, 6 }, { 4, 4 },
    { 5, 2 }, { 5, 5 }, { 6, 0 },
    BITFIELDMAP_TERMINATOR,
};

struct bitfieldmap bfmaps_doors[] = {
    { 0, 2 }, { 0, 3 }, { 0, 5 }, { 0, 7 }, { 1, 2 },
    { 1, 7 }, { 2, 7 }, { 3, 1 }, { 3, 5 }, { 4, 0 },
    { 4, 3 }, { 4, 5 }, { 4, 6 }, { 5, 1 }, { 5, 4 },
    { 5, 7 }, { 6, 2 }, { 6, 3 }, { 6, 4 },
    BITFIELDMAP_TERMINATOR,
};

struct bitfieldmap bfmaps_doors_zebetite[] = {
    { 6, 5 }, { 6, 6 }, { 6, 7 }, { 7, 0 }, { 7, 1 },
    BITFIELDMAP_TERMINATOR,
};

struct bitfieldmap bfmaps_doors_stat[] = {
    { 15, 5 }, { 15, 7 },
    BITFIELDMAP_TERMINATOR,
};

struct bitfieldmap bfmaps_bosses[] = {
    { 7,  2 }, { 15, 4 }, { 15, 6 },
    BITFIELDMAP_TERMINATOR,
};

struct bitfieldmap bfmaps_unused[] = {
    { 7,  3 }, { 7,  4 }, { 7,  5 }, { 7,  6 }, { 7,  7 },
    { 8,  4 }, { 8,  5 }, { 8,  6 }, { 15, 0 }, { 15, 1 },
    { 15, 2 }, { 15, 3 },
    BITFIELDMAP_TERMINATOR,
};

/* name:    bitfieldmap_get
 * @selmap: specifies bit
 * return:  1 or 0
 * desc:    gets the value of the specified bit
 */
int
bitfieldmap_get ( struct bitfieldmap const * selmap )
{
    return data_get_bit(selmap->byte, selmap->bit);
}

/* name:    bitfieldmap_set
 * @selmap: specifies bit
 * context: modifies data[]
 * desc:    sets the specified bit
 */
void
bitfieldmap_set ( struct bitfieldmap const * selmap )
{
    data_set_bit(selmap->byte, selmap->bit);
}

/* name:    bitfieldmap_toggle
 * @selmap: specifies bit
 * context: modifies data[]
 * desc:    toggles the specified bit
 */
void
bitfieldmap_toggle ( struct bitfieldmap const * selmap )
{
    data_toggle_bit(selmap->byte, selmap->bit);
}

/* name:     bitfieldmap_total
 * @basemap: array of bfmaps
 * return:   count of set bits
 * desc:     checks every mapped bit in the array and counts how many are
 *           set
 */
int
bitfieldmap_total ( struct bitfieldmap const * basemap )
{
    int total = 0;

    for (struct bitfieldmap const *selmap = basemap; selmap->byte != BITFIELDMAP_END; ++selmap)
        total += data_get_bit(selmap->byte, selmap->bit);

    return total;
}
