#define DATA_BITFIELDRANGE_C
#include "data/bitfieldrange.h"

struct bitfieldrange ranges_items[] = {
    // bfmaps_items_taken
    { 0, 4 },

    // bfmaps_items_owned
    { 0, 8 },
};

struct bitfieldrange ranges_missiles[] = {
    { 0, 20 }, { 0, 1 }, { 2, 13 }, { 14, 17 }, { 18, 20 },
};

struct bitfieldrange ranges_etanks[] = {
    { 0, 7 }, { 0, 2 }, { 3, 3 }, { 4, 5 }, { 6, 7 },
};

struct bitfieldrange ranges_doors[] = {
    // bfmaps_doors
    { 0, 18 },

    // bfmaps_doors_zebetite
    { 0, 4 },

    // bfmaps_doors_stat
    { 0, 1 },
};

struct bitfieldrange ranges_unused[] = {
    { 0, 11 },
};
