/* For managing 18-byte game data */

#include <stdio.h>
#include <string.h>

#include "util/util.h"
#include "data/codec.h"

#define DATA_DATA_C
#include "data/data.h"
#include "data/data.static.c"

/* name:    data_set_bit
 * @byte:   0..15 - specifies index in game data
 * @bit:    0..7 - specifies bit at 1 << bit
 * context: mutates game data, wrapped by the bitfieldmap interface
 * desc:    sets selected bit to 1
 */
void
data_set_bit (
    int byte ,
    int bit  )
{
    gamedata[byte] |= (1 << bit);
}

/* name:    data_get_bit
 * @byte:   0..15 - specifies index in game data
 * @bit:    0..7 - specifies bit at 1 << bit
 * return:  0 or 1
 * context: wrapped by the bitfieldmap interface
 * desc:    gets value of selected bit
 */
int
data_get_bit (
    int byte ,
    int bit  )
{
    return (gamedata[byte] >> bit) & 1;
}

/* name:    data_toggle_bit
 * @byte:   0..15 - specifies index in game data
 * @bit:    0..7 - specifies bit at 1 << bit
 * context: mutates game data, wrapped by the bitfieldmap interface
 * desc:    toggles selected bit
 */
void
data_toggle_bit (
    int byte ,
    int bit  )
{
    gamedata[byte] ^= (1 << bit);
}

/* name:    data_set_bytes
 * @offset: 0..15 - byte offset in game data
 * @src:    source of data to write over game data
 * @size:   size of src
 * context: mutates game data
 * desc:    overwrites game data segment with src
 */
void
data_set_bytes (
    int        offset ,
    u8 const * src    ,
    size_t     size   )
{
    memcpy(gamedata + offset, src, size);
}

/* name:    data_get_bytes
 * return:  game data
 * context: as of v1.11.11, this function is only used in unit tests
 * desc:    returns pointer to game data
 */
u8 const *
data_get_bytes ( void )
{
    return gamedata;
}

/* name:    data_set_missile_ammo
 * @value:  value to set to
 * context: mutates game data
 * desc:    sets the missile ammo byte
 */
void
data_set_missile_ammo ( u8 value )
{
    gamedata[DATABYTE_10_MISSILE_AMMO] = value;
}

/* name:   data_get_missile_ammo
 * return: missile ammo
 * desc:   returns missile ammo
 */
u8
data_get_missile_ammo ( void )
{
    return gamedata[DATABYTE_10_MISSILE_AMMO];
}

/* name:    data_set_game_age
 * @value:  value to set to
 * context: mutates game data
 * desc:    sets game age. If the platform is big endian, it reverses
 *          the bytes first
 */
void
data_set_game_age ( u32 value )
{
    u32 *dataptr = (u32 *)(gamedata + DATABYTE_11_AGE_0);

    if (BIGENDIAN)
        value = reverse_bytes_u32(value);

    memcpy(dataptr, &value, 4);
}

/* name:    data_get_game_age
 * return:  game age
 * context: assumes platform is little-endian
 * desc:    returns game age. If the platform is big endian, it reverses
 *          the bytes first.
 */
u32
data_get_game_age ( void )
{
    u32 game_age = *(u32 *)(gamedata + DATABYTE_11_AGE_0);

    if (BIGENDIAN)
        return reverse_bytes_u32(game_age);

    return game_age;
}

#if FEATURE_VERBOSEMENU
/* name:    data_get_game_ticks
 * return:  game age in ticks (effective tick amount)
 * desc:    returns the game age in ticks, which is distinct from the
 *          raw value as it represents how much real time the game
 *          thinks has passed. E.g. 0x0100 represents 208 ticks, not
 *          256. See note 1.
 *
 * 1. https://www.metroid-database.com/wp-content/uploads/Metroid/MetroidGameEnginePage.txt
 *    search for "update age"
 *
 *    Through reading this disassembly and experimenting with the actual
 *    game, I've found some things:
 *
 *    i. When the low byte overflows at 0xd0, the middle and high bytes
 *    instantly increment. The game does not wait until 0xff.
 *
 *    ii. the fourth byte is not read at any time during the UpdateAge
 *    and ChooseEnding subroutines. It can be any value and the game
 *    won't care. If 0x00ffffcf ticks up, it will overflow to 0x01000000
 *    but the game does not care. The highest byte is not checked, so
 *    you will still get the best ending, meaning a value of 16777217
 *    *effectively* represents 1 tick.
 *
 *    iii. every low byte value from 0xd0..0xff is treated the same as
 *    0xcf and overflows to 0 on the next tick. I definitively confirmed
 *    this by setting the age to 0xffffd1 and doing the "tourian race".
 *    I got the best ending, which means it overflowed quickly.
 *
 * 2. Apparently a low byte value of 0xff is treated as -1, or the same
 *    as 0xcf but with the middle byte decremented, so 0x01cf, 0x01fe
 *    and 0x02ff all refer to the same number of ticks. Joel says this
 *    in his video deconstructing the password system.
 *
 * 3. The highest byte is ignored when calculating the ticks, and the
 *    reason for this is because in my opinion, it makes more sense. The
 *    game also ignores the highest byte when deciding the ending (it
 *    only checks that age+2 is zero and age+1 is within one of various
 *    thresholds--see data_get_ending), so regardless of the high byte
 *    value, the lower bytes decide the ending. Since the highest byte
 *    ticks roughly every 2 years, it has no impact on gameplay and
 *    might as well be ignored, because it's simpler to look at an age
 *    value like 0x01000001 and treat it as 0x00000001 than to give some
 *    huge amount of ticks like 13631489 when it's effectively just 2
 *    ticks as far as the game cares.
 */
u32
data_get_game_ticks ( void )
{
    u32 age = data_get_game_age();
    u32 ticks_high = age & 0xffff00; // 3
    u32 ticks_low = age & 0xff;
    u32 ticks = (ticks_high >> 8) * 0xd0 + MIN(ticks_low, 0xcf); // 1-iii

    if ((age & 0xff) == 0xff)
        return ticks - 0xd0; // 2

    return ticks;
}

/* name:   data_get_ending
 * return: string representation of ending
 * desc:   figures out which ending you would get if you loaded the
 *         password and somehow beat the game without the game age ever
 *         incrementing (whether by freezing the high and middle bytes
 *         with a game genie code or wrong warping to the ending). If
 *         you can find an adversarial age value that doesn't behave as
 *         this function implies, please do file an issue with all the
 *         details.
 *
 * 1. https://www.metroid-database.com/wp-content/uploads/Metroid/MetroidGameEnginePage.txt
 *    search for "choose ending"
 *    These are the thresholds for the second byte that determine which
 *    ending you get. We know the comparison is > and not >= because
 *    5 * 208 * 4.3 seconds = 63 minutes, and the threshold for the best
 *    ending is that you beat it in under roughly an hour. I.e. if the
 *    middle byte is > 4 (63+ minutes), then you get the second best
 *    ending
 *
 * 2. This was very difficult to suss out. I've come to the conclusion
 *    that the comparison is >= (as opposed to >) after
 *    cross-referencing several sources of information.  BCS boils down
 *    to "branch if accumulator >= cmp value". In this case, samus age
 *    byte 2 (age_mid) is the accumulator value.
 *
 *    The relevant disassembly:
 *
 *    LCAFC: lda SamusAge+1
 *    LCAFF: cmp AgeTable-1,y
 *    LCB02: bcs +
 *
 */
char const *
data_get_ending ( void )
{
    static u8 const thresholds[] = { 0x7A, 0x16, 0x0A, 0x04 }; // 1
    u32 age = data_get_game_age();
    u32 age_high = (age & 0xff0000) >> 16;
    u32 age_mid  = (age & 0xff00) >> 8;

    if (age_high != 0x00)
        return dataend_str[DATA_ENDING_WORST];

    for (int i = 0; i < 4; ++i)
        if (age_mid >= thresholds[i]) // 2
            return dataend_str[i];

    return dataend_str[DATA_ENDING_BEST];
}
#endif // FEATURE_VERBOSEMENU

/* name:      data_set_location
 * @location: value to set to (DATALOC_*)
 * context:   mutates game data
 * desc:      sets spawn location
 */
void
data_set_location ( u8 location )
{
    gamedata[DATABYTE_8_LOCATION] = (gamedata[DATABYTE_8_LOCATION] & 0xF0) | (location & 0x0F);
}

/* name:   data_get_location
 * return: location (DATALOC_*)
 * desc:   returns spawn location
 */
u8
data_get_location ( void )
{
    return gamedata[DATABYTE_8_LOCATION] & 0x0F;
}

/* name:   data_get_location_str
 * return: string
 * desc:   returns string representation of spawn location
 */
char const *
data_get_location_str ( void )
{
    return dataloc_str[data_get_location()];
}

/* name:   data_isridleydead
 * return: 1 or 0
 * desc:   if the ridley defeated or ridley statue flags are set in the
 *         game data, 1 is returned, else 0
 */
int
data_isridleydead ( void )
{
    int killed = data_get_bit(15, 4);
    int statueup = data_get_bit(15, 5);

    return killed || statueup;
}

/* name:   data_iskraiddead
 * return: 1 or 0
 * desc:   if the kraid defeated or kraid statue flags are set in the
 *         game data, 1 is returned, else 0
 */
int
data_iskraiddead ( void )
{
    int killed = data_get_bit(15, 6);
    int statueup = data_get_bit(15, 7);

    return killed || statueup;
}

/* name:    data_hexdump
 * return:  string
 * context: returns static buffer
 * desc:    returns a hexdump of the game data, in groups of 2 bytes as
 *          per a typical hexdump utility
 */
char const *
data_hexdump ( void )
{
    static char out[45];
    char snpbuf[6];

    for (int i = 0; i < 9; ++i)
        {
            snprintf(snpbuf, 6, "%02x%02x ", gamedata[2*i], gamedata[2*i + 1]);
            memcpy(out + (5 * i), snpbuf, 5);
        }

    out[44] = '\0';
    return out;
}

/* name:    data_set_shiftkey
 * @shift:  shift key 0..255
 * desc:    sets encryption key for game data
 */
void
data_set_shiftkey ( u8 shift )
{
    gamedata[DATABYTE_16_SHIFT_KEY] = shift;
}

/* name:   data_get_shiftkey
 * return: shift key 0..255
 * desc:   returns encryption key for game data
 */
u8
data_get_shiftkey ( void )
{
    return gamedata[DATABYTE_16_SHIFT_KEY];
}

/* name:   data_validate
 * return: 1 or 0
 * desc:   compares stored checksum with generated checksum and returns
 *         1 if they match else 0
 */
int
data_validate ( void )
{
    return gamedata[DATABYTE_17_CHECKSUM] == codec_checksum(gamedata);
}

/* name:    data_check_narpas
 * return:  1 or 0
 * context: may be implemented as a macro depending on FEATURE_NARPAS
 * desc:    generates a password and checks the first 16 bytes against
 *          "NARPASSWORD00000", returning 1 if it's a match else 0.
 *          The NARPASSWORD00000 password is a special password that
 *          isn't actually parsed by the game, but mpassw does not care
 *          and will treat it like a normal password. This feature is
 *          here to alert the user to this fact.
 */
#if FEATURE_NARPAS
int
data_check_narpas ( void )
{
    u8 const *password = codec_fullencode(gamedata);

    return memcmp(password, "NARPASSWORD00000", 16) == 0;
}
#endif

/* name:      data_from_password
 * @password: a password u8[24]
 * return:    1 or 0
 * context:   mutates game data
 * desc:      wraps codec_fulldecode, returns 1 on successful decode or
 *            0 otherwise.
 */
int
data_from_password ( u8 const *password )
{
    u8 const *data_temp = codec_fulldecode(password);

    if (data_temp == NULL)
        return 0;

    memcpy(gamedata, data_temp, 18);
    return 1;
}

/* name:    data_to_password
 * return:  a password u8[24]
 * context: mutates game data, returns static buffer from
 *          codec_fullencode
 * desc:    wraps codec_fullencode, returns generated password
 */
u8 const *
data_to_password ( void )
{
    return codec_fullencode(gamedata);
}
