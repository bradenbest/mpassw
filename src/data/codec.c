/* The encoding of 18-byte password data to a 24-character password is a
 * 3-step transformation: checksum -> encrypt -> base64. The reverse is
 * a 2-step transformation: unbase64 -> decrypt. The checksum is used
 * for validation.
 *
 * Base64: the base64 encoding used here is different from the standard.
 * he charset is '[0-9A-Za-z]?-', with additional space and blank
 * characters. blanks are represented by '\0' and are treated the same
 * as '0'. They are supported so partial passwords can be entered.
 * spaces are equivalent to '-' (0x3f) but the previous byte in the
 * chunk is also OR'd with 0x03. This means that the a space in the
 * first position of any chunk of 4 is exactly equivalent to '-' while
 * any other position has the ORing behavior. A lot of metroid password
 * tools get this detail wrong and apply the space transformation over
 * the whole password before decoding it.
 *
 * Encryption: the encryption is a big wrapping bit shift. The 17th
 * byte stores the one-byte encryption key, and encryption acts on the
 * first 16 bytes while leaving bytes 17 and 18 alone. The 16 bytes are
 * shifted right N bits where N is the value of the shift key, and
 * anything that falls off the end of the 16th byte wraps around the 1st
 * byte. Decryption is naturally the exact opposite. See
 * codec_shiftencrypt for implementation details
 *
 * Checksum: very simply, the first 17 bytes are added together and
 * stored in the 18th byte.
 */
#include <string.h>

#define DATA_CODEC_C
#include "data/codec.h"
#include "data/codec.static.c"

/* name:    codec_base64encode
 * @data:   18-byte game data (16) shift byte (1) checksum (1)
 * return:  24-byte password (non-terminated)
 * desc:    converts game data to its base64 representation.
 */
u8 const *
codec_base64encode ( u8 const * data )
{
    static u8 out[24];

    for (int i = 0; i < 6; ++i)
        memcpy(out + (i * 4), encode_chunk(data + (i * 3)), 4);

    return out;
}

/* name:   codec_base64decode
 * @b64:   24-byte base64 string
 * return: 18-byte game data or NULL
 * desc:   converts base64 to game data. If it can't be decoded, NULL is
 *         returned.
 */
u8 const *
codec_base64decode ( u8 const * b64 )
{
    static u8 out[18];
    u8 const *temp;

    for (int i = 0; i < 6; ++i)
        {
            temp = decode_chunk(b64 + (i * 4));

            if (temp == NULL)
                return NULL;

            memcpy(out + (i * 3), temp, 3);
        }

    return out;
}

/* name:   codec_checksum
 * @data:  17-byte game data (16) shift byte (1)
 * return: byte 0..255
 * desc:   adds the first 17 bytes together and returns the sum as a u8
 */
u8
codec_checksum ( u8 const *data )
{
    u8 total = 0;

    for (int i = 0; i < 17; ++i)
        total += data[i];

    return total;
}

/* name:    codec_shiftencrypt
 * @data:   17-byte game data (16) shift key (1)
 * context: pulls shift key from the 17th byte, mutates argument
 * desc:    performs shift encryption on the first 16 bytes.
 *          shift_right is an expensive function, so most of this
 *          function optimizes away unnecessary calls to it, from 255
 *          max calls to 7. This is over 16 times faster than just
 *          shifting N times.
 *
 * 1. Since data[] (minus the shift and checksum bytes) is 128 bits, 128
 *    shifts and 0 shifts have the same effect. 255 -> 127
 *
 * 2. 8 shifts is the same as a copying a byte over, two calls to memcpy
 *    cuts out almost every call. 127 -> 7
 *
 *    Example: shift x 176
 *    176 MOD 128 = 48
 *    48 / 8 = 6
 *    data[18]              buffer[16]
 *    AAAAAAAAAABBBBBBSC -> BBBBBB__________
 *    AAAAAAAAAABBBBBBSC -> BBBBBBAAAAAAAAAA
 *    BBBBBBAAAAAAAAAASC <- BBBBBBAAAAAAAAAA
 */
void
codec_shiftencrypt ( u8 * data )
{
    u8 buffer[16];
    u8 shiftkey = data[16] % 128; // 1
    int byteamount;

    if (shiftkey == 0)
        return;

    // 2
    byteamount = shiftkey / 8;
    memcpy(buffer, data + (16 - byteamount), byteamount);
    memcpy(buffer + byteamount, data, (16 - byteamount));

    for (int bitamount = shiftkey % 8; bitamount > 0; --bitamount)
        shift_right(buffer);

    memcpy(data, buffer, 16);
}

/* name:    codec_shiftdecrypt
 * @data:   17-byte game data (16) shift key (1)
 * context: pulls shift key from the 17th byte, mutates argument
 * desc:    sets shift key to its inverse, calls codec_shiftencrypt, and
 *          then resets the key
 */
void
codec_shiftdecrypt ( u8 * data )
{
    u8 original_key = data[16];
    u8 inverse_key = 128 - (original_key % 128);

    data[16] = inverse_key;
    codec_shiftencrypt(data);
    data[16] = original_key;
}

/* name:   codec_fullencode
 * @data:  18-byte game data (16) shift key (1) checksum (1)
 * return: 24-byte password (non-terminated)
 * desc:   encodes data into a password. Encoding is a 3-step process.
 *         1. generate a checksum from the first 17 bytes and store it
 *            at the 18th byte.
 *         2. encrypt the first 16 bytes using the 17th byte as the key
 *         3. base64 encode all 18 bytes to 24-byte password
 */
u8 const *
codec_fullencode ( u8 const * data )
{
    u8 data_cp[18];

    memcpy(data_cp, data, 17);
    data_cp[17] = codec_checksum(data);
    codec_shiftencrypt(data_cp);
    return codec_base64encode(data_cp);
}

/* name:      codec_fulldecode
 * @password: 24-byte password
 * return:    18-byte data or NULL
 * desc:      decodes a password into data. Decoding is a 2-step process.
 *
 *            1. base64 decode to 18-byte encrypted game data (16)
 *               encryption key (1) checksum (1)
 *            2. decrypt bytes 1-16 using byte 17 byte as the key
 *
 *            if the password can't be decoded, NULL is returned.
 */
u8 const *
codec_fulldecode ( u8 const * password )
{
    u8 const *data_temp = codec_base64decode(password);
    static u8 buffer[18];

    if (data_temp == NULL)
        return NULL;

    memcpy(buffer, data_temp, 18);
    codec_shiftdecrypt(buffer);
    return buffer;
}
