#ifndef DATA_DATA_H
#define DATA_DATA_H

#include "util/types.h"

#define BIGENDIAN ( *(uint16_t *)"\0\xff" < 0x100 )

/* B = returns static buffer
 * S = modifies state
 */

/* -S */ void         data_set_bit          ( int byte, int bit );
/* -- */ int          data_get_bit          ( int byte, int bit );
/* -S */ void         data_toggle_bit       ( int byte, int bit );
/* -S */ void         data_set_bytes        ( int offset, u8 const *src, size_t size );
/* B- */ u8 const *   data_get_bytes        ( void );
/* -S */ void         data_set_missile_ammo ( u8 value );
/* -- */ u8           data_get_missile_ammo ( void );
/* -S */ void         data_set_game_age     ( u32 value );
/* -- */ u32          data_get_game_age     ( void );
#if FEATURE_VERBOSEMENU
/* -- */ u32          data_get_game_ticks   ( void );
/* -- */ char const * data_get_ending       ( void );
#endif // FEATURE_VERBOSEMENU
/* -S */ void         data_set_location     ( u8 location );
/* -- */ u8           data_get_location     ( void );
/* -- */ char const * data_get_location_str ( void );
/* -- */ int          data_isridleydead     ( void );
/* -- */ int          data_iskraiddead      ( void );
/* B- */ char const * data_hexdump          ( void );
/* -S */ void         data_set_shiftkey     ( u8 shift );
/* -- */ u8           data_get_shiftkey     ( void );
/* -- */ int          data_validate         ( void );
#if FEATURE_NARPAS
/* -- */ int          data_check_narpas     ( void );
#else
#define data_check_narpas() (0)
#endif
/* -S */ int          data_from_password    ( u8 const *password );
/* BS */ u8 const *   data_to_password      ( void );

#endif // DATA_DATA_H



#ifdef DATA_DATA_C

enum dataloc {
    DATALOC_BRINSTAR,
    DATALOC_NORFAIR,
    DATALOC_KRAID,
    DATALOC_TOURIAN,
    DATALOC_RIDLEY,
    DATALOC_INVALID_1,
    DATALOC_INVALID_2,
    DATALOC_INVALID_3,
    DATALOC_INVALID_4,
    DATALOC_INVALID_5,
    DATALOC_INVALID_6,
    DATALOC_INVALID_7,
    DATALOC_INVALID_8,
    DATALOC_INVALID_9,
    DATALOC_INVALID_10,
    DATALOC_INVALID_11,
    DATALOC_END
};

enum databyte {
    DATABYTE_0_BITFIELD,
    DATABYTE_1_BITFIELD,
    DATABYTE_2_BITFIELD,
    DATABYTE_3_BITFIELD,
    DATABYTE_4_BITFIELD,
    DATABYTE_5_BITFIELD,
    DATABYTE_6_BITFIELD,
    DATABYTE_7_BITFIELD,
    DATABYTE_8_LOCATION,
    DATABYTE_9_BITFIELD,
    DATABYTE_10_MISSILE_AMMO,
    DATABYTE_11_AGE_0,
    DATABYTE_12_AGE_1,
    DATABYTE_13_AGE_2,
    DATABYTE_14_AGE_3,
    DATABYTE_15_BITFIELD,
    DATABYTE_16_SHIFT_KEY,
    DATABYTE_17_CHECKSUM
};

#if FEATURE_VERBOSEMENU
enum dataending {
    DATA_ENDING_WORST,
    DATA_ENDING_BAD,
    DATA_ENDING_FAIR,
    DATA_ENDING_GOOD,
    DATA_ENDING_BEST,
    DATA_ENDING_END
};

static char const * dataend_str [DATA_ENDING_END];
#endif // FEATURE_VERBOSEMENU

static u8           gamedata    [18];
static char const * dataloc_str [DATALOC_END];

static inline u32 reverse_bytes_u32 ( u32 value );

#endif // DATA_DATA_C
