#ifndef DATA_GAMEAGETIME_H
#define DATA_GAMEAGETIME_H

#include "util/types.h"

struct gameagetime {
    u32    hours;
    u32    minutes;
    double seconds;
    double time;
};

/* A = modifies argument
 * B = returns static buffer
 */

/* A- */ void         gameagetime_settime ( struct gameagetime *gt, u32 ticks, double ticklen );
/* -B */ char const * gameagetime_str     ( double tvrefreshrate );

#endif // DATA_GAMEAGETIME_H
