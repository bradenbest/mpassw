#ifndef DATA_CODEC_H
#define DATA_CODEC_H

#include "util/types.h"

/* B = returns static buffer
 * A = modifies argument
 */

/* B- */ u8 const * codec_base64encode ( u8 const *data );
/* B- */ u8 const * codec_base64decode ( u8 const *b64 );
/* -- */ u8         codec_checksum     ( u8 const *data );
/* -A */ void       codec_shiftencrypt ( u8 *data );
/* -A */ void       codec_shiftdecrypt ( u8 *data );
/* -- */ u8 const * codec_fullencode   ( u8 const *data );
/* B- */ u8 const * codec_fulldecode   ( u8 const *password );

#endif // DATA_CODEC_H



#ifdef DATA_CODEC_C

enum get_offset_special {
    OFF_SPACE   = 255,
    OFF_INVALID = 254,
};

static char const *alphabet;

/* B = returns static buffer
 * A = modifies argument
 */

/* -- */ static inline u8          get_offset   ( u8 b64char );
/* B- */ static inline u8 const *  encode_chunk ( u8 const *dataptr );
/* B- */ static inline u8 const *  decode_chunk ( u8 const *b64ptr );
/* -A */ static inline void        shift_right  ( u8 *data );

#endif // DATA_CODEC_C
