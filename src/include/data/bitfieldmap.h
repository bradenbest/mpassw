#ifndef DATA_BITFIELDMAP_H
#define DATA_BITFIELDMAP_H

#include "util/types.h"

struct bitfieldmap {
    u32 byte;
    u32 bit;
};

extern struct bitfieldmap  bfmaps_items_taken     [];
extern struct bitfieldmap  bfmaps_items_owned     [];
extern struct bitfieldmap  bfmaps_missiles        [];
extern struct bitfieldmap  bfmaps_etanks          [];
extern struct bitfieldmap  bfmaps_doors           [];
extern struct bitfieldmap  bfmaps_doors_zebetite  [];
extern struct bitfieldmap  bfmaps_doors_stat      [];
extern struct bitfieldmap  bfmaps_bosses          [];
extern struct bitfieldmap  bfmaps_unused          [];

/* S = modifies state */

/* - */ int  bitfieldmap_get      ( struct bitfieldmap const *selmap );
/* S */ void bitfieldmap_set      ( struct bitfieldmap const *selmap );
/* S */ void bitfieldmap_toggle   ( struct bitfieldmap const *selmap );
/* - */ int  bitfieldmap_total    ( struct bitfieldmap const *basemap );

#endif // DATA_BITFIELDMAP_H



#ifdef DATA_BITFIELDMAP_C

#define BITFIELDMAP_END 0xffff
#define BITFIELDMAP_TERMINATOR { BITFIELDMAP_END, 0 }

#endif // DATA_BITFIELDMAP_C
