#ifndef DATA_BITFIELDRANGE_H
#define DATA_BITFIELDRANGE_H

#include "util/types.h"

struct bitfieldrange {
    u32 start;
    u32 end;
};

extern struct bitfieldrange  ranges_items    [];
extern struct bitfieldrange  ranges_missiles [];
extern struct bitfieldrange  ranges_etanks   [];
extern struct bitfieldrange  ranges_doors    [];
extern struct bitfieldrange  ranges_unused   [];

#endif // DATA_BITFIELDRANGE_H
