#ifndef FEATURE_H
#define FEATURE_H

#ifdef DEBUG
#undef FEATURE_DEBUG
#define FEATURE_DEBUG 1
#endif

#if FEATURE_DEBUG == 1
#define FEATURE_DEBUG_STR "+debug"
#else
#define FEATURE_DEBUG_STR "-debug"
#endif

#if FEATURE_TUTORIAL == 1
#define FEATURE_TUTORIAL_STR "+tutorial"
#else
#define FEATURE_TUTORIAL_STR "-tutorial"
#endif

#if FEATURE_NARPAS == 1
#define FEATURE_NARPAS_STR "+narpas"
#else
#define FEATURE_NARPAS_STR "-narpas"
#endif

#if FEATURE_FANCYPROMPT == 1
#define FEATURE_FANCYPROMPT_STR "+fancy_prompt"
#else
#define FEATURE_FANCYPROMPT_STR "-fancy_prompt"
#endif

#if FEATURE_COMPACT == 1
#define FEATURE_COMPACT_STR "+compact"
#else
#define FEATURE_COMPACT_STR "-compact"
#endif

#if FEATURE_VERBOSEMENU == 1
#define FEATURE_VERBOSEMENU_STR "+verbose_menu"
#else
#define FEATURE_VERBOSEMENU_STR "-verbose_menu"
#endif

#if FEATURE_ARGS == 1
#define FEATURE_ARGS_STR "+args"
#else
#define FEATURE_ARGS_STR "-args"
#endif

#if FEATURE_COLOR == 1
#define FEATURE_COLOR_STR "+color"
#else
#define FEATURE_COLOR_STR "-color"
#endif

#include "localization/feature.h"

#define FEATURE_STR \
    FEATURE_STR_HEADER \
    "    " \
    FEATURE_DEBUG_STR       "         " \
    FEATURE_TUTORIAL_STR    "  " \
    FEATURE_NARPAS_STR      "  " \
    FEATURE_FANCYPROMPT_STR "  " \
    FEATURE_COMPACT_STR     "  \n" \
    "    " \
    FEATURE_VERBOSEMENU_STR "  " \
    FEATURE_ARGS_STR        "      " \
    FEATURE_COLOR_STR       "  " \


#endif // FEATURE_H
