#ifndef UTIL_TYPES_H
#define UTIL_TYPES_H

#include <stdint.h>
#include <stddef.h>

#define IGNORE_VARIABLE(arg) \
    ( (void)(arg) )

#define IGNORE_FUNCTION(fn) \
    IGNORE_VARIABLE(fn)

#define IGNORE_LABEL(label) \
    if(0) goto label;

typedef uint8_t  u8;
typedef uint32_t u32;
typedef uint64_t u64;

union userinput {
    u32 _u32;
    char _line[32];
};

struct menu_page;

typedef int (*entryfn)(struct menu_page *);
typedef int (*getterfn)(struct menu_page *, union userinput *);
typedef int (*handlerfn)(struct menu_page *, union userinput *);

#endif
