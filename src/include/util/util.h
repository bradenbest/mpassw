#ifndef UTIL_UTIL_H
#define UTIL_UTIL_H

#include "util/types.h"

#define SETFLAG(flags, bit) \
    ( (flags) |= (bit) )

#define UNSETFLAG(flags, bit) \
    ( (flags) &= ~(bit) )

#define TOGGLEFLAG(flags, bit) \
    ( (flags) ^= (bit) )

#define LOGFERR(msg, ...) do { \
    fprintf(stderr, "%s/%s: " msg, __FILE__, __func__, __VA_ARGS__); \
} while(0)

#define LOGERR(msg) \
    LOGFERR("%s", msg);

#define LOGFERRNO(msg, ...) do { \
    LOGFERR(msg, __VA_ARGS__); \
    perror("Error"); \
} while(0)

#define LOGERRNO(msg) \
    LOGFERRNO("%s", msg);

#define MIN(a, b) \
    ( (a) < (b) ? (a) : (b) )

/* name:  SEEKEXPR
 * @ptr:  offset pointer to array
 * @size: available size of segment pointed to by ptr
 * @expr: a number of bytes to seek forward
 * desc:  this is a fairly common pattern used for seeking along a
 *        buffer in a loop while maintaining the length
 */
#define SEEKEXPR(ptr, size, expr) do { \
    int skipamt = (expr); \
    (ptr) += skipamt; \
    (size) -= skipamt; \
} while(0)

u32          u32_parse_number ( char *buffer, size_t length );
u32          strtou32         ( char const *buffer, size_t len, int base );
size_t       strcountdigits   ( char const *str );
char const * boolstr          ( int value );
u32          clamp            ( u32 value, u32 min, u32 max );

#endif
