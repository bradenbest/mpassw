#ifndef UTIL_PUTSVFMT_H
#define UTIL_PUTSVFMT_H

#include <stddef.h>

void putsvfmt ( char const **text, struct menu_page *selmenu );

#endif



#ifdef UTIL_PUTSVFMT_C

#define PUTSVFMT_BUFSZ 40
#define PUTSVFMT_SNPSZ 32

// Note: fmt MUST be a string literal
#define GENERIC_SPLICERFN(name_frag, fmt, expr) \
    static void \
    splicerfn_##name_frag ( \
        struct menu_page * selmenu , \
        u32                valueid ) \
    { \
        struct valuefnret vfnret; \
        int snpres; \
        \
        selmenu->entrydata->fmtargs[valueid](&vfnret); \
        snpres = snprintf(snpbuf, PUTSVFMT_SNPSZ, (fmt), (expr)); \
        \
        if (snpres < 0) \
            goto exit_snprintferror; \
        \
        if (snpres >= PUTSVFMT_SNPSZ) \
            goto exit_truncate; \
        \
        buffer_append(snpbuf, snpres); \
        return; \
        \
    exit_snprintferror: \
        { \
            LOGERRNO("I/O error in snprintf\n"); \
            return; \
        } \
        \
    exit_truncate: \
        { \
            LOGERR("FATAL: Output truncated during putsvfmt splice\n"); \
            return; \
        } \
    }

enum escape_flag {
    ESCFLAG_NONE     = 0,
    ESCFLAG_SKIPLINE = 0x1,
    ESCFLAG_REDTEXT  = 0x2,
};

static int escape_flags;
static char buffer[PUTSVFMT_BUFSZ];
static char snpbuf[PUTSVFMT_SNPSZ];
static size_t buflen;

typedef void (*splicerfn)(struct menu_page *, u32);

static void splicerfn_str      ( struct menu_page *selmenu, u32 valueid );
static void splicerfn_u32      ( struct menu_page *selmenu, u32 valueid );
static void splicerfn_02u32    ( struct menu_page *selmenu, u32 valueid );
static void splicerfn_x8       ( struct menu_page *selmenu, u32 valueid );
static void splicerfn_x32      ( struct menu_page *selmenu, u32 valueid );
static void splicerfn_bool     ( struct menu_page *selmenu, u32 valueid );
static void splicerfn_tinybool ( struct menu_page *selmenu, u32 valueid );
static void splicerfn_idxchar  ( struct menu_page *selmenu, u32 valueid );
static void splicerfn_padding  ( struct menu_page *unused, u32 amount );

static inline int escseq_i ( char const * restrict unused, size_t len );
static inline int escseq_r ( char const * restrict unused1, size_t unused2 );

#if FEATURE_DEBUG
static inline size_t debug_get_fmtargslen   ( valuefn const *fmtargs );
static inline int    debug_validate_idxchar ( struct getterdata const *getterdata, int range_end );
static inline int    debug_validate_fmtargs ( struct entrydata const *entrydata, int range_start, int range_end );
#endif // FEATURE_DEBUG

static inline int  call_splicer    ( char const *tok, struct menu_page *selmenu, splicerfn splicer );
static inline int  parse           ( char const *line, struct menu_page *selmenu );
static inline int  parse_escapeseq ( char const * restrict lineoff, size_t len );
static inline void putsfmt         ( char const *line, struct menu_page *selmenu );
static inline void buffer_append   ( char const *text, size_t len );
static inline void buffer_dump     ( void );

#endif
