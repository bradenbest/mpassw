#ifndef MENU_ENTRYFN_H
#define MENU_ENTRYFN_H

#include "util/types.h"
#include "menu/valuefn.h"

#define entryfn_generic_putsv entryfn_generic_putsvfmt

enum entryfnret {
    ENTRYFN_CONTINUE,
};

int entryfn_generic_putsvfmt ( struct menu_page *selmenu );
int entryfn_generic_none     ( struct menu_page *unused );

#endif // MENU_ENTRYFN_H



#ifdef MENU_ENTRYFN_C

#if FEATURE_COMPACT
static inline char const ** get_entry_text ( struct menu_page *selmenu );
#else // FEATURE_COMPACT
#define get_entry_text(selmenu) (selmenu->entrydata->text)
#endif // FEATURE_COMPACT

#endif // MENU_ENTRYFN_C
