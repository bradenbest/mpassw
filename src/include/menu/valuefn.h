#ifndef MENU_VALUEFN_H
#define MENU_VALUEFN_H

#include "util/types.h"

struct valuefnret {
    u32          _u32;
    char const * _str;
};

typedef void (*valuefn)(struct valuefnret *vfnret);

#if FEATURE_DEBUG
#define FMTARGS_TERMINATOR valuefn_fmtargs_terminator
void valuefn_fmtargs_terminator ( struct valuefnret *unused );
#else // FEATURE_DEBUG
#define FMTARGS_TERMINATOR NULL
#endif // FEATURE_DEBUG

// U32
void valuefn_shiftbyte      ( struct valuefnret *vfnret );
void valuefn_items_taken    ( struct valuefnret *vfnret );
void valuefn_items_owned    ( struct valuefnret *vfnret );
void valuefn_missiles_total ( struct valuefnret *vfnret );
void valuefn_missiles_ammo  ( struct valuefnret *vfnret );
void valuefn_etanks         ( struct valuefnret *vfnret );
void valuefn_doors          ( struct valuefnret *vfnret );
void valuefn_doors_zebetite ( struct valuefnret *vfnret );
void valuefn_doors_stat     ( struct valuefnret *vfnret );
void valuefn_bosses         ( struct valuefnret *vfnret );
void valuefn_location       ( struct valuefnret *vfnret );
void valuefn_game_age       ( struct valuefnret *vfnret );
#if FEATURE_VERBOSEMENU
void valuefn_game_ticks     ( struct valuefnret *vfnret );
#else // FEATURE_VERBOSEMENU
#define valuefn_game_ticks NULL
#endif // FEATURE_VERBOSEMENU
void valuefn_unused         ( struct valuefnret *vfnret );
#if FEATURE_NARPAS
void valuefn_narpas_warning ( struct valuefnret *vfnret );
#else // FEATURE_NARPAS
#define valuefn_narpas_warning NULL
#endif // FEATURE_NARPAS

// STR
void valuefn_location_str    ( struct valuefnret *vfnret );
#if FEATURE_NARPAS
void valuefn_narpas_warn_str ( struct valuefnret *vfnret );
#endif // FEATURE_NARPAS
#if FEATURE_VERBOSEMENU
void valuefn_age_ntsc        ( struct valuefnret *vfnret );
void valuefn_age_pal         ( struct valuefnret *vfnret );
void valuefn_game_ending     ( struct valuefnret *vfnret );
#else //FEATURE_VERBOSEMENU
#define valuefn_age_ntsc NULL
#define valuefn_age_pal NULL
#define valuefn_game_ending NULL
#endif // FEATURE_VERBOSEMENU
void valuefn_hexdump         ( struct valuefnret *vfnret );
void valuefn_version         ( struct valuefnret *vfnret );

// BFMAP
void valuefn_items_taken_marumari    ( struct valuefnret *vfnret );
void valuefn_items_taken_bombs       ( struct valuefnret *vfnret );
void valuefn_items_taken_varia       ( struct valuefnret *vfnret );
void valuefn_items_taken_highjump    ( struct valuefnret *vfnret );
void valuefn_items_taken_screwattack ( struct valuefnret *vfnret );
void valuefn_items_owned_marumari    ( struct valuefnret *vfnret );
void valuefn_items_owned_bombs       ( struct valuefnret *vfnret );
void valuefn_items_owned_varia       ( struct valuefnret *vfnret );
void valuefn_items_owned_highjump    ( struct valuefnret *vfnret );
void valuefn_items_owned_screwattack ( struct valuefnret *vfnret );
void valuefn_items_owned_longbeam    ( struct valuefnret *vfnret );
void valuefn_items_owned_wavebeam    ( struct valuefnret *vfnret );
void valuefn_items_owned_icebeam     ( struct valuefnret *vfnret );
void valuefn_items_owned_swimsuit    ( struct valuefnret *vfnret );
void valuefn_missiles_taken_brin1    ( struct valuefnret *vfnret );
void valuefn_missiles_taken_brin2    ( struct valuefnret *vfnret );
void valuefn_missiles_taken_norf1    ( struct valuefnret *vfnret );
void valuefn_missiles_taken_norf2    ( struct valuefnret *vfnret );
void valuefn_missiles_taken_norf3    ( struct valuefnret *vfnret );
void valuefn_missiles_taken_norf4    ( struct valuefnret *vfnret );
void valuefn_missiles_taken_norf5    ( struct valuefnret *vfnret );
void valuefn_missiles_taken_norf6    ( struct valuefnret *vfnret );
void valuefn_missiles_taken_norf7    ( struct valuefnret *vfnret );
void valuefn_missiles_taken_norf8    ( struct valuefnret *vfnret );
void valuefn_missiles_taken_norf9    ( struct valuefnret *vfnret );
void valuefn_missiles_taken_norf10   ( struct valuefnret *vfnret );
void valuefn_missiles_taken_norf11   ( struct valuefnret *vfnret );
void valuefn_missiles_taken_norf12   ( struct valuefnret *vfnret );
void valuefn_missiles_taken_kraid1   ( struct valuefnret *vfnret );
void valuefn_missiles_taken_kraid2   ( struct valuefnret *vfnret );
void valuefn_missiles_taken_kraid3   ( struct valuefnret *vfnret );
void valuefn_missiles_taken_kraid4   ( struct valuefnret *vfnret );
void valuefn_missiles_taken_ridley1  ( struct valuefnret *vfnret );
void valuefn_missiles_taken_ridley2  ( struct valuefnret *vfnret );
void valuefn_missiles_taken_ridley3  ( struct valuefnret *vfnret );
void valuefn_etanks_brin1            ( struct valuefnret *vfnret );
void valuefn_etanks_brin2            ( struct valuefnret *vfnret );
void valuefn_etanks_brin3            ( struct valuefnret *vfnret );
void valuefn_etanks_norf1            ( struct valuefnret *vfnret );
void valuefn_etanks_kraid1           ( struct valuefnret *vfnret );
void valuefn_etanks_kraid2           ( struct valuefnret *vfnret );
void valuefn_etanks_ridley1          ( struct valuefnret *vfnret );
void valuefn_etanks_ridley2          ( struct valuefnret *vfnret );
void valuefn_doors_red_longbeam      ( struct valuefnret *vfnret );
void valuefn_doors_red_tourianbridge ( struct valuefnret *vfnret );
void valuefn_doors_red_bombs         ( struct valuefnret *vfnret );
void valuefn_doors_red_icebeam1      ( struct valuefnret *vfnret );
void valuefn_doors_red_varia         ( struct valuefnret *vfnret );
void valuefn_doors_red_icebeam2      ( struct valuefnret *vfnret );
void valuefn_doors_red_highjump      ( struct valuefnret *vfnret );
void valuefn_doors_red_screwattack   ( struct valuefnret *vfnret );
void valuefn_doors_red_wavebeam      ( struct valuefnret *vfnret );
void valuefn_doors_red_kraid1        ( struct valuefnret *vfnret );
void valuefn_doors_red_kraid2        ( struct valuefnret *vfnret );
void valuefn_doors_red_kraid3        ( struct valuefnret *vfnret );
void valuefn_doors_red_kraid4        ( struct valuefnret *vfnret );
void valuefn_doors_red_kraid5        ( struct valuefnret *vfnret );
void valuefn_doors_red_ridley        ( struct valuefnret *vfnret );
void valuefn_doors_yellow_ridley     ( struct valuefnret *vfnret );
void valuefn_doors_yellow_tourian    ( struct valuefnret *vfnret );
void valuefn_doors_red_tourian1      ( struct valuefnret *vfnret );
void valuefn_doors_red_tourian2      ( struct valuefnret *vfnret );
void valuefn_doors_zebetite1         ( struct valuefnret *vfnret );
void valuefn_doors_zebetite2         ( struct valuefnret *vfnret );
void valuefn_doors_zebetite3         ( struct valuefnret *vfnret );
void valuefn_doors_zebetite4         ( struct valuefnret *vfnret );
void valuefn_doors_zebetite5         ( struct valuefnret *vfnret );
void valuefn_doors_stat_ridley       ( struct valuefnret *vfnret );
void valuefn_doors_stat_kraid        ( struct valuefnret *vfnret );
void valuefn_bosses_mbrain           ( struct valuefnret *vfnret );
void valuefn_bosses_ridley           ( struct valuefnret *vfnret );
void valuefn_bosses_kraid            ( struct valuefnret *vfnret );
void valuefn_unused_1                ( struct valuefnret *vfnret );
void valuefn_unused_2                ( struct valuefnret *vfnret );
void valuefn_unused_3                ( struct valuefnret *vfnret );
void valuefn_unused_4                ( struct valuefnret *vfnret );
void valuefn_unused_5                ( struct valuefnret *vfnret );
void valuefn_unused_6                ( struct valuefnret *vfnret );
void valuefn_unused_7                ( struct valuefnret *vfnret );
void valuefn_unused_8                ( struct valuefnret *vfnret );
void valuefn_unused_9                ( struct valuefnret *vfnret );
void valuefn_unused_10               ( struct valuefnret *vfnret );
void valuefn_unused_11               ( struct valuefnret *vfnret );
void valuefn_unused_12               ( struct valuefnret *vfnret );

/* S = modifies state
 */

// non-generic
/* - */ void valuefn_missiles_max     ( struct valuefnret *vfnret );
/* S */ void valuefn_data_to_password ( struct valuefnret *vfnret );
/* S */ void valuefn_show_password    ( struct valuefnret *vfnret );

#endif // MENU_VALUEFN_H



#ifdef MENU_VALUEFN_C

#define VALUEFN_GENERIC(name_frag, expr, type) \
    void \
    (valuefn_##name_frag)(struct valuefnret *vfnret) \
    { \
        vfnret->type = (expr); \
    }

#define VALUEFN_GENERIC_U32(name_frag, expr) \
    VALUEFN_GENERIC(name_frag, (expr), _u32)

#define VALUEFN_GENERIC_STR(name_frag, expr) \
    VALUEFN_GENERIC(name_frag, (expr), _str)

#define VALUEFN_GENERIC_BFMAP(name_frag, basemap, offset) \
    VALUEFN_GENERIC_U32(name_frag, bitfieldmap_get(bfmaps_##basemap + offset))

// The exact refresh rates of the NTSC/PAL NES
#define NES_RFRATE_NTSC 60.0988
#define NES_RFRATE_PAL  50.006978

#endif // MENU_VALUEFN_C
