#ifndef MENU_HANDLERDATA_H
#define MENU_HANDLERDATA_H

#include <stddef.h>

#include "util/types.h"

struct handlerdata {
    u32 *                  children;
    u32                    byteid;
    struct bitfieldmap *   bitfield;
    struct bitfieldrange * ranges;
};

extern struct handlerdata handlerdata_main;
extern struct handlerdata handlerdata_quit_confirm;
extern struct handlerdata handlerdata_edit_bytes;
extern struct handlerdata handlerdata_items;
extern struct handlerdata handlerdata_missiles;
extern struct handlerdata handlerdata_etanks;
extern struct handlerdata handlerdata_doors;
extern struct handlerdata handlerdata_bosses;
extern struct handlerdata handlerdata_unused;

extern struct handlerdata handlerdata_bulk_items_taken;
extern struct handlerdata handlerdata_bulk_items_owned;
extern struct handlerdata handlerdata_bulk_missiles_all;
extern struct handlerdata handlerdata_bulk_missiles_brinstar;
extern struct handlerdata handlerdata_bulk_missiles_norfair;
extern struct handlerdata handlerdata_bulk_missiles_kraid;
extern struct handlerdata handlerdata_bulk_missiles_ridley;
extern struct handlerdata handlerdata_bulk_etanks_all;
extern struct handlerdata handlerdata_bulk_etanks_brinstar;
extern struct handlerdata handlerdata_bulk_etanks_norfair;
extern struct handlerdata handlerdata_bulk_etanks_kraid;
extern struct handlerdata handlerdata_bulk_etanks_ridley;
extern struct handlerdata handlerdata_bulk_doors_doors;
extern struct handlerdata handlerdata_bulk_doors_zebetites;
extern struct handlerdata handlerdata_bulk_doors_statues;
extern struct handlerdata handlerdata_bulk_unused;

extern struct handlerdata handlerdata_setbyte_0;
extern struct handlerdata handlerdata_setbyte_1;
extern struct handlerdata handlerdata_setbyte_2;
extern struct handlerdata handlerdata_setbyte_3;
extern struct handlerdata handlerdata_setbyte_4;
extern struct handlerdata handlerdata_setbyte_5;
extern struct handlerdata handlerdata_setbyte_6;
extern struct handlerdata handlerdata_setbyte_7;
extern struct handlerdata handlerdata_setbyte_8;
extern struct handlerdata handlerdata_setbyte_9;
extern struct handlerdata handlerdata_setbyte_10;
extern struct handlerdata handlerdata_setbyte_11;
extern struct handlerdata handlerdata_setbyte_12;
extern struct handlerdata handlerdata_setbyte_13;
extern struct handlerdata handlerdata_setbyte_14;
extern struct handlerdata handlerdata_setbyte_15;
extern struct handlerdata handlerdata_setbyte_16;
extern struct handlerdata handlerdata_setbyte_17;

extern struct handlerdata handlerdata_togglebit_items_taken_0;
extern struct handlerdata handlerdata_togglebit_items_taken_1;
extern struct handlerdata handlerdata_togglebit_items_taken_2;
extern struct handlerdata handlerdata_togglebit_items_taken_3;
extern struct handlerdata handlerdata_togglebit_items_taken_4;
extern struct handlerdata handlerdata_togglebit_items_owned_0;
extern struct handlerdata handlerdata_togglebit_items_owned_1;
extern struct handlerdata handlerdata_togglebit_items_owned_2;
extern struct handlerdata handlerdata_togglebit_items_owned_3;
extern struct handlerdata handlerdata_togglebit_items_owned_4;
extern struct handlerdata handlerdata_togglebit_items_owned_5;
extern struct handlerdata handlerdata_togglebit_items_owned_6;
extern struct handlerdata handlerdata_togglebit_items_owned_7;
extern struct handlerdata handlerdata_togglebit_items_owned_8;
extern struct handlerdata handlerdata_togglebit_missiles_brin1;
extern struct handlerdata handlerdata_togglebit_missiles_brin2;
extern struct handlerdata handlerdata_togglebit_missiles_norf1;
extern struct handlerdata handlerdata_togglebit_missiles_norf2;
extern struct handlerdata handlerdata_togglebit_missiles_norf3;
extern struct handlerdata handlerdata_togglebit_missiles_norf4;
extern struct handlerdata handlerdata_togglebit_missiles_norf5;
extern struct handlerdata handlerdata_togglebit_missiles_norf6;
extern struct handlerdata handlerdata_togglebit_missiles_norf7;
extern struct handlerdata handlerdata_togglebit_missiles_norf8;
extern struct handlerdata handlerdata_togglebit_missiles_norf9;
extern struct handlerdata handlerdata_togglebit_missiles_norf10;
extern struct handlerdata handlerdata_togglebit_missiles_norf11;
extern struct handlerdata handlerdata_togglebit_missiles_norf12;
extern struct handlerdata handlerdata_togglebit_missiles_kraid1;
extern struct handlerdata handlerdata_togglebit_missiles_kraid2;
extern struct handlerdata handlerdata_togglebit_missiles_kraid3;
extern struct handlerdata handlerdata_togglebit_missiles_kraid4;
extern struct handlerdata handlerdata_togglebit_missiles_ridley1;
extern struct handlerdata handlerdata_togglebit_missiles_ridley2;
extern struct handlerdata handlerdata_togglebit_missiles_ridley3;
extern struct handlerdata handlerdata_togglebit_etanks_brin1;
extern struct handlerdata handlerdata_togglebit_etanks_brin2;
extern struct handlerdata handlerdata_togglebit_etanks_brin3;
extern struct handlerdata handlerdata_togglebit_etanks_norf1;
extern struct handlerdata handlerdata_togglebit_etanks_kraid1;
extern struct handlerdata handlerdata_togglebit_etanks_kraid2;
extern struct handlerdata handlerdata_togglebit_etanks_ridley1;
extern struct handlerdata handlerdata_togglebit_etanks_ridley2;
extern struct handlerdata handlerdata_togglebit_doors_red_longbeam;
extern struct handlerdata handlerdata_togglebit_doors_red_tourianbridge;
extern struct handlerdata handlerdata_togglebit_doors_red_bombs;
extern struct handlerdata handlerdata_togglebit_doors_red_icebeam1;
extern struct handlerdata handlerdata_togglebit_doors_red_varia;
extern struct handlerdata handlerdata_togglebit_doors_red_icebeam2;
extern struct handlerdata handlerdata_togglebit_doors_red_highjump;
extern struct handlerdata handlerdata_togglebit_doors_red_screwattack;
extern struct handlerdata handlerdata_togglebit_doors_red_wavebeam;
extern struct handlerdata handlerdata_togglebit_doors_red_kraid1;
extern struct handlerdata handlerdata_togglebit_doors_red_kraid2;
extern struct handlerdata handlerdata_togglebit_doors_red_kraid3;
extern struct handlerdata handlerdata_togglebit_doors_red_kraid4;
extern struct handlerdata handlerdata_togglebit_doors_red_kraid5;
extern struct handlerdata handlerdata_togglebit_doors_red_ridley;
extern struct handlerdata handlerdata_togglebit_doors_yellow_ridley;
extern struct handlerdata handlerdata_togglebit_doors_yellow_tourian;
extern struct handlerdata handlerdata_togglebit_doors_red_tourian1;
extern struct handlerdata handlerdata_togglebit_doors_red_tourian2;
extern struct handlerdata handlerdata_togglebit_doors_zebetite1;
extern struct handlerdata handlerdata_togglebit_doors_zebetite2;
extern struct handlerdata handlerdata_togglebit_doors_zebetite3;
extern struct handlerdata handlerdata_togglebit_doors_zebetite4;
extern struct handlerdata handlerdata_togglebit_doors_zebetite5;
extern struct handlerdata handlerdata_togglebit_doors_stat_ridley;
extern struct handlerdata handlerdata_togglebit_doors_stat_kraid;
extern struct handlerdata handlerdata_togglebit_bosses_mbrain;
extern struct handlerdata handlerdata_togglebit_bosses_ridley;
extern struct handlerdata handlerdata_togglebit_bosses_kraid;
extern struct handlerdata handlerdata_togglebit_unused_1;
extern struct handlerdata handlerdata_togglebit_unused_2;
extern struct handlerdata handlerdata_togglebit_unused_3;
extern struct handlerdata handlerdata_togglebit_unused_4;
extern struct handlerdata handlerdata_togglebit_unused_5;
extern struct handlerdata handlerdata_togglebit_unused_6;
extern struct handlerdata handlerdata_togglebit_unused_7;
extern struct handlerdata handlerdata_togglebit_unused_8;
extern struct handlerdata handlerdata_togglebit_unused_9;
extern struct handlerdata handlerdata_togglebit_unused_10;
extern struct handlerdata handlerdata_togglebit_unused_11;
extern struct handlerdata handlerdata_togglebit_unused_12;

#endif // MENU_HANDLERDATA_H



#ifdef MENU_HANDLERDATA_C

#define GENERIC_HANDLERDATA(name_frag, children_ptr, byteid_ref, bitfield_ptr, range_ptr) \
    struct handlerdata handlerdata_##name_frag = { \
        .children = (children_ptr), \
        .byteid = (byteid_ref), \
        .bitfield = (bitfield_ptr), \
        .ranges = (range_ptr), \
    }

#define GENERIC_HANDLERDATA_DIRECTORY(name_frag) \
    GENERIC_HANDLERDATA(name_frag, children_##name_frag, 0, NULL, NULL)

#define GENERIC_HANDLERDATA_BULK(name_frag, bfmap_frag, range_frag, offset) \
    GENERIC_HANDLERDATA(bulk_##name_frag, NULL, 0, bfmaps_##bfmap_frag, ranges_##range_frag + (offset))

#define GENERIC_HANDLERDATA_SETBYTE(byteid) \
    GENERIC_HANDLERDATA(setbyte_##byteid, NULL, byteid, NULL, NULL)

#define GENERIC_HANDLERDATA_TOGGLEBIT(name_frag, bfmaps_frag, offset) \
    GENERIC_HANDLERDATA(togglebit_##name_frag, NULL, 0, bfmaps_##bfmaps_frag + (offset), NULL)

static u32  children_main         [22];
static u32  children_quit_confirm [3 ];
static u32  children_edit_bytes   [20];
static u32  children_items        [22];
static u32  children_missiles     [34];
static u32  children_etanks       [20];
static u32  children_doors        [36];
static u32  children_bosses       [5 ];
static u32  children_unused       [16];

#endif // MENU_HANDLERDATA_C
