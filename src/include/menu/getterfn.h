#ifndef MENU_GETTERFN_H
#define MENU_GETTERFN_H

#include "util/types.h"

enum getterfnret {
    GETTERFN_CONTINUE,
    GETTERFN_EOF,
    GETTERFN_CANCEL,
};

/* A = modifies argument
 * I = ignores arguments
 */

/* A- */ int getterfn_generic_u32          ( struct menu_page *selmenu, union userinput *uinput );
/* A- */ int getterfn_generic_char_or_u32  ( struct menu_page *selmenu, union userinput *uinput );
/* A- */ int getterfn_generic_line         ( struct menu_page *selmenu, union userinput *uinput );
/* -I */ int getterfn_generic_waitcontinue ( struct menu_page *unused, union userinput *unused2 );
/* -I */ int getterfn_generic_none         ( struct menu_page *unused, union userinput *unused2 );

#endif // MENU_GETTERFN_H



#ifdef MENU_GETTERFN_C

#define GENERIC_GETTERFN_SETUP(buffer, bufsz) do { \
    ui_show_prompt(selmenu); \
    nread = ui_getline((buffer), (bufsz)); \
    \
    if(feof(stdin) || ferror(stdin)) \
        return GETTERFN_EOF; \
    \
    if(nread == 0) \
        return GETTERFN_CANCEL; \
} while(0)

#endif // MENU_GETTERFN_C
