#ifndef MENU_ENTRYDATA_H
#define MENU_ENTRYDATA_H

#include <stddef.h>

#include "menu/valuefn.h"
#include "util/types.h"

struct entrydata {
    char const ** text;
    valuefn *     fmtargs;
};

extern struct entrydata entrydata_intro;
extern struct entrydata entrydata_tutorial_hint;
extern struct entrydata entrydata_main;
extern struct entrydata entrydata_quit_confirm;
extern struct entrydata entrydata_input_password;
extern struct entrydata entrydata_show_password;
extern struct entrydata entrydata_input_hex;
extern struct entrydata entrydata_edit_bytes;
extern struct entrydata entrydata_show_summary;
extern struct entrydata entrydata_set_key;
extern struct entrydata entrydata_tutorial;
extern struct entrydata entrydata_tutorial_oops;
extern struct entrydata entrydata_items;
extern struct entrydata entrydata_missiles;
extern struct entrydata entrydata_etanks;
extern struct entrydata entrydata_doors;
extern struct entrydata entrydata_bosses;
extern struct entrydata entrydata_location;
extern struct entrydata entrydata_age;
extern struct entrydata entrydata_unused;
extern struct entrydata entrydata_missiles_ammo;
extern struct entrydata entrydata_generic_setbyte;

#endif // MENU_ENTRYDATA_H



#ifdef MENU_ENTRYDATA_C

#define GENERIC_ENTRYDATA(name_frag, text_ptr, fmtargs_ptr) \
    struct entrydata entrydata_##name_frag = { \
        .text = (text_ptr), \
        .fmtargs = (fmtargs_ptr), \
    }

#define GENERIC_ENTRYDATA_PUTSVFMT(name_frag) \
    GENERIC_ENTRYDATA(name_frag, text_##name_frag, fmtargs_##name_frag)

#define GENERIC_ENTRYDATA_PUTSV(name_frag) \
    GENERIC_ENTRYDATA(name_frag, text_##name_frag, NULL)

#if 0
These are here for the human reader

static char const * text_intro           [];
static char const * text_tutorial_hint   [];
static char const * text_main            [];
static char const * text_quit_confirm    [];
static char const * text_input_password  [];
static char const * text_show_password   [];
static char const * text_input_hex       [];
static char const * text_edit_bytes      [];
static char const * text_show_summary    [];
static char const * text_set_key         [];
static char const * text_tutorial        [];
static char const * text_tutorial_oops   [];
static char const * text_items           [];
static char const * text_missiles        [];
static char const * text_etanks          [];
static char const * text_doors           [];
static char const * text_bosses          [];
static char const * text_location        [];
static char const * text_age             [];
static char const * text_unused          [];
static char const * text_missiles_ammo   [];
static char const * text_generic_setbyte [];

static valuefn fmtargs_intro           [];
static valuefn fmtargs_main            [];
static valuefn fmtargs_show_password   [];
static valuefn fmtargs_input_hex       [];
static valuefn fmtargs_edit_bytes      [];
static valuefn fmtargs_show_summary    [];
static valuefn fmtargs_items           [];
static valuefn fmtargs_missiles        [];
static valuefn fmtargs_etanks          [];
static valuefn fmtargs_doors           [];
static valuefn fmtargs_bosses          [];
static valuefn fmtargs_location        [];
static valuefn fmtargs_age             [];
static valuefn fmtargs_unused          [];
#endif // 0

#endif // MENU_ENTRYDATA_C
