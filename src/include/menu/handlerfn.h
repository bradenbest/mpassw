#ifndef MENU_HANDLERFN_H
#define MENU_HANDLERFN_H

#include "util/types.h"

enum handlerfnret {
    HANDLERFN_NOSETPAGEID,
    HANDLERFN_RETURNTOPARENT,
};

int handlerfn_generic_directory     ( struct menu_page *selmenu, union userinput *uinput );
int handlerfn_generic_togglebit     ( struct menu_page *selmenu, union userinput *unused2 );
int handlerfn_generic_setbyte       ( struct menu_page *selmenu, union userinput *uinput );
int handlerfn_generic_bulk_toggle   ( struct menu_page *selmenu, union userinput *unused2 );
int handlerfn_generic_bulk_set      ( struct menu_page *selmenu, union userinput *unused2 );
int handlerfn_generic_none          ( struct menu_page *unused, union userinput *unused2 );
int handlerfn_input_password        ( struct menu_page *unused, union userinput *uinput );
int handlerfn_input_hex             ( struct menu_page *unused, union userinput *uinput );
int handlerfn_set_key               ( struct menu_page *unused, union userinput *uinput );
int handlerfn_set_random_key        ( struct menu_page *unused, union userinput *unused2 );
#if FEATURE_COMPACT
int handlerfn_compact               ( struct menu_page *unused, union userinput *unused2 );
int handlerfn_nocompact             ( struct menu_page *unused, union userinput *unused2 );
#endif // FEATURE_COMPACT
int handlerfn_missiles_ammo         ( struct menu_page *unused, union userinput *uinput );
int handlerfn_location              ( struct menu_page *unused, union userinput *uinput );
int handlerfn_age                   ( struct menu_page *unused, union userinput *uinput );
#if FEATURE_NARPAS
int handlerfn_toggle_narpas_warning ( struct menu_page *unused, union userinput *unused2 );
#endif // FEATURE_NARPAS
#if FEATURE_TUTORIAL
int handlerfn_tutorial ( struct menu_page *unused, union userinput *unused2 );
#endif // FEATURE_TUTORIAL

#endif // MENU_HANDLERFN_H



#ifdef MENU_HANDLERFN_C

typedef void (*bulkbitfn)(struct bitfieldmap const *);

#if FEATURE_TUTORIAL
struct tutorial_page {
    u32          pageid;
    char const * text;
};

static struct tutorial_page tutorial_pages[20];

static inline int tutorial_exec ( struct tutorial_page *selpage );
#endif // FEATURE_TUTORIAL

static inline int bulk_set_or_toggle ( struct menu_page *selmenu, bulkbitfn operation );

#endif // MENU_HANDLERFN_C
