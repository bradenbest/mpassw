#ifndef MENU_GETTERDATA_H
#define MENU_GETTERDATA_H

struct getterdata {
    char const *children_char;
};

extern struct getterdata getterdata_main;
extern struct getterdata getterdata_quit_confirm;
extern struct getterdata getterdata_items;
extern struct getterdata getterdata_missiles;
extern struct getterdata getterdata_etanks;
extern struct getterdata getterdata_doors;
extern struct getterdata getterdata_bosses;
extern struct getterdata getterdata_location;
extern struct getterdata getterdata_unused;

#endif // MENU_GETTERDATA_H



#ifdef MENU_GETTERDATA_C

#define GENERIC_GETTERDATA(name_frag, children_chars) \
    struct getterdata getterdata_##name_frag = { \
        .children_char = (children_chars), \
    }

#endif // MENU_GETTERDATA_C
