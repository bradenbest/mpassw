#ifndef UI_UI_H
#define UI_UI_H

#include <stddef.h>

#include "util/types.h"
#include "menu/menu.h"

#define ANSIESC_COLORRED  "\x1b[31m"
#define ANSIESC_COLORCLR  "\x1b[0m"

#define ui_puts(str) \
    ui_print((str), UIFLAG_NONE)

#define ui_qputs(str) \
    ui_print((str), UIFLAG_INTERACTIVE)

enum uistate_flag {
    UIFLAG_NONE        = 0,
    UIFLAG_WARN_NARPAS = 0x1,
    UIFLAG_COMPACT     = 0x2,
    UIFLAG_TUTORIAL    = 0x4,
    UIFLAG_INTERACTIVE = 0x8,
};

/* A = modifies argument
 * S = modifies state
 */

/* -S */ void         ui_set_pageid      ( u32 pageid );
/* -- */ int          ui_getflags        ( void );
/* -S */ void         ui_setflag         ( int bit );
/* -S */ void         ui_unsetflag       ( int bit );
/* -S */ void         ui_toggleflag      ( int bit );
/* -- */ void         ui_clearscreen     ( void );
/* -- */ int          ui_isatty          ( void );
/* A- */ size_t       ui_getline         ( char *buffer, size_t bufsz );
/* -- */ void         ui_print           ( char const *str, int flags );
/* -- */ void         ui_waitcontinue    ( void );
#if FEATURE_NARPAS
/* -- */ char const * ui_narpas_warn_str ( void );
#endif // FEATURE_NARPAS
/* -- */ void         ui_show_prompt     ( struct menu_page *selmenu );
/* AS */ int          ui_call_entry      ( struct menu_page *selmenu );
/* A- */ int          ui_call_getter     ( struct menu_page *selmenu, union userinput *uinput );
/* AS */ int          ui_call_handler    ( struct menu_page *selmenu, union userinput *uinput );
/* AS */ int          ui_exec_menupage   ( struct menu_page *selmenu );
/* -S */ void         ui_menuloop        ( void );

#endif // UI_UI_H



#ifdef UI_UI_C

/* Relevant documentation: https://en.wikipedia.org/wiki/ANSI_escape_code#CSIsection
 * 0x1b '['   CSI
 * CSI H      CUP  reset cursor position to 0,0
 * CSI n J    ED   clear screen, 1 = from cursor to beginning of screen
 */

#define ANSIESC_CSIEDTO0  "\x1b[1J"
#define ANSIESC_CSICUPTO0 "\x1b[H"

struct uistate {
    u32 pageid;
    u32 flags;
};

static struct uistate uistate;
#if FEATURE_NARPAS
static char const *   narpas_warn_str;
#endif // FEATURE_NARPAS

/* AS */ static inline void try_clearscreen     ( struct menu_page *selmenu );
/* -- */ static inline void try_showheader      ( struct menu_page *selmenu );

#endif // UI_UI_C
