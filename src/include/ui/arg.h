#ifndef UI_ARG_H
#define UI_ARG_H

#if FEATURE_ARGS
#define OPTS_TERMINATOR { NULL, NULL, 0, NULL }

typedef int (*argfn)(char const *);

struct arg {
    char const * opt;
    char const * opt_long;
    int          hasarg;
    argfn        callback;
};

int parse_arg ( struct arg *opts, char **argptr );

#endif // FEATURE_ARGS

#endif // UI_ARG_H



#ifdef UI_ARG_C

#if FEATURE_ARGS
enum matchtype {
    OPT_NONE,
    OPT_SHORT,
    OPT_LONG,
};

static int matchtype;

static inline int          streqopt     ( char const *str, char const *opt );
static inline struct arg * find_arg     ( struct arg *opts, char const *arg );
static inline size_t       strlen_delim ( char const *str, int delim );
#endif // FEATURE_ARGS

#endif // UI_ARG_C
