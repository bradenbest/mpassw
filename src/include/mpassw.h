#ifndef MPASSW_H
#define MPASSW_H

int main ( int argc, char **argv );

#endif // MPASSW_H



#ifdef MPASSW_C

#if 0
For human eyes only

static int          caught_sigint;
static struct arg   opts[];
static char const * usage_text;
#endif // 0

/* E = exits the program
 * S = modifies state
 * A = takes argument
 */

#if FEATURE_ARGS

#if FEATURE_DEBUG
/* E-- */ static int opt_d ( char const * restrict unused );
#endif // FEATURE_DEBUG
#if FEATURE_COMPACT
/* -S- */ static int opt_c ( char const * restrict unused );
#endif // FEATURE_COMPACT
/* -S- */ static int opt_q ( char const * restrict unused );
/* -SA */ static int opt_p ( char const * restrict arg );
/* -SA */ static int opt_x ( char const * restrict arg );
/* -SA */ static int opt_m ( char const * restrict arg );
/* -SA */ static int opt_l ( char const * restrict arg );
/* -SA */ static int opt_a ( char const * restrict arg );
/* E-- */ static int opt_h ( char const * restrict unused );
/* E-- */ static int opt_H ( char const * restrict unused );
#if FEATURE_TUTORIAL
/* -S- */ static int opt_t ( char const * restrict unused );
#endif // FEATURE_TUTORIAL
/* E-- */ static int opt_v ( char const * restrict unused );
/* E-- */ static int opt_e ( char const * restrict unused );

#endif // FEATURE_ARGS

static void callback_sigint ( int unused );
static void callback_exit   ( void );

#endif // MPASSW_C
