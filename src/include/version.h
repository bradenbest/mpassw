#ifndef VERSION_H
#define VERSION_H

#include "feature.h"

char const * version_str ( void );

#endif



#ifdef VERSION_C

#define BUFSZ 32
#define DEFAULT_STR "<undefined>"

enum {
    MAJOR,
    MINOR,
    PATCH,
    SUBPATCH,
    END
};

static inline size_t populate_version_fragment ( char *buffer, size_t size, int fragment );

#endif
