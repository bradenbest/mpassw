#include <stdio.h>
#include <string.h>

#include "util/util.h"

#define UI_ARG_C
#include "ui/arg.h"
#include "ui/arg.static.c"

/* name:    parse_arg
 * @opts:   opt table (contains callbacks)
 * @argptr: offset pointer to argv
 * return:  number of arguments parsed or 0
 * context: may be undefined depending on FEATURE_ARGS
 * desc:    parses a commandline argument and returns nonzero to tell
 *          the caller how many arguments to advance by or 0 to quit
 *          parse_arg will end the program if...
 *
 *          i.   the argument is unexpectedly NULL
 *          ii.  an argument-takinng opt is missing its argument
 *          iii. the argument callback (such as opt_h) returned 0
 *          iv.  match is not NULL but matchtype is OPT_NONE. This state
 *               should not be reachable, so if it is, an error is
 *               printed (+debug only)
 *
 * 1. In rare cases, argv[i < argc] can be NULL
 */
#if FEATURE_ARGS
int
parse_arg (
    struct arg * opts   ,
    char **      argptr )
{
    struct arg *match;
    char const *param = NULL;
    int nargs = 1;

    if (*argptr == NULL) // 1
        goto exit_quit;

    match = find_arg(opts, *argptr);

    if (match == NULL)
        goto exit_nextarg;

    if (!match->hasarg)
        goto exit_execopt;

    switch (matchtype) {
        case OPT_SHORT:
            param = *(argptr + 1);
            nargs = 2;

            if (param == NULL)
                goto exit_badarg;

            goto exit_execopt;

        case OPT_LONG:
            param = strchr(*argptr, '=') + 1;

            if (param - 1 == NULL)
                goto exit_badarg;

            goto exit_execopt;
    }

#if FEATURE_DEBUG
    goto exit_unreachable;
#else
    IGNORE_LABEL(exit_unreachable);
#endif // FEATURE_DEBUG

exit_execopt:
    {
        if (match->callback(param) == 0)
            return 0;

        return nargs;
    }

exit_badarg:
    {
        LOGERR("Received malformed arguments.\n");
        LOGFERR("Usage: %s value or %s=value\n", match->opt, match->opt_long);
        return 0;
    }

exit_unreachable:
    {
        LOGFERR("This branch wasn't supposed to be reachable. opt: %s\n", match->opt_long);
        return 0;
    }

exit_quit:
    return 0;

exit_nextarg:
    return 1;
}
#endif // FEATURE_ARGS
