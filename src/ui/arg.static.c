#if FEATURE_ARGS
static int matchtype;

/* name:   streqopt
 * @str:   string from user
 * @opt:   opt to compare against
 * return: 1 or 0
 * desc:   checks if the two strings have the same length and compare
 *         equal. length is terminated by NUL (`-l t`) or '='
 *         (`--location=t`)
 *         returns 1 if equal and 0 otherwise
 */
static inline int
streqopt (
    char const * str ,
    char const * opt )
{
    size_t optlen = strlen_delim(opt, '=');
    size_t slen = strlen_delim(str, '=');

    if (slen != optlen)
        return 0;

    return memcmp(str, opt, optlen) == 0;
}

/* name:    find_arg
 * @opts:   opt table
 * @arg:    argument from argv
 * return:  arg pointer from opts
 * context: used by parse_arg to look up and find an argument
 * desc:    does a table lookup returning the first entry in opts that
 *          matches the commandline argument according to streqopt and
 *          marking the type of match that was found. If no match is
 *          found, NULL is returned and matchtype is set to OPT_NONE.
 */
static inline struct arg *
find_arg (
    struct arg * opts ,
    char const * arg  )
{
    for (int i = 0; opts[i].callback != NULL; ++i)
        {
            if (streqopt(arg, opts[i].opt))
                {
                    matchtype = OPT_SHORT;
                    return opts + i;
                }

            if (streqopt(arg, opts[i].opt_long))
                {
                    matchtype = OPT_LONG;
                    return opts + i;
                }
        }

    matchtype = OPT_NONE;
    return NULL;
}

/* name:    strlen_delim
 * @str:    string to count
 * @delim:  delimiter to stop at
 * return:  length of string up to delimiter or length of string
 * context: used by arg parsing code to correctly parse --longopt=value
 *          arguments
 * deesc:   same as strlen, except a delimiter is accepted to allow an
 *          alternate stopping point
 */
static inline size_t
strlen_delim(char const *str, int delim)
{
    size_t slen = strlen(str);
    char const *match = memchr(str, delim, slen);

    return (match == NULL) ? (slen) : (size_t)(match - str);
}
#endif // FEATURE_ARGS
