#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "data/data.h"
#include "localization/ui.h"
#include "menu/menu.h"
#include "menu/entryfn.h"
#include "menu/getterfn.h"
#include "menu/handlerfn.h"
#include "util/util.h"

#define UI_UI_C
#include "ui/ui.h"
#include "ui/ui.static.c"

#if defined(WIN32) && !( defined(__MINGW32__) || defined(__MINGW64__) )
#    error Windows is supported through mingw only. Set MINGWCC appropriately and run the `release-mingw` build target.
#endif

/* name:    ui_set_pageid
 * @pageid: page ID to switch to
 * context: mutates uistate
 * desc:    sets selected menu page
 */
void
ui_set_pageid ( u32 pageid )
{
    uistate.pageid = pageid;
}

/* name:   ui_getflags
 * return: uistate.flags (see UIFLAG_*)
 * desc:   returns uistate flags
 */
int
ui_getflags ( void )
{
    return uistate.flags;
}

/* callback:   void (*) (int flagvalue)
 * name:       ui_{set,unset,toggle}flag
 * @flagvalue: flag to act on (see UIFLAG_*)
 * desc:       sets, unsets, or toggles the flag(s) specified by
 *             flagvalue
 */

void
ui_setflag ( int flagvalue )
{
    SETFLAG(uistate.flags, flagvalue);
}

void
ui_unsetflag ( int flagvalue )
{
    UNSETFLAG(uistate.flags, flagvalue);
}

void
ui_toggleflag ( int flagvalue )
{
    TOGGLEFLAG(uistate.flags, flagvalue);
}

/* name:    ui_clearscreen
 * context: if WIN32 is defined, this function falls back on 30 newlines
 *          to be safe, as cmd.exe and similar terminals do not support
 *          ANSI escape sequences
 * desc:    clears the screen
 */
void
ui_clearscreen ( void )
#ifdef WIN32
{
    static char const lines[] = "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";

    fwrite(lines, 1, sizeof lines - 1, stdout);
}
#else
{
    static char const escapeseq[] = ANSIESC_CSIEDTO0 ANSIESC_CSICUPTO0;

    fwrite(escapeseq, 1, sizeof escapeseq - 1, stdout);
}
#endif

/* name:    ui_isatty
 * return:  1 or 0
 * desc:    wraps isatty (is a TTY), which tells if the connection to
 *          stdin is interactive (from a human). isatty is a POSIX
 *          function.
 */
int
ui_isatty ( void )
{
    return isatty(STDIN_FILENO);
}

/* name:    ui_getline
 * @buffer: mutable buffer or NULL
 * @bufsz:  size of buffer (must be 0 if buffer is NULL)
 * return:  number of bytes written to buffer
 * context: mutates argument, has some unusual checks put in to deal
 *          with misbehaving implementations of getchar
 * desc:    reads an entire line from stdin and stores what can be
 *          stored in the buffer. The rest is thrown away to ensure that
 *          every call prompts the user and does not pull input from
 *          "leftovers" (see note 2). The trailing newline is also
 *          thrown away.
 *
 * 1. On some platforms, getchar is non-compliant and will continue
 *    prompting the user after EOF, resulting in an irritating UX bug.
 *    An feof() check before the loop is necessary to prevent this
 *
 * 2. The behavior of this loop is intentional and necessary to
 *    guarantee that every call will cleanly prompt the user instead of
 *    sucking up leftover stdin buffer contents. Those leftovers need to
 *    be thrown away, because they are garbage. It's also the mechanism
 *    that ui_waitcontinue relies on to function.
 *
 * 3. This is just for polish. It looks nicer if the next output is on
 *    the next line rather than hugging the prompt.
 */
size_t
ui_getline (
    char * buffer ,
    size_t bufsz  )
{
    int ch;
    size_t buflen = 0;

    if (ferror(stdin))
        goto exit_error;

    if (feof(stdin)) // 1
        goto exit_eof;

    while ((ch = getchar()) != EOF && ch != '\n')
        if (buflen < bufsz) // 2
            buffer[buflen++] = ch;

    if (ch == EOF && (ui_getflags() & UIFLAG_INTERACTIVE))
        putchar('\n'); // 3

    return buflen;

exit_error:
    {
        LOGERRNO("Encountered I/O error.\n");
        return 0;
    }

exit_eof:
    return 0;
}

/* name:    ui_print
 * @str:    string to write to stdout
 * @flags:  flags (UIFLAG_*)
 * context: if UIFLAG_INTERACTIVE is set in flags, then whether the
 *          uiflags contains UIFLAG_INTERACTIVE will affect whether this
 *          prints anything, the macros ui_puts and ui_qputs wrap this
 *          function
 * desc:    most print statements go through this function. The only
 *          exceptions would be the LOGERR macros, putsvfmt, and any
 *          stragglers in the main/arg parsing pipeline
 */
void
ui_print (
    char const * str   ,
    int          flags )
{
    if (flags & UIFLAG_INTERACTIVE && !(ui_getflags() & UIFLAG_INTERACTIVE))
        return;

    fwrite(str, 1, strlen(str), stdout);
}

/* name:    ui_waitcontinue
 * context: does nothing in quiet mode
 * desc:    gives the user a press enter to continue prompt that is
 *          tolerant of garbage
 */
void
ui_waitcontinue ( void )
{
    if (!(ui_getflags() & UIFLAG_INTERACTIVE))
        return;

    ui_qputs(UI_WAITCONTINUE_TEXT);
    ui_getline(NULL, 0);
}

/* name: ui_narpas_warn_str
 * return:  narpas_warn_str or ""
 * context: narpas_warn_str is defined in ui/ui.static.c
 * desc:    checks if the narpas warning flag is set and checks if the
 *          password matches NARPASSWORD. Returns narpas_warn_str if so,
 *          else it returns an empty string
 *
 */
#if FEATURE_NARPAS
char const *
ui_narpas_warn_str ( void )
{
    if ((ui_getflags() & UIFLAG_WARN_NARPAS) && data_check_narpas())
        return narpas_warn_str;

    return "";
}
#endif // FEATURE_NARPAS

/* name:     ui_show_prompt
 * @selmenu: menu page pointer
 * context:  affected by tutorial flag, interactive flag, searches
 *           recursively up menu parent relations, so the parent tree
 *           should ultimately end at MAIN, dead ends are not fatal,
 *           though.
 * desc:     prints the input prompt, which includes information about
 *           the user's current "path" in the menu table.
 */
void
ui_show_prompt ( struct menu_page *selmenu )
#if FEATURE_FANCYPROMPT
{
    static size_t const nssize = 3;
    char const *namestack[nssize];
    char const **nsptr = namestack;
    char const *leader = "mpassw";

    if (!(ui_getflags() & UIFLAG_INTERACTIVE))
        return;

#if FEATURE_TUTORIAL
    if (ui_getflags() & UIFLAG_TUTORIAL)
        leader = "Tutorial";
#endif

    while (selmenu != NULL)
        {
            *nsptr++ = selmenu->name;

            if (selmenu->flags & MPFLAG_ISMAIN)
                break;

            if ((size_t)(nsptr - namestack) >= nssize)
                {
                    leader = "...";
                    break;
                }

            selmenu = menu_get_page(selmenu->parent);
        }

    ui_puts(leader);

    while (nsptr - namestack > 0)
        printf("/%s", *--nsptr);

    ui_puts("> ");
}
#else // FEATURE_FANCYPROMPT
{
    IGNORE_VARIABLE(selmenu);
    ui_puts("> ");
}
#endif // FEATURE_FANCYPROMPT

/* callback: int (*) (struct menu_page *selmenu, union userinput *uinput)
 *           (not actually a callback)
 * name:     ui_call_*
 * @selmenu: menu page pointer
 * @uinput:  getter/handler input
 * return:   entryfnret / getterfnret / handlerfnret
 *           ({ENTRY,GETTER,HANDLER}FN_*)
 * desc:     calls to entryfn, getterfn and handlerfn go through these
 *           wrapper functions, which set up the state for the call
 */

int
ui_call_entry ( struct menu_page *selmenu )
{
    if (!(ui_getflags() & UIFLAG_INTERACTIVE || selmenu->flags & MPFLAG_QUIET_VISIBLE))
        goto exit_quietmode;

    try_clearscreen(selmenu);
    try_showheader(selmenu);
    return selmenu->entry_callback(selmenu);

exit_quietmode:
    return ENTRYFN_CONTINUE;
}

int
ui_call_getter (
    struct menu_page * selmenu ,
    union userinput *  uinput  )
{
    return selmenu->getter_callback(selmenu, uinput);
}

int
ui_call_handler (
    struct menu_page * selmenu ,
    union userinput *  uinput  )
{
    return selmenu->handler_callback(selmenu, uinput);
}

/* name:     ui_exec_menupage
 * @selmenu: menu page pointer
 * return:   1 or 0
 * context:  called by ui_menuloop until it returns 0
 * desc:     executes the entryfn, getterfn and handlerfn callbacks in
 *           selmenu in order. Return value tells caller whether to
 *           continue (1) or quit (0) the program.
 */
int
ui_exec_menupage ( struct menu_page *selmenu )
{
    static union userinput uinput;
    int getterret;
    int handlerret;

    ui_call_entry(selmenu);
    getterret = ui_call_getter(selmenu, &uinput);

    if (getterret == GETTERFN_EOF)
        goto exit_quit;

    if (getterret == GETTERFN_CANCEL)
        goto exit_returntoparent;

    handlerret = ui_call_handler(selmenu, &uinput);

    if (handlerret == HANDLERFN_RETURNTOPARENT)
        goto exit_returntoparent;

    return 1;

exit_returntoparent:
    {
        ui_set_pageid(selmenu->parent);
        return 1;
    }

exit_quit:
    return 0;
}

/* name:    ui_menuloop
 * context: uistate.pageid determines which page is loaded at every call
 * desc:    calls ui_exec_menupage until it returns 0
 */
void
ui_menuloop ( void )
{
    struct menu_page *selmenu;

    while ((selmenu = menu_get_page(uistate.pageid)) != NULL)
        if (!ui_exec_menupage(selmenu))
            return;
}
