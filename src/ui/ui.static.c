static struct uistate uistate = {
    .pageid = MENUPAGE_INTRO,
    .flags = UIFLAG_WARN_NARPAS | UIFLAG_INTERACTIVE,
};

#if FEATURE_NARPAS
static char const *narpas_warn_str = "\n" UI_NARPAS_WARN_STR;
#endif // FEATURE_NARPAS

/* callback: void (*) (struct menu_page *selmenu)
 *           (not actually a callback)
 * name:     try_*
 * @selmenu: menu page pointer
 * context:  called by ui_call_entry
 * desc:     these try to do what their name says, checking for
 *           conditions that prevent said thing from happening
 *
 * 1. Do not FEATURE_COMPACT this. It's preferable to not clear the
 *    screen. When FEATURE_COMPACT is set to 0, it's impossible to set
 *    the UIFLAG_COMPACT flag, except through one exception
 *    mpassw.static.c/callback_exit, which is why this should be
 *    preseved regardless of FEATURE_COMPACT
 */

static inline void
try_clearscreen ( struct menu_page *selmenu )
{
    if (selmenu->flags & MPFLAG_NOCLEAR_ONCE)
        goto exit_noclear_once;

    if (selmenu->flags & MPFLAG_NOCLEAR)
        goto exit_noclear;

    // 1
    if (ui_getflags() & UIFLAG_COMPACT)
        goto exit_noclear;

    ui_clearscreen();
    return;

exit_noclear_once:
    {
        selmenu->flags &= ~MPFLAG_NOCLEAR_ONCE;
        return;
    }

exit_noclear:
    return;
}

static inline void
try_showheader ( struct menu_page *selmenu )
{
    if (!(selmenu->flags & MPFLAG_SHOWHEADER))
        return;

    if (!(ui_getflags() & UIFLAG_INTERACTIVE))
        return;

    // 1
    if (ui_getflags() & UIFLAG_COMPACT)
        return;

#if FEATURE_VERBOSEMENU
    printf("\n== %s ==\n\n", selmenu->name);
#endif // FEATURE_VERBOSEMENU
}
