## Tabls of contents
* function declarations
* function definitions
* function documentation
* macros
* if, while, for
* switch
* goto
* headers and includes
* other
*   `u8 *` vs `char *`

## function declarations

should be aligned at the name and argument-list

```c
type       name       (argument-list);
longertype longername (longer argument-list);
```

Although if there's a group of functions that serve a related function (like callbacks), then they can be grouped away
from other function prototypes.

They may sometimes be prefixed with comments like `/* -S */` to document any side effects they have. So far, that just
depends on the file.

## function definitions

functions should be defined like this:

```c
/* documentation (covered in next section)
 */
type
name (
    char *       arg1 ,
    size_t       arg2 ,
    char const * arg3 )
{
    // variables and constants

    // normal stuff

    // #if FEATURE_DEBUG stuff

    // exit_* labels
}
```

the argument names and `,)` should be aligned with a one space gap from the longest argument line.

For `(void)` functions and functions with a single argument, keep it same-line

```c
type
name ( void )
{
    ...
}

type
name ( type name )
{
    ...
}
```

if a function needs to have different definitions depending on the preprocessor, I prefer that the function body
including the curly braces be inside the `#if...#endif`, so that it's clear the entire function is being replaced.

```c
type
name ( type arg )
#ifdef SOMECONSTANT
{
    ...
}
#else
{
    ...
}
#endif
```

For unused variables, be sure to place `IGNORE_VARIABLE(unused);` at the top of the function. I consider them
in the same class as variable declarations, so they are to be grouped there with no gap between them and the first
variable. Or, if an `IGNORE_VARIABLE` is the only thing in that group, then a gap should be put between it and the first
statement. There is also `IGNORE_FUNCTION` and `IGNORE_LABEL`. They need be be used when the compiler complains about
something being unused. `IGNORE_FUNCTION` for example was introduced to deal with `populate_version_fragment`, which is
unused when VERSION is undefined.

## function documentation

for documentation, I do have a template:

```c
/* name:    somefn
 * @arg:    description of argument
 * return:  description of return value
 * context: notes about operating context and side effects, like
            modified arguments, state, etc.
 * desc:    brief description of what the function does
 *
 * 1. Note that appears in the function
 */
...
    --buflen; // 1 (the 1 refers to the `1` note above)
```

(do not bother documenting unused parameters)

For groups of similar small functions with a common purpose, such as callbacks, it's slightly different. 

callbacks: summarize fn type and namespace

```c
/* callback: argfn aka int (*) (char const *arg)
 * name:     opt_*
 * ...
 */
```

Just one comment to precede the whole group of functions, as it applies to all of them.

## macros

constants are one-liners, callables should be spread out over multiple lines for readability.

I've yet to come up with any kind of documentation format for macros, but expression macros should be wrapped in parens
so they they can accept a variety of non-side-effect-causing arguments, and statement macros should be wrapped in a `do
while(0)` block so that they can be used in one-liner if/while/for without issue.

```c
#define BUFSZ 4096

#define MIN(a, b) \
    ( (a) < (b) ? (a) : (b) )

#define LOGFERR(msg, ...) do { \
    fprintf(stderr, "%s/%s: " msg, __FILE__, __func__, __VA_ARGS__); \
while(0);
```

sometimes the macros can get more elaborate, like the macros used in `menu/*`. I consider it fine as long as the syntax
isn't being severely broken. The goal is to simplify and reduce repetitive constructs, not to write a whole new language

## if, while, for

Use GNU-style syntax but no tabs or weird alignment. These are the correct formats:

```c
if (expr)
    single statement

if (expr)
    {
        multiple statements
    }

```

the `{ ... }` is a block statement, and the child of the if statement, so it should be indented consistently

For `for`, I prefer that all three fields be filled out. Rather than leave a part blank, leave a void expression
`(void)0`.

```c
for (offset = 0; offset < BUFSZ; (void)0)
    {
        ...
        offset += some_function(...);
    }
```

`else` is a bit tricky, since I try not to use it (such constructs can usually be represented more simply with an early
return)

```c
if (expr)
    statement
else
    statement
```
```c
if (expr)
    {
        ...
    }
else
    {
        ...
    }
```

I'm not very consistent with the spacing between the else and if. They *should* be grouped together, as they are
directly related, or part of the same logical block. The else depends on the if, so they are part of the same overall
statement.

Under no circumstances should `else if` be used. Either chain some early returns, use a switch, or use a table and
parser. `else if` is a code smell.

## switch

I generally try to avoid switch, because it can almost always be replaced with data structures and a parser. But the
syntax is similar to if/while/for

```c
switch (value) {
    case someconstant:
        ...
        // break if stuff after the switch has to be executed, otherwise, return.
}
```

the `{` must be on the same line because a switch is not a function and the `{}` used in a switch is not a block
statement, and is part of the required syntax. The same applies to `do {...} while`, enum, struct, union, etc.

## goto

gotos should only jump forward, to a label named `exit_*`

The labels should all do what their name implies and return from the function, and the only possible way into them
should be to use goto.

labels are kind of similar to if statements. That is, one-liners are without brackets, but multiliners should have a
block statement as their child, and it should be indented GNU-style.

labels must not ever jump to other labels, even if it would save lines.

Do this:

```c
    ...
    return somevalue;

exit_somethirdfailure:
    return 0;

exit_continue:
    return 1;

exit_somefailure:
    {
        LOGERR("Something went wrong");
        return 0;
    }

exit_someotherfailure:
    return 0;
```

not this:

```c
    ...
    return somevalue;

exit_somethirdfailure:
    goto exit_someotherfailure; // No.

exit_continue:
    return 1;

exit_somefailure:
    LOGERR("Something went wrong");
    // NO.
exit_someotherfailure:
    return 0;
```

## headers and includes

Every header should have an include guard named after its full path (relative to `src` or `src/include`) and a
self-include section for static objects. It's pointless to have a second header for the static stuff. It's not like
we're trying to hide it from the reader. In my opinion, it's better to have it all in the header so that the header acts
as an overview of the entire module.

```c
// include/dir/somefile.h
#ifndef DIR_SOMEFILE_H
#define DIR_SOMEFILE_H

...

#endif // DIR_SOMEFILE_H

// three-line gap

#ifdef DIR_SOMEFILE_C

...

#endif // DIR_SOMEFILE_C
```

Note that the static section is *outside* of the include guard.

Includes are ordered in a specific way: Library includes, gap, local includes, gap, self-includes. In ascending order of
locality. By convention, standard and external library includes use `<>` while locals and selfs use `""`. AFAIK
the preprocessor treats them the same, so the reason you use `<>` for external libraries is for the benefit of the
reader.

While the exact order within groups isn't important, I have been making an effort to at least roughly alphabetize them.

```c
// dir/somefile.c
#include <stdio.h>
#include <string.h>

#include "data/data.h"
#include "data/codec.h"
#include "ui/ui.h"

#define DIR_SOMEFILE_C // this should ONLY appear in somefile.c
#include "dir/somefile.h"
#include "dir/somefile.static.c"
```

## other

### `u8 *` vs `char *`

While this is not a strong convention, `u8 *` is typically used when dealing with passwords and game data.

This is because those are not strings, but non-terminated buffers that have a length associated.
