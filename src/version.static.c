/* Returns the number of bytes written
 * A return value of 0 indicates failure
 */
/* name:      populate_version_fragment
 * @buffer:   buffer offset
 * @size:     available space
 * @fragment: number provided by VERSION macro
 * return:    number of bytes written or 0
 * context:   mutates argument
 * desc:      populates buffer with part of the version number. On error
 *            (indicated by snprintf returning < 0 or >= size), 0 is
 *            returned
 */
static inline size_t
populate_version_fragment (
    char * buffer   ,
    size_t size     ,
    int    fragment )
{
    int nwritten = snprintf(buffer, size, "%u.", fragment);

    if (nwritten < 0)
        goto exit_snprintferror;

    if (nwritten >= (int)size)
        goto exit_notenoughspace;

    return nwritten;

exit_snprintferror:
    {
        LOGERRNO("I/O error in snprintf\n");
        return 0;
    }

exit_notenoughspace:
    {
        LOGERR("Not enough space in buffer.\n");
        return 0;
    }
}
