#include <stdio.h>
#include <string.h>

#include "menu/getterdata.h"
#include "ui/ui.h"
#include "util/util.h"

#define MENU_GETTERFN_C
#include "menu/getterfn.h"

/* callback: getterfn aka int (*) (struct menu_page *selmenu,
 *                                 union userinput *uinput)
 * name:     getterfn_*
 * @selmenu: menu page pointer
 * @uinput:  where user input will be stored
 * return:   getterfnret (GETTERFN_*)
 * context:  called by menu controller loop after getterfn and before
 *           handlerfn
 * desc:     a getterfn's job is to get input from the user and prepare
 *           it for the handler. The GENERIC_GETTERFN_SETUP macro does
 *           the bulk of the work, showing the prompt, obtaining the
 *           line and checking for eof/error and empty line (in which
 *           case it will return GETTERFN_EOF or GETTERFN_CANCEL
 *           respectively to tell the program to quit or back out to the
 *           parent menu, respectively)
 */

int
getterfn_generic_u32 (
    struct menu_page * selmenu ,
    union userinput *  uinput  )
{
    static char digitbuf[10];
    size_t nread;

    GENERIC_GETTERFN_SETUP(digitbuf, 10);
    uinput->_u32 = u32_parse_number(digitbuf, nread);
    return GETTERFN_CONTINUE;
}

int
getterfn_generic_char_or_u32 (
    struct menu_page * selmenu ,
    union userinput *  uinput  )
{
    static char buffer[10];
    char const *children_char = selmenu->getterdata->children_char;
    char const *match;
    size_t nread;

    GENERIC_GETTERFN_SETUP(buffer, 10);
    match = strchr(children_char, buffer[0]);

    if (match == NULL)
        uinput->_u32 = u32_parse_number(buffer, nread);
    else
        uinput->_u32 = (u32)(match - children_char);

    return GETTERFN_CONTINUE;
}

int
getterfn_generic_line (
    struct menu_page * selmenu ,
    union userinput *  uinput  )
{
    size_t nread;

    memset(uinput->_line, 0, 32);
    GENERIC_GETTERFN_SETUP(uinput->_line, 32);
    return GETTERFN_CONTINUE;
}

int
getterfn_generic_waitcontinue (
    struct menu_page * unused  ,
    union userinput *  unused2 )
{
    IGNORE_VARIABLE(unused);
    IGNORE_VARIABLE(unused2);

    ui_waitcontinue();
    return GETTERFN_CONTINUE;
}

int
getterfn_generic_none (
    struct menu_page * unused  ,
    union userinput *  unused2 )
{
    IGNORE_VARIABLE(unused);
    IGNORE_VARIABLE(unused2);

    return GETTERFN_CONTINUE;
}
