static struct menu_page menus[MENUPAGE_END] = {
    /* [thisid] = {
     *     name, parentid, flags,
     *     fn,   fn,       fn,
     *     data, data,     data
     * }
     */

    [MENUPAGE_INTRO] = {
        "Intro",                   MENUPAGE_TUTORIAL_HINT,        MPFLAG_NOCLEAR,
        entryfn_generic_putsvfmt,  getterfn_generic_none,         handlerfn_generic_none,
        &entrydata_intro,          NULL,                          NULL,
    },

    [MENUPAGE_MAIN] = {
        "Main",                    MENUPAGE_QUIT_CONFIRM,         FLAGS_MAIN,
        entryfn_generic_putsvfmt,  getterfn_generic_char_or_u32,  handlerfn_generic_directory,
        &entrydata_main,           &getterdata_main,              &handlerdata_main,
    },

    [MENUPAGE_QUIT_CONFIRM] = {
        "Confirm Quit",            MENUPAGE_QUIT,                 MPFLAG_NOCLEAR,
        entryfn_generic_putsv,     getterfn_generic_char_or_u32,  handlerfn_generic_directory,
        &entrydata_quit_confirm,   &getterdata_quit_confirm,      &handlerdata_quit_confirm,
    },

    [MENUPAGE_INPUT_PASSWORD] = {
        "Input Password",          MENUPAGE_MAIN,                 MPFLAG_NOCLEAR,
        entryfn_generic_putsv,     getterfn_generic_line,         handlerfn_input_password,
        &entrydata_input_password, NULL,                          NULL,
    },

    [MENUPAGE_SHOW_PASSWORD] = {
        "Show Password",           MENUPAGE_MAIN,                 FLAGS_QUIET_PRINTOUT,
        entryfn_generic_putsvfmt,  getterfn_generic_waitcontinue, handlerfn_generic_none,
        &entrydata_show_password,  NULL,                          NULL,
    },

    [MENUPAGE_INPUT_HEX] = {
        "Input Hex",               MENUPAGE_MAIN,                 MPFLAG_NOCLEAR,
        entryfn_generic_putsvfmt,  getterfn_generic_line,         handlerfn_input_hex,
        &entrydata_input_hex,      NULL,                          NULL,
    },

    [MENUPAGE_EDIT_BYTES] = {
        "Edit Bytes",              MENUPAGE_MAIN,                 MPFLAG_SHOWHEADER,
        entryfn_generic_putsvfmt,  getterfn_generic_u32,          handlerfn_generic_directory,
        &entrydata_edit_bytes,     NULL,                          &handlerdata_edit_bytes,
    },

    [MENUPAGE_SHOW_SUMMARY] = {
        "Show Summary",            MENUPAGE_MAIN,                 FLAGS_QUIET_PRINTOUT,
        entryfn_generic_putsvfmt,  getterfn_generic_waitcontinue, handlerfn_generic_none,
        &entrydata_show_summary,   NULL,                          NULL,
    },

    [MENUPAGE_SET_KEY] = {
        "Set Encryption Key",      MENUPAGE_MAIN,                 MPFLAG_NOCLEAR,
        entryfn_generic_putsv,     getterfn_generic_u32,          handlerfn_set_key,
        &entrydata_set_key,        NULL,                          NULL,
    },

#if FEATURE_TUTORIAL
    [MENUPAGE_TUTORIAL_HINT] = {
        "Tutorial Hint",           MENUPAGE_MAIN,                 MPFLAG_NOCLEAR,
        entryfn_generic_putsvfmt,  getterfn_generic_none,         handlerfn_generic_none,
        &entrydata_tutorial_hint,  NULL,                          NULL,
    },

    [MENUPAGE_TUTORIAL] = {
        "Tutorial",                MENUPAGE_MAIN,                 MPFLAG_SHOWHEADER,
        entryfn_generic_putsv,     getterfn_generic_waitcontinue, handlerfn_tutorial,
        &entrydata_tutorial,       NULL,                          NULL,
    },

    [MENUPAGE_TUTORIAL_OOPS] = {
        "Oops!",                   MENUPAGE_TUTORIAL,             MPFLAG_SHOWHEADER,
        entryfn_generic_putsv,     getterfn_generic_waitcontinue, handlerfn_generic_none,
        &entrydata_tutorial_oops,  NULL,                          NULL,
    },
#else // FEATURE_TUTORIAL
    /*                          MENUPAGE_*     MENUPAGE_* handlerfn_* */
    /* SIMPLEACTION             thisid         parentid   handler     */
    GENERIC_MENU_SIMPLEACTION ( TUTORIAL_HINT, MAIN,      generic_none ),
    GENERIC_MENU_SIMPLEACTION ( TUTORIAL,      MAIN,      generic_none ),
    GENERIC_MENU_SIMPLEACTION ( TUTORIAL_OOPS, MAIN,      generic_none ),
#endif // FEATURE_TUTORIAL

    [MENUPAGE_LOCATION] = {
        "Location",                MENUPAGE_MAIN,                 MPFLAG_SHOWHEADER,
        entryfn_generic_putsvfmt,  getterfn_generic_char_or_u32,  handlerfn_location,
        &entrydata_location,       &getterdata_location,          NULL,
    },

    [MENUPAGE_AGE] = {
        "Game Age",                MENUPAGE_MAIN,                 MPFLAG_SHOWHEADER,
        entryfn_generic_putsvfmt,  getterfn_generic_u32,          handlerfn_age,
        &entrydata_age,            NULL,                          NULL,
    },

    [MENUPAGE_MISSILES_AMMO] = {
        "Ammo",                    MENUPAGE_MISSILES,             MPFLAG_NOCLEAR,
        entryfn_generic_putsv,     getterfn_generic_u32,          handlerfn_missiles_ammo,
        &entrydata_missiles_ammo,  NULL,                          NULL,
    },

    /*                       MENUPAGE_* MENUPAGE_*                  {entry,getter,handler}data_* */
    /* DIRECTORY             thisid     parentid   name             data                         */
    GENERIC_MENU_DIRECTORY ( ITEMS,     MAIN,      "Items",         items    ),
    GENERIC_MENU_DIRECTORY ( MISSILES,  MAIN,      "Missiles",      missiles ),
    GENERIC_MENU_DIRECTORY ( ETANKS,    MAIN,      "Energy Tanks",  etanks   ),
    GENERIC_MENU_DIRECTORY ( DOORS,     MAIN,      "Doors",         doors    ),
    GENERIC_MENU_DIRECTORY ( BOSSES,    MAIN,      "Bosses",        bosses   ),
    GENERIC_MENU_DIRECTORY ( UNUSED,    MAIN,      "Unused Values", unused   ),

    /*                          MENUPAGE_*             MENUPAGE_* handlerfn_* */
    /* SIMPLEACTION             thisid                 parentid   handler     */
    GENERIC_MENU_SIMPLEACTION ( SET_RANDOM_KEY,        MAIN,      set_random_key        ),
#if FEATURE_NARPAS
    GENERIC_MENU_SIMPLEACTION ( TOGGLE_NARPAS_WARNING, MAIN,      toggle_narpas_warning ),
#else // FEATURE_NARPAS
    GENERIC_MENU_SIMPLEACTION ( TOGGLE_NARPAS_WARNING, MAIN,      generic_none          ),
#endif // FEATURE_NARPAS
#if FEATURE_COMPACT
    GENERIC_MENU_SIMPLEACTION ( COMPACT,               MAIN,      compact               ),
    GENERIC_MENU_SIMPLEACTION ( NOCOMPACT,             MAIN,      nocompact             ),
#else // FEATURE_COMPACT
    GENERIC_MENU_SIMPLEACTION ( COMPACT,               MAIN,      generic_none          ),
    GENERIC_MENU_SIMPLEACTION ( NOCOMPACT,             MAIN,      generic_none          ),
#endif // FEATURE_COMPACT

    /* BULK_TOGGLE,            MENUPAGE_*                MENUPAGE_*              handlerdata_bulk_* */
    /* BULK_SET                thisid                    parentid                handlerdata        */
    GENERIC_MENU_BULK_TOGGLE ( ITEMS_TOGGLE_ALL_STEP1,   ITEMS_TOGGLE_OWNED,     items_taken       ),
    GENERIC_MENU_BULK_TOGGLE ( ITEMS_TOGGLE_TAKEN,       ITEMS,                  items_taken       ),
    GENERIC_MENU_BULK_TOGGLE ( ITEMS_TOGGLE_OWNED,       ITEMS,                  items_owned       ),
    GENERIC_MENU_BULK_TOGGLE ( MISSILES_TOGGLE_ALL,      MISSILES,               missiles_all      ),
    GENERIC_MENU_BULK_TOGGLE ( MISSILES_TOGGLE_BRINSTAR, MISSILES,               missiles_brinstar ),
    GENERIC_MENU_BULK_TOGGLE ( MISSILES_TOGGLE_NORFAIR,  MISSILES,               missiles_norfair  ),
    GENERIC_MENU_BULK_TOGGLE ( MISSILES_TOGGLE_KRAID,    MISSILES,               missiles_kraid    ),
    GENERIC_MENU_BULK_TOGGLE ( MISSILES_TOGGLE_RIDLEY,   MISSILES,               missiles_ridley   ),
    GENERIC_MENU_BULK_TOGGLE ( ETANKS_TOGGLE_ALL,        ETANKS,                 etanks_all        ),
    GENERIC_MENU_BULK_TOGGLE ( ETANKS_TOGGLE_BRINSTAR,   ETANKS,                 etanks_brinstar   ),
    GENERIC_MENU_BULK_TOGGLE ( ETANKS_TOGGLE_NORFAIR,    ETANKS,                 etanks_norfair    ),
    GENERIC_MENU_BULK_TOGGLE ( ETANKS_TOGGLE_KRAID,      ETANKS,                 etanks_kraid      ),
    GENERIC_MENU_BULK_TOGGLE ( ETANKS_TOGGLE_RIDLEY,     ETANKS,                 etanks_ridley     ),
    GENERIC_MENU_BULK_TOGGLE ( DOORS_TOGGLE_ALL_STEP1,   DOORS_TOGGLE_ALL_STEP2, doors_doors       ),
    GENERIC_MENU_BULK_TOGGLE ( DOORS_TOGGLE_ALL_STEP2,   DOORS_TOGGLE_STATUES,   doors_zebetites   ),
    GENERIC_MENU_BULK_TOGGLE ( DOORS_TOGGLE_DOORS,       DOORS,                  doors_doors       ),
    GENERIC_MENU_BULK_TOGGLE ( DOORS_TOGGLE_ZEBETITES,   DOORS,                  doors_zebetites   ),
    GENERIC_MENU_BULK_TOGGLE ( DOORS_TOGGLE_STATUES,     DOORS,                  doors_statues     ),
    GENERIC_MENU_BULK_TOGGLE ( UNUSED_TOGGLE_ALL,        UNUSED,                 unused            ),
    GENERIC_MENU_BULK_SET    ( ITEMS_SET_ALL_STEP1,      ITEMS_SET_OWNED,        items_taken       ),
    GENERIC_MENU_BULK_SET    ( ITEMS_SET_TAKEN,          ITEMS,                  items_taken       ),
    GENERIC_MENU_BULK_SET    ( ITEMS_SET_OWNED,          ITEMS,                  items_owned       ),
    GENERIC_MENU_BULK_SET    ( MISSILES_SET_ALL,         MISSILES,               missiles_all      ),
    GENERIC_MENU_BULK_SET    ( MISSILES_SET_BRINSTAR,    MISSILES,               missiles_brinstar ),
    GENERIC_MENU_BULK_SET    ( MISSILES_SET_NORFAIR,     MISSILES,               missiles_norfair  ),
    GENERIC_MENU_BULK_SET    ( MISSILES_SET_KRAID,       MISSILES,               missiles_kraid    ),
    GENERIC_MENU_BULK_SET    ( MISSILES_SET_RIDLEY,      MISSILES,               missiles_ridley   ),
    GENERIC_MENU_BULK_SET    ( ETANKS_SET_ALL,           ETANKS,                 etanks_all        ),
    GENERIC_MENU_BULK_SET    ( ETANKS_SET_BRINSTAR,      ETANKS,                 etanks_brinstar   ),
    GENERIC_MENU_BULK_SET    ( ETANKS_SET_NORFAIR,       ETANKS,                 etanks_norfair    ),
    GENERIC_MENU_BULK_SET    ( ETANKS_SET_KRAID,         ETANKS,                 etanks_kraid      ),
    GENERIC_MENU_BULK_SET    ( ETANKS_SET_RIDLEY,        ETANKS,                 etanks_ridley     ),
    GENERIC_MENU_BULK_SET    ( DOORS_SET_ALL_STEP1,      DOORS_SET_ALL_STEP2,    doors_doors       ),
    GENERIC_MENU_BULK_SET    ( DOORS_SET_ALL_STEP2,      DOORS_SET_STATUES,      doors_zebetites   ),
    GENERIC_MENU_BULK_SET    ( DOORS_SET_DOORS,          DOORS,                  doors_doors       ),
    GENERIC_MENU_BULK_SET    ( DOORS_SET_ZEBETITES,      DOORS,                  doors_zebetites   ),
    GENERIC_MENU_BULK_SET    ( DOORS_SET_STATUES,        DOORS,                  doors_statues     ),
    GENERIC_MENU_BULK_SET    ( UNUSED_SET_ALL,           UNUSED,                 unused            ),

    /*                    MENUPAGE_EDIT_BYTES_*, */
    /*                    handlerdata_setbyte_*  */
    /* SETBYTE            byte ID                */
    GENERIC_MENU_SETBYTE ( 0  ),
    GENERIC_MENU_SETBYTE ( 1  ),
    GENERIC_MENU_SETBYTE ( 2  ),
    GENERIC_MENU_SETBYTE ( 3  ),
    GENERIC_MENU_SETBYTE ( 4  ),
    GENERIC_MENU_SETBYTE ( 5  ),
    GENERIC_MENU_SETBYTE ( 6  ),
    GENERIC_MENU_SETBYTE ( 7  ),
    GENERIC_MENU_SETBYTE ( 8  ),
    GENERIC_MENU_SETBYTE ( 9  ),
    GENERIC_MENU_SETBYTE ( 10 ),
    GENERIC_MENU_SETBYTE ( 11 ),
    GENERIC_MENU_SETBYTE ( 12 ),
    GENERIC_MENU_SETBYTE ( 13 ),
    GENERIC_MENU_SETBYTE ( 14 ),
    GENERIC_MENU_SETBYTE ( 15 ),
    GENERIC_MENU_SETBYTE ( 16 ),
    GENERIC_MENU_SETBYTE ( 17 ),

    /*                       MENUPAGE_*               MENUPAGE_* handlerdata_togglebit_* */
    /* TOGGLEBIT             thisid                   parentid   handlerdata             */
    GENERIC_MENU_TOGGLEBIT ( ITEMS_TAKEN_MARUMARI,    ITEMS,     items_taken_0           ),
    GENERIC_MENU_TOGGLEBIT ( ITEMS_TAKEN_BOMBS,       ITEMS,     items_taken_1           ),
    GENERIC_MENU_TOGGLEBIT ( ITEMS_TAKEN_VARIA,       ITEMS,     items_taken_2           ),
    GENERIC_MENU_TOGGLEBIT ( ITEMS_TAKEN_HIGHJUMP,    ITEMS,     items_taken_3           ),
    GENERIC_MENU_TOGGLEBIT ( ITEMS_TAKEN_SCREWATTACK, ITEMS,     items_taken_4           ),
    GENERIC_MENU_TOGGLEBIT ( ITEMS_OWNED_MARUMARI,    ITEMS,     items_owned_0           ),
    GENERIC_MENU_TOGGLEBIT ( ITEMS_OWNED_BOMBS,       ITEMS,     items_owned_1           ),
    GENERIC_MENU_TOGGLEBIT ( ITEMS_OWNED_VARIA,       ITEMS,     items_owned_2           ),
    GENERIC_MENU_TOGGLEBIT ( ITEMS_OWNED_HIGHJUMP,    ITEMS,     items_owned_3           ),
    GENERIC_MENU_TOGGLEBIT ( ITEMS_OWNED_SCREWATTACK, ITEMS,     items_owned_4           ),
    GENERIC_MENU_TOGGLEBIT ( ITEMS_OWNED_LONGBEAM,    ITEMS,     items_owned_5           ),
    GENERIC_MENU_TOGGLEBIT ( ITEMS_OWNED_WAVEBEAM,    ITEMS,     items_owned_6           ),
    GENERIC_MENU_TOGGLEBIT ( ITEMS_OWNED_ICEBEAM,     ITEMS,     items_owned_7           ),
    GENERIC_MENU_TOGGLEBIT ( ITEMS_OWNED_SWIMSUIT,    ITEMS,     items_owned_8           ),
    GENERIC_MENU_TOGGLEBIT ( MISSILES_TAKEN_BRIN1,    MISSILES,  missiles_brin1          ),
    GENERIC_MENU_TOGGLEBIT ( MISSILES_TAKEN_BRIN2,    MISSILES,  missiles_brin2          ),
    GENERIC_MENU_TOGGLEBIT ( MISSILES_TAKEN_NORF1,    MISSILES,  missiles_norf1          ),
    GENERIC_MENU_TOGGLEBIT ( MISSILES_TAKEN_NORF2,    MISSILES,  missiles_norf2          ),
    GENERIC_MENU_TOGGLEBIT ( MISSILES_TAKEN_NORF3,    MISSILES,  missiles_norf3          ),
    GENERIC_MENU_TOGGLEBIT ( MISSILES_TAKEN_NORF4,    MISSILES,  missiles_norf4          ),
    GENERIC_MENU_TOGGLEBIT ( MISSILES_TAKEN_NORF5,    MISSILES,  missiles_norf5          ),
    GENERIC_MENU_TOGGLEBIT ( MISSILES_TAKEN_NORF6,    MISSILES,  missiles_norf6          ),
    GENERIC_MENU_TOGGLEBIT ( MISSILES_TAKEN_NORF7,    MISSILES,  missiles_norf7          ),
    GENERIC_MENU_TOGGLEBIT ( MISSILES_TAKEN_NORF8,    MISSILES,  missiles_norf8          ),
    GENERIC_MENU_TOGGLEBIT ( MISSILES_TAKEN_NORF9,    MISSILES,  missiles_norf9          ),
    GENERIC_MENU_TOGGLEBIT ( MISSILES_TAKEN_NORF10,   MISSILES,  missiles_norf10         ),
    GENERIC_MENU_TOGGLEBIT ( MISSILES_TAKEN_NORF11,   MISSILES,  missiles_norf11         ),
    GENERIC_MENU_TOGGLEBIT ( MISSILES_TAKEN_NORF12,   MISSILES,  missiles_norf12         ),
    GENERIC_MENU_TOGGLEBIT ( MISSILES_TAKEN_KRAID1,   MISSILES,  missiles_kraid1         ),
    GENERIC_MENU_TOGGLEBIT ( MISSILES_TAKEN_KRAID2,   MISSILES,  missiles_kraid2         ),
    GENERIC_MENU_TOGGLEBIT ( MISSILES_TAKEN_KRAID3,   MISSILES,  missiles_kraid3         ),
    GENERIC_MENU_TOGGLEBIT ( MISSILES_TAKEN_KRAID4,   MISSILES,  missiles_kraid4         ),
    GENERIC_MENU_TOGGLEBIT ( MISSILES_TAKEN_RIDLEY1,  MISSILES,  missiles_ridley1        ),
    GENERIC_MENU_TOGGLEBIT ( MISSILES_TAKEN_RIDLEY2,  MISSILES,  missiles_ridley2        ),
    GENERIC_MENU_TOGGLEBIT ( MISSILES_TAKEN_RIDLEY3,  MISSILES,  missiles_ridley3        ),
    GENERIC_MENU_TOGGLEBIT ( ETANKS_BRIN1,            ETANKS,    etanks_brin1            ),
    GENERIC_MENU_TOGGLEBIT ( ETANKS_BRIN2,            ETANKS,    etanks_brin2            ),
    GENERIC_MENU_TOGGLEBIT ( ETANKS_BRIN3,            ETANKS,    etanks_brin3            ),
    GENERIC_MENU_TOGGLEBIT ( ETANKS_NORF1,            ETANKS,    etanks_norf1            ),
    GENERIC_MENU_TOGGLEBIT ( ETANKS_KRAID1,           ETANKS,    etanks_kraid1           ),
    GENERIC_MENU_TOGGLEBIT ( ETANKS_KRAID2,           ETANKS,    etanks_kraid2           ),
    GENERIC_MENU_TOGGLEBIT ( ETANKS_RIDLEY1,          ETANKS,    etanks_ridley1          ),
    GENERIC_MENU_TOGGLEBIT ( ETANKS_RIDLEY2,          ETANKS,    etanks_ridley2          ),
    GENERIC_MENU_TOGGLEBIT ( DOORS_RED_LONGBEAM,      DOORS,     doors_red_longbeam      ),
    GENERIC_MENU_TOGGLEBIT ( DOORS_RED_TOURIANBRIDGE, DOORS,     doors_red_tourianbridge ),
    GENERIC_MENU_TOGGLEBIT ( DOORS_RED_BOMBS,         DOORS,     doors_red_bombs         ),
    GENERIC_MENU_TOGGLEBIT ( DOORS_RED_ICEBEAM1,      DOORS,     doors_red_icebeam1      ),
    GENERIC_MENU_TOGGLEBIT ( DOORS_RED_VARIA,         DOORS,     doors_red_varia         ),
    GENERIC_MENU_TOGGLEBIT ( DOORS_RED_ICEBEAM2,      DOORS,     doors_red_icebeam2      ),
    GENERIC_MENU_TOGGLEBIT ( DOORS_RED_HIGHJUMP,      DOORS,     doors_red_highjump      ),
    GENERIC_MENU_TOGGLEBIT ( DOORS_RED_SCREWATTACK,   DOORS,     doors_red_screwattack   ),
    GENERIC_MENU_TOGGLEBIT ( DOORS_RED_WAVEBEAM,      DOORS,     doors_red_wavebeam      ),
    GENERIC_MENU_TOGGLEBIT ( DOORS_RED_KRAID1,        DOORS,     doors_red_kraid1        ),
    GENERIC_MENU_TOGGLEBIT ( DOORS_RED_KRAID2,        DOORS,     doors_red_kraid2        ),
    GENERIC_MENU_TOGGLEBIT ( DOORS_RED_KRAID3,        DOORS,     doors_red_kraid3        ),
    GENERIC_MENU_TOGGLEBIT ( DOORS_RED_KRAID4,        DOORS,     doors_red_kraid4        ),
    GENERIC_MENU_TOGGLEBIT ( DOORS_RED_KRAID5,        DOORS,     doors_red_kraid5        ),
    GENERIC_MENU_TOGGLEBIT ( DOORS_RED_RIDLEY,        DOORS,     doors_red_ridley        ),
    GENERIC_MENU_TOGGLEBIT ( DOORS_YELLOW_RIDLEY,     DOORS,     doors_yellow_ridley     ),
    GENERIC_MENU_TOGGLEBIT ( DOORS_YELLOW_TOURIAN,    DOORS,     doors_yellow_tourian    ),
    GENERIC_MENU_TOGGLEBIT ( DOORS_RED_TOURIAN1,      DOORS,     doors_red_tourian1      ),
    GENERIC_MENU_TOGGLEBIT ( DOORS_RED_TOURIAN2,      DOORS,     doors_red_tourian2      ),
    GENERIC_MENU_TOGGLEBIT ( DOORS_ZEBETITE1,         DOORS,     doors_zebetite1         ),
    GENERIC_MENU_TOGGLEBIT ( DOORS_ZEBETITE2,         DOORS,     doors_zebetite2         ),
    GENERIC_MENU_TOGGLEBIT ( DOORS_ZEBETITE3,         DOORS,     doors_zebetite3         ),
    GENERIC_MENU_TOGGLEBIT ( DOORS_ZEBETITE4,         DOORS,     doors_zebetite4         ),
    GENERIC_MENU_TOGGLEBIT ( DOORS_ZEBETITE5,         DOORS,     doors_zebetite5         ),
    GENERIC_MENU_TOGGLEBIT ( DOORS_STAT_RIDLEY,       DOORS,     doors_stat_ridley       ),
    GENERIC_MENU_TOGGLEBIT ( DOORS_STAT_KRAID,        DOORS,     doors_stat_kraid        ),
    GENERIC_MENU_TOGGLEBIT ( BOSSES_MBRAIN,           BOSSES,    bosses_mbrain           ),
    GENERIC_MENU_TOGGLEBIT ( BOSSES_RIDLEY,           BOSSES,    bosses_ridley           ),
    GENERIC_MENU_TOGGLEBIT ( BOSSES_KRAID,            BOSSES,    bosses_kraid            ),
    GENERIC_MENU_TOGGLEBIT ( UNUSED_1,                UNUSED,    unused_1                ),
    GENERIC_MENU_TOGGLEBIT ( UNUSED_2,                UNUSED,    unused_2                ),
    GENERIC_MENU_TOGGLEBIT ( UNUSED_3,                UNUSED,    unused_3                ),
    GENERIC_MENU_TOGGLEBIT ( UNUSED_4,                UNUSED,    unused_4                ),
    GENERIC_MENU_TOGGLEBIT ( UNUSED_5,                UNUSED,    unused_5                ),
    GENERIC_MENU_TOGGLEBIT ( UNUSED_6,                UNUSED,    unused_6                ),
    GENERIC_MENU_TOGGLEBIT ( UNUSED_7,                UNUSED,    unused_7                ),
    GENERIC_MENU_TOGGLEBIT ( UNUSED_8,                UNUSED,    unused_8                ),
    GENERIC_MENU_TOGGLEBIT ( UNUSED_9,                UNUSED,    unused_9                ),
    GENERIC_MENU_TOGGLEBIT ( UNUSED_10,               UNUSED,    unused_10               ),
    GENERIC_MENU_TOGGLEBIT ( UNUSED_11,               UNUSED,    unused_11               ),
    GENERIC_MENU_TOGGLEBIT ( UNUSED_12,               UNUSED,    unused_12               ),
};

#if FEATURE_DEBUG

/* callback: datasizefn aka size_t (*) (int pageid)
 * name:     *data_size (I should probably rename them to datasizefn_*)
 * @pageid:  menu page ID to count
 * return:   size of attached data structures
 * context:  called by get_data_size
 * desc:     calculates sizes of relevant data structures for a single
 *           menu page
 */

static size_t
entrydata_size ( int pageid )
{
    struct entrydata *seldata = menu_get_page(pageid)->entrydata;
    size_t total = 0;

    if (seldata == NULL)
        return 0;

    if (seldata->text != NULL)
        for (int i = 0; seldata->text[i] != NULL; ++i)
            total += strlen(seldata->text[i]);

    if (seldata->fmtargs != NULL)
        for (int i = 0; seldata->fmtargs[i] != NULL; ++i)
            total += sizeof seldata->fmtargs[i];

    return total;
}

static size_t
getterdata_size ( int pageid )
{
    struct getterdata *seldata = menu_get_page(pageid)->getterdata;
    size_t total = 0;

    if (seldata == NULL)
        return 0;

    if (seldata->children_char != NULL)
        total += strlen(seldata->children_char);

    return total;
}

static size_t
handlerdata_size ( int pageid )
{
    struct handlerdata *seldata = menu_get_page(pageid)->handlerdata;
    size_t total = 0;

    if (seldata == NULL)
        return 0;

    if (seldata->children != NULL)
        for (int i = 0; seldata->children[i] != MENUPAGE_END; ++i)
            total += sizeof seldata->children[i];

    if (seldata->bitfield != NULL)
        total += sizeof *(seldata->bitfield);

    if (seldata->ranges != NULL)
        total += sizeof *(seldata->ranges);

    return total;
}

/* name:      get_data_size
 * @callback: datasizefn to execute on given page ID
 * return:    accumulated size of entry, getter, or handler data
 *            structures for all menu pages
 */
static inline size_t
get_data_size ( datasizefn callback )
{
    size_t total = 0;

    for (int i = 0; i < MENUPAGE_END; ++i)
        total += callback(i);

    return total;
}

#endif // FEATURE_DEBUG
