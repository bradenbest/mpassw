#if FEATURE_TUTORIAL

#include "localization/tutorial.h"

/* name:     tutorial_exec
 * @selpage: tutorial page (see above data structure)
 * return:   handlerfnret (HANDLERFN_*)
 * context:  called by handlerfn_tutorial to run the tutorial
 * dsc:      executes a tutorial page, which consists of a menu page ID
 *           and some flavor text
 */
static inline int
tutorial_exec ( struct tutorial_page *selpage )
{
    static union userinput uinput;
    struct menu_page *selmenu;
    int getter_action;

    if (!(ui_getflags() & UIFLAG_INTERACTIVE))
        return HANDLERFN_RETURNTOPARENT;

    if (selpage->pageid == MENUPAGE_QUIT)
        return HANDLERFN_RETURNTOPARENT;

    selmenu = menu_get_page(selpage->pageid);
    ui_call_entry(selmenu);
    ui_qputs("== Tutorial ==\n");
    ui_qputs(selpage->text);

    getter_action = selmenu->getter_callback(selmenu, &uinput);

    if (getter_action == GETTERFN_EOF || getter_action == GETTERFN_CANCEL)
        return HANDLERFN_RETURNTOPARENT;

    selmenu->handler_callback(selmenu, &uinput);
    return HANDLERFN_NOSETPAGEID;
}
#endif // FEATURE_TUTORIAL

/* name:       bulk_set_or_toggle
 * @selmenu:   menu page pointer
 * @operation: callback to execute on each mapping
 * return:     HANDLERFN_RETURNTOPARENT (see handlerfn_*)
 * desc:       sets range of mapped bits according to bitfieldrange in
 *             selmenu
 */
static inline int
bulk_set_or_toggle (
    struct menu_page * selmenu   ,
    bulkbitfn          operation )
{
    struct handlerdata *seldata = selmenu->handlerdata;
    struct bitfieldrange *selrange = seldata->ranges;

    for (u32 i = selrange->start; i <= selrange->end; ++i)
        operation(seldata->bitfield + i);

    return HANDLERFN_RETURNTOPARENT;
}
