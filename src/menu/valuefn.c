#include <stdio.h>
#include <string.h>

#include "data/data.h"
#include "data/gameagetime.h"
#include "data/bitfieldmap.h"
#include "ui/ui.h"
#include "version.h"

#define MENU_VALUEFN_C
#include "menu/valuefn.h"

/* callback: valuefn aka void (*) (struct valuefnret *vfnret)
 * name:     valuefn_*
 * @vfnret:  valuefnret (used to store the output)
 * context:  called by the splicerfns in util/putsvfmt.static.c.
 *           entrydata.static.c defines arrays of these for the
 *           entrytexts to reference with format specifiers
 *           mutates argument
 * desc:     a valuefn calculates a value (as of v1.12, that's either
 *           a u32 or a string) and stores it in its argument. The
 *           splicers know exactly how to extract the value, which is
 *           why the format specifier must match the function.
 *           nearly all of these functions are so simple that they are
 *           covered by a macro
 */

#if FEATURE_DEBUG
/* context: FEATURE_DEBUG only
 * desc:    used to terminate fmtargs lists while being distinct from
 *          NULL
 */
void
valuefn_fmtargs_terminator ( struct valuefnret *unused )
{
    IGNORE_VARIABLE(unused);
}
#endif // FEATURE_DEBUG

/* semicolons were removed to comply with -pedantic */

/*                    valuefn_*            */
/* U32                name fragment   expr */
VALUEFN_GENERIC_U32 ( shiftbyte,      data_get_shiftkey()                      )
VALUEFN_GENERIC_U32 ( items_taken,    bitfieldmap_total(bfmaps_items_taken)    )
VALUEFN_GENERIC_U32 ( items_owned,    bitfieldmap_total(bfmaps_items_owned)    )
VALUEFN_GENERIC_U32 ( missiles_total, bitfieldmap_total(bfmaps_missiles)       )
VALUEFN_GENERIC_U32 ( missiles_ammo,  data_get_missile_ammo()                  )
VALUEFN_GENERIC_U32 ( etanks,         bitfieldmap_total(bfmaps_etanks)         )
VALUEFN_GENERIC_U32 ( doors,          bitfieldmap_total(bfmaps_doors)          )
VALUEFN_GENERIC_U32 ( doors_zebetite, bitfieldmap_total(bfmaps_doors_zebetite) )
VALUEFN_GENERIC_U32 ( doors_stat,     bitfieldmap_total(bfmaps_doors_stat)     )
VALUEFN_GENERIC_U32 ( bosses,         bitfieldmap_total(bfmaps_bosses)         )
VALUEFN_GENERIC_U32 ( location,       data_get_location()                      )
VALUEFN_GENERIC_U32 ( game_age,       data_get_game_age()                      )
#if FEATURE_VERBOSEMENU
VALUEFN_GENERIC_U32 ( game_ticks,     data_get_game_ticks()                    )
#endif // FEATURE_VERBOSEMENU
VALUEFN_GENERIC_U32 ( unused,         bitfieldmap_total(bfmaps_unused)         )
#if FEATURE_NARPAS
VALUEFN_GENERIC_U32 ( narpas_warning, ui_getflags() & UIFLAG_WARN_NARPAS       )
#endif // FEATURE_NARPAS

/*                    valuefn_*             */
/* STR                name fragment    expr */
VALUEFN_GENERIC_STR ( location_str,    data_get_location_str()            )
#if FEATURE_NARPAS
VALUEFN_GENERIC_STR ( narpas_warn_str, ui_narpas_warn_str()               )
#endif // FEATURE_NARPAS
#if FEATURE_VERBOSEMENU
VALUEFN_GENERIC_STR ( age_ntsc,        gameagetime_str(NES_RFRATE_NTSC)   )
VALUEFN_GENERIC_STR ( age_pal,         gameagetime_str(NES_RFRATE_PAL)    )
VALUEFN_GENERIC_STR ( game_ending,     data_get_ending()                  )
#endif // FEATURE_VERBOSEMENU
VALUEFN_GENERIC_STR ( hexdump,         data_hexdump()                     )
VALUEFN_GENERIC_STR ( version,         version_str()                      )

/*                      valuefn_*                bfmaps_*               */
/* BFMAP                name fragment            bitfield        offset */
VALUEFN_GENERIC_BFMAP ( items_taken_marumari,    items_taken,    0 )
VALUEFN_GENERIC_BFMAP ( items_taken_bombs,       items_taken,    1 )
VALUEFN_GENERIC_BFMAP ( items_taken_varia,       items_taken,    2 )
VALUEFN_GENERIC_BFMAP ( items_taken_highjump,    items_taken,    3 )
VALUEFN_GENERIC_BFMAP ( items_taken_screwattack, items_taken,    4 )
VALUEFN_GENERIC_BFMAP ( items_owned_marumari,    items_owned,    0 )
VALUEFN_GENERIC_BFMAP ( items_owned_bombs,       items_owned,    1 )
VALUEFN_GENERIC_BFMAP ( items_owned_varia,       items_owned,    2 )
VALUEFN_GENERIC_BFMAP ( items_owned_highjump,    items_owned,    3 )
VALUEFN_GENERIC_BFMAP ( items_owned_screwattack, items_owned,    4 )
VALUEFN_GENERIC_BFMAP ( items_owned_longbeam,    items_owned,    5 )
VALUEFN_GENERIC_BFMAP ( items_owned_wavebeam,    items_owned,    6 )
VALUEFN_GENERIC_BFMAP ( items_owned_icebeam,     items_owned,    7 )
VALUEFN_GENERIC_BFMAP ( items_owned_swimsuit,    items_owned,    8 )
VALUEFN_GENERIC_BFMAP ( missiles_taken_brin1,    missiles,       0 )
VALUEFN_GENERIC_BFMAP ( missiles_taken_brin2,    missiles,       1 )
VALUEFN_GENERIC_BFMAP ( missiles_taken_norf1,    missiles,       2 )
VALUEFN_GENERIC_BFMAP ( missiles_taken_norf2,    missiles,       3 )
VALUEFN_GENERIC_BFMAP ( missiles_taken_norf3,    missiles,       4 )
VALUEFN_GENERIC_BFMAP ( missiles_taken_norf4,    missiles,       5 )
VALUEFN_GENERIC_BFMAP ( missiles_taken_norf5,    missiles,       6 )
VALUEFN_GENERIC_BFMAP ( missiles_taken_norf6,    missiles,       7 )
VALUEFN_GENERIC_BFMAP ( missiles_taken_norf7,    missiles,       8 )
VALUEFN_GENERIC_BFMAP ( missiles_taken_norf8,    missiles,       9 )
VALUEFN_GENERIC_BFMAP ( missiles_taken_norf9,    missiles,       10)
VALUEFN_GENERIC_BFMAP ( missiles_taken_norf10,   missiles,       11)
VALUEFN_GENERIC_BFMAP ( missiles_taken_norf11,   missiles,       12)
VALUEFN_GENERIC_BFMAP ( missiles_taken_norf12,   missiles,       13)
VALUEFN_GENERIC_BFMAP ( missiles_taken_kraid1,   missiles,       14)
VALUEFN_GENERIC_BFMAP ( missiles_taken_kraid2,   missiles,       15)
VALUEFN_GENERIC_BFMAP ( missiles_taken_kraid3,   missiles,       16)
VALUEFN_GENERIC_BFMAP ( missiles_taken_kraid4,   missiles,       17)
VALUEFN_GENERIC_BFMAP ( missiles_taken_ridley1,  missiles,       18)
VALUEFN_GENERIC_BFMAP ( missiles_taken_ridley2,  missiles,       19)
VALUEFN_GENERIC_BFMAP ( missiles_taken_ridley3,  missiles,       20)
VALUEFN_GENERIC_BFMAP ( etanks_brin1,            etanks,         0 )
VALUEFN_GENERIC_BFMAP ( etanks_brin2,            etanks,         1 )
VALUEFN_GENERIC_BFMAP ( etanks_brin3,            etanks,         2 )
VALUEFN_GENERIC_BFMAP ( etanks_norf1,            etanks,         3 )
VALUEFN_GENERIC_BFMAP ( etanks_kraid1,           etanks,         4 )
VALUEFN_GENERIC_BFMAP ( etanks_kraid2,           etanks,         5 )
VALUEFN_GENERIC_BFMAP ( etanks_ridley1,          etanks,         6 )
VALUEFN_GENERIC_BFMAP ( etanks_ridley2,          etanks,         7 )
VALUEFN_GENERIC_BFMAP ( doors_red_longbeam,      doors,          0 )
VALUEFN_GENERIC_BFMAP ( doors_red_tourianbridge, doors,          1 )
VALUEFN_GENERIC_BFMAP ( doors_red_bombs,         doors,          2 )
VALUEFN_GENERIC_BFMAP ( doors_red_icebeam1,      doors,          3 )
VALUEFN_GENERIC_BFMAP ( doors_red_varia,         doors,          4 )
VALUEFN_GENERIC_BFMAP ( doors_red_icebeam2,      doors,          5 )
VALUEFN_GENERIC_BFMAP ( doors_red_highjump,      doors,          6 )
VALUEFN_GENERIC_BFMAP ( doors_red_screwattack,   doors,          7 )
VALUEFN_GENERIC_BFMAP ( doors_red_wavebeam,      doors,          8 )
VALUEFN_GENERIC_BFMAP ( doors_red_kraid1,        doors,          9 )
VALUEFN_GENERIC_BFMAP ( doors_red_kraid2,        doors,          10)
VALUEFN_GENERIC_BFMAP ( doors_red_kraid3,        doors,          11)
VALUEFN_GENERIC_BFMAP ( doors_red_kraid4,        doors,          12)
VALUEFN_GENERIC_BFMAP ( doors_red_kraid5,        doors,          13)
VALUEFN_GENERIC_BFMAP ( doors_red_ridley,        doors,          14)
VALUEFN_GENERIC_BFMAP ( doors_yellow_ridley,     doors,          15)
VALUEFN_GENERIC_BFMAP ( doors_yellow_tourian,    doors,          16)
VALUEFN_GENERIC_BFMAP ( doors_red_tourian1,      doors,          17)
VALUEFN_GENERIC_BFMAP ( doors_red_tourian2,      doors,          18)
VALUEFN_GENERIC_BFMAP ( doors_zebetite1,         doors_zebetite, 0 )
VALUEFN_GENERIC_BFMAP ( doors_zebetite2,         doors_zebetite, 1 )
VALUEFN_GENERIC_BFMAP ( doors_zebetite3,         doors_zebetite, 2 )
VALUEFN_GENERIC_BFMAP ( doors_zebetite4,         doors_zebetite, 3 )
VALUEFN_GENERIC_BFMAP ( doors_zebetite5,         doors_zebetite, 4 )
VALUEFN_GENERIC_BFMAP ( doors_stat_ridley,       doors_stat,     0 )
VALUEFN_GENERIC_BFMAP ( doors_stat_kraid,        doors_stat,     1 )
VALUEFN_GENERIC_BFMAP ( bosses_mbrain,           bosses,         0 )
VALUEFN_GENERIC_BFMAP ( bosses_ridley,           bosses,         1 )
VALUEFN_GENERIC_BFMAP ( bosses_kraid,            bosses,         2 )
VALUEFN_GENERIC_BFMAP ( unused_1,                unused,         0 )
VALUEFN_GENERIC_BFMAP ( unused_2,                unused,         1 )
VALUEFN_GENERIC_BFMAP ( unused_3,                unused,         2 )
VALUEFN_GENERIC_BFMAP ( unused_4,                unused,         3 )
VALUEFN_GENERIC_BFMAP ( unused_5,                unused,         4 )
VALUEFN_GENERIC_BFMAP ( unused_6,                unused,         5 )
VALUEFN_GENERIC_BFMAP ( unused_7,                unused,         6 )
VALUEFN_GENERIC_BFMAP ( unused_8,                unused,         7 )
VALUEFN_GENERIC_BFMAP ( unused_9,                unused,         8 )
VALUEFN_GENERIC_BFMAP ( unused_10,               unused,         9 )
VALUEFN_GENERIC_BFMAP ( unused_11,               unused,         10)
VALUEFN_GENERIC_BFMAP ( unused_12,               unused,         11)

void
valuefn_missiles_max(struct valuefnret *vfnret)
{
    int bosses_dead = data_isridleydead() + data_iskraiddead();

    valuefn_missiles_total(vfnret);
    vfnret->_u32 = 5 * vfnret->_u32 + 75 * bosses_dead;
}

void
valuefn_data_to_password(struct valuefnret *vfnret)
{
    static char buffer[25];
    u8 const *password = data_to_password();

    memcpy(buffer, password, 24);
    buffer[24] = '\0';
    vfnret->_str = buffer;
}

void
valuefn_show_password(struct valuefnret *vfnret)
{
    static char buffer[28];
    u8 const *password = data_to_password();

    snprintf(buffer, 28, "%.6s %.6s\n%.6s %.6s", password, password + 6, password + 12, password + 18);
    vfnret->_str = buffer;
}
