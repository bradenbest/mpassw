#include "localization/menutexts.h"

/* I don't recommend messing with these unless you know what you're
 * doing. Every function must stay in their original place regargless of
 * features enabled/disabled (removing them would break alignment), and
 * new functions must be inserted at the end no matter how tempting it
 * is to insert it next to similar functions. If the indices of the
 * functions change, then so too must the format specifiers in every
 * single menu text that uses them, and every translation thereof. So
 * when compiling a function out, it's better to just replace it with
 * NULL and not call it (doing so will cause a crash). Which means that
 * a function being taken out because of e.g. FEATURE_NARPAS, must have
 * a matching feature detect check with alternate/no text in the menu
 * texts that reference that function.
 *
 * I've placed marker comments every 5 members (and on interruptions by
 * feature detect macros) to aid in locating the correct function for
 * menu text writing.
 *
 * Note also that some functions may not be functions and may in fact be
 * null pointers. This is done be feature detect macros in valuefn.h
 *
 * Consider compiling with -DDEBUG or -DFEATURE_DEBUG=1 when modifying
 * these so that it's easier to detect fuckups, as this will enable
 * extra bounds checks in putsvfmt, which will catch out-of-bounds.
 * You'll also be able to find ranges that ask for null pointers this
 * way.
 */

static valuefn fmtargs_intro[] = {
    valuefn_version,
    FMTARGS_TERMINATOR
};

static valuefn fmtargs_main[] = {
    // 0
    valuefn_shiftbyte,
    valuefn_narpas_warning,
    valuefn_items_taken,
    valuefn_items_owned,
    valuefn_missiles_total,
    // 5
    valuefn_missiles_ammo,
    valuefn_missiles_max,
    valuefn_etanks,
    valuefn_doors,
    valuefn_doors_zebetite,
    // 10
    valuefn_doors_stat,
    valuefn_bosses,
    valuefn_location_str,
    valuefn_game_age,
    valuefn_unused,
    // 15
    FMTARGS_TERMINATOR
};

static valuefn fmtargs_show_password[] = {
    // 0
    valuefn_shiftbyte,
    valuefn_show_password,
    valuefn_narpas_warn_str,
    FMTARGS_TERMINATOR
};

static valuefn fmtargs_input_hex[] = {
    // 0
    valuefn_hexdump,
    FMTARGS_TERMINATOR
};

static valuefn fmtargs_edit_bytes[] = {
    // 0
    valuefn_hexdump,
    FMTARGS_TERMINATOR
};

static valuefn fmtargs_show_summary[] = {
    // 0
    valuefn_hexdump,
    valuefn_data_to_password,
    valuefn_location_str,
    valuefn_age_ntsc,
    valuefn_age_pal,
    // 5
    valuefn_shiftbyte,
    valuefn_items_taken,
    valuefn_items_taken_marumari,
    valuefn_items_taken_bombs,
    valuefn_items_taken_varia,
    // 10
    valuefn_items_taken_highjump,
    valuefn_items_taken_screwattack,
    valuefn_items_owned,
    valuefn_items_owned_marumari,
    valuefn_items_owned_bombs,
    // 15
    valuefn_items_owned_varia,
    valuefn_items_owned_highjump,
    valuefn_items_owned_screwattack,
    valuefn_items_owned_longbeam,
    valuefn_items_owned_wavebeam,
    // 20
    valuefn_items_owned_icebeam,
    valuefn_items_owned_swimsuit,
    valuefn_missiles_ammo,
    valuefn_missiles_max,
    valuefn_missiles_total,
    // 25
    valuefn_missiles_taken_brin1,
    valuefn_missiles_taken_brin2,
    valuefn_missiles_taken_norf1,
    valuefn_missiles_taken_norf2,
    valuefn_missiles_taken_norf3,
    // 30
    valuefn_missiles_taken_norf4,
    valuefn_missiles_taken_norf5,
    valuefn_missiles_taken_norf6,
    valuefn_missiles_taken_norf7,
    valuefn_missiles_taken_norf8,
    // 35
    valuefn_missiles_taken_norf9,
    valuefn_missiles_taken_norf10,
    valuefn_missiles_taken_norf11,
    valuefn_missiles_taken_norf12,
    valuefn_missiles_taken_kraid1,
    // 40
    valuefn_missiles_taken_kraid2,
    valuefn_missiles_taken_kraid3,
    valuefn_missiles_taken_kraid4,
    valuefn_missiles_taken_ridley1,
    valuefn_missiles_taken_ridley2,
    // 45
    valuefn_missiles_taken_ridley3,
    valuefn_etanks,
    valuefn_etanks_brin1,
    valuefn_etanks_brin2,
    valuefn_etanks_brin3,
    // 50
    valuefn_etanks_norf1,
    valuefn_etanks_kraid1,
    valuefn_etanks_kraid2,
    valuefn_etanks_ridley1,
    valuefn_etanks_ridley2,
    // 55
    valuefn_doors,
    valuefn_doors_red_longbeam,
    valuefn_doors_red_tourianbridge,
    valuefn_doors_red_bombs,
    valuefn_doors_red_icebeam1,
    // 60
    valuefn_doors_red_varia,
    valuefn_doors_red_icebeam2,
    valuefn_doors_red_highjump,
    valuefn_doors_red_screwattack,
    valuefn_doors_red_wavebeam,
    // 65
    valuefn_doors_red_kraid1,
    valuefn_doors_red_kraid2,
    valuefn_doors_red_kraid3,
    valuefn_doors_red_kraid4,
    valuefn_doors_red_kraid5,
    // 70
    valuefn_doors_red_ridley,
    valuefn_doors_yellow_ridley,
    valuefn_doors_yellow_tourian,
    valuefn_doors_red_tourian1,
    valuefn_doors_red_tourian2,
    // 75
    valuefn_doors_zebetite,
    valuefn_doors_zebetite1,
    valuefn_doors_zebetite2,
    valuefn_doors_zebetite3,
    valuefn_doors_zebetite4,
    // 80
    valuefn_doors_zebetite5,
    valuefn_doors_stat,
    valuefn_doors_stat_ridley,
    valuefn_doors_stat_kraid,
    valuefn_bosses,
    // 85
    valuefn_bosses_mbrain,
    valuefn_bosses_ridley,
    valuefn_bosses_kraid,
    valuefn_location,
    valuefn_game_age,
    // 90
    valuefn_game_ticks,
    valuefn_unused,
    valuefn_unused_1,
    valuefn_unused_2,
    valuefn_unused_3,
    // 95
    valuefn_unused_4,
    valuefn_unused_5,
    valuefn_unused_6,
    valuefn_unused_7,
    valuefn_unused_8,
    // 100
    valuefn_unused_9,
    valuefn_unused_10,
    valuefn_unused_11,
    valuefn_unused_12,
    valuefn_game_ending,
    // 105
    FMTARGS_TERMINATOR
};

static valuefn fmtargs_items[] = {
    // 0
    valuefn_items_taken,
    valuefn_items_owned,
    valuefn_items_taken_marumari,
    valuefn_items_taken_bombs,
    valuefn_items_taken_varia,
    // 5
    valuefn_items_taken_highjump,
    valuefn_items_taken_screwattack,
    valuefn_items_owned_marumari,
    valuefn_items_owned_bombs,
    valuefn_items_owned_varia,
    // 10
    valuefn_items_owned_highjump,
    valuefn_items_owned_screwattack,
    valuefn_items_owned_longbeam,
    valuefn_items_owned_wavebeam,
    valuefn_items_owned_icebeam,
    // 15
    valuefn_items_owned_swimsuit,
    FMTARGS_TERMINATOR
};

static valuefn fmtargs_missiles[] = {
    // 0
    valuefn_missiles_total,
    valuefn_missiles_ammo,
    valuefn_missiles_max,
    valuefn_missiles_taken_brin1,
    valuefn_missiles_taken_brin2,
    // 5
    valuefn_missiles_taken_norf1,
    valuefn_missiles_taken_norf2,
    valuefn_missiles_taken_norf3,
    valuefn_missiles_taken_norf4,
    valuefn_missiles_taken_norf5,
    // 10
    valuefn_missiles_taken_norf6,
    valuefn_missiles_taken_norf7,
    valuefn_missiles_taken_norf8,
    valuefn_missiles_taken_norf9,
    valuefn_missiles_taken_norf10,
    // 15
    valuefn_missiles_taken_norf11,
    valuefn_missiles_taken_norf12,
    valuefn_missiles_taken_kraid1,
    valuefn_missiles_taken_kraid2,
    valuefn_missiles_taken_kraid3,
    // 20
    valuefn_missiles_taken_kraid4,
    valuefn_missiles_taken_ridley1,
    valuefn_missiles_taken_ridley2,
    valuefn_missiles_taken_ridley3,
    FMTARGS_TERMINATOR
};

static valuefn fmtargs_etanks[] = {
    // 0
    valuefn_etanks,
    valuefn_etanks_brin1,
    valuefn_etanks_brin2,
    valuefn_etanks_brin3,
    valuefn_etanks_norf1,
    // 5
    valuefn_etanks_kraid1,
    valuefn_etanks_kraid2,
    valuefn_etanks_ridley1,
    valuefn_etanks_ridley2,
    FMTARGS_TERMINATOR
};

static valuefn fmtargs_doors[] = {
    // 0
    valuefn_doors,
    valuefn_doors_zebetite,
    valuefn_doors_stat,
    valuefn_doors_red_longbeam,
    valuefn_doors_red_tourianbridge,
    // 5
    valuefn_doors_red_bombs,
    valuefn_doors_red_icebeam1,
    valuefn_doors_red_varia,
    valuefn_doors_red_icebeam2,
    valuefn_doors_red_highjump,
    // 10
    valuefn_doors_red_screwattack,
    valuefn_doors_red_wavebeam,
    valuefn_doors_red_kraid1,
    valuefn_doors_red_kraid2,
    valuefn_doors_red_kraid3,
    // 15
    valuefn_doors_red_kraid4,
    valuefn_doors_red_kraid5,
    valuefn_doors_red_ridley,
    valuefn_doors_yellow_ridley,
    valuefn_doors_yellow_tourian,
    // 20
    valuefn_doors_red_tourian1,
    valuefn_doors_red_tourian2,
    valuefn_doors_zebetite1,
    valuefn_doors_zebetite2,
    valuefn_doors_zebetite3,
    // 25
    valuefn_doors_zebetite4,
    valuefn_doors_zebetite5,
    valuefn_doors_stat_ridley,
    valuefn_doors_stat_kraid,
    FMTARGS_TERMINATOR
};

static valuefn fmtargs_bosses[] = {
    // 0
    valuefn_bosses,
    valuefn_bosses_mbrain,
    valuefn_bosses_ridley,
    valuefn_bosses_kraid,
    FMTARGS_TERMINATOR
};

static valuefn fmtargs_location[] = {
    // 0
    valuefn_location_str,
    valuefn_location,
    FMTARGS_TERMINATOR
};

static valuefn fmtargs_age[] = {
    // 0
    valuefn_age_ntsc,
    valuefn_age_pal,
    valuefn_game_age,
    valuefn_game_ticks,
    valuefn_game_ending,
    // 5
    FMTARGS_TERMINATOR
};

static valuefn fmtargs_unused[] = {
    // 0
    valuefn_unused,
    valuefn_unused_1,
    valuefn_unused_2,
    valuefn_unused_3,
    valuefn_unused_4,
    // 5
    valuefn_unused_5,
    valuefn_unused_6,
    valuefn_unused_7,
    valuefn_unused_8,
    valuefn_unused_9,
    // 10
    valuefn_unused_10,
    valuefn_unused_11,
    valuefn_unused_12,
    FMTARGS_TERMINATOR
};

