#define MENU_GETTERDATA_C
#include "menu/getterdata.h"

/* expose mappings for getter data. Mainly just char strings for
 * mapping letter input to a child menu (see also getterfn_char_or_u32)
 */

/* GETTERDATA        getterdata_*  .children_char */
GENERIC_GETTERDATA ( main,         "qiphBsernImEdblaut'cN"               );
GENERIC_GETTERDATA ( quit_confirm, "yn"                                  );
GENERIC_GETTERDATA ( items,        "qmbvhsMBVHSlwiW*^tToO"               );
GENERIC_GETTERDATA ( missiles,     "qa_____________________*^bBnNkKrR"   );
GENERIC_GETTERDATA ( etanks,       "q________*^bBnNkKrR"                 );
GENERIC_GETTERDATA ( doors,        "qltbivIhsw_______________rk*^dDzZS$" );
GENERIC_GETTERDATA ( bosses,       "qmrk"                                );
GENERIC_GETTERDATA ( location,     "bnktr"                               );
GENERIC_GETTERDATA ( unused,       "q____________*^"                     );
