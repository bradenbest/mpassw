#include "version.h"
#include "feature.h"

#define MENU_ENTRYDATA_C
#include "menu/entrydata.h"
#include "menu/entrydata.static.c"

/* expose mappings for menu entry data. See GENERIC_ENTRYDATA_* macros
 * and entrydata.static.c
 */

GENERIC_ENTRYDATA_PUTSVFMT ( intro           );
GENERIC_ENTRYDATA_PUTSVFMT ( main            );
GENERIC_ENTRYDATA_PUTSV    ( quit_confirm    );
GENERIC_ENTRYDATA_PUTSV    ( input_password  );
GENERIC_ENTRYDATA_PUTSVFMT ( show_password   );
GENERIC_ENTRYDATA_PUTSVFMT ( input_hex       );
GENERIC_ENTRYDATA_PUTSVFMT ( edit_bytes      );
GENERIC_ENTRYDATA_PUTSVFMT ( show_summary    );
GENERIC_ENTRYDATA_PUTSV    ( set_key         );
GENERIC_ENTRYDATA_PUTSVFMT ( items           );
GENERIC_ENTRYDATA_PUTSVFMT ( missiles        );
GENERIC_ENTRYDATA_PUTSVFMT ( etanks          );
GENERIC_ENTRYDATA_PUTSVFMT ( doors           );
GENERIC_ENTRYDATA_PUTSVFMT ( bosses          );
GENERIC_ENTRYDATA_PUTSVFMT ( location        );
GENERIC_ENTRYDATA_PUTSVFMT ( age             );
GENERIC_ENTRYDATA_PUTSVFMT ( unused          );
GENERIC_ENTRYDATA_PUTSV    ( missiles_ammo   );
GENERIC_ENTRYDATA_PUTSV    ( generic_setbyte );

#if FEATURE_TUTORIAL
GENERIC_ENTRYDATA_PUTSV ( tutorial_hint );
GENERIC_ENTRYDATA_PUTSV ( tutorial      );
GENERIC_ENTRYDATA_PUTSV ( tutorial_oops );
#endif // FEATURE_TUTORIAL
