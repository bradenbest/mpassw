#include <string.h>
#include <stddef.h>

#include "feature.h"
#include "data/bitfieldmap.h"
#include "data/bitfieldrange.h"
#include "menu/entryfn.h"
#include "menu/entrydata.h"
#include "menu/getterfn.h"
#include "menu/getterdata.h"
#include "menu/handlerfn.h"

#define MENU_MENU_C
#include "menu/menu.h"
#include "menu/menu.static.c"

/* name:    menu_get_page
 * @pageid: menu page id (MENUPAGE_*)
 * return:  menu page pointer or NULL
 * desc:    returns menu page pointer or NULL if the page is
 *          MENUPAGE_QUIT
 */
struct menu_page *
menu_get_page ( u32 pageid )
{
    if (pageid == MENUPAGE_QUIT)
        return NULL;

    return menus + pageid;
}

#if FEATURE_DEBUG

/* name:     menu_get_pageid
 * @selmenu: menu page pointer
 * return:   menu page's ID
 * desc:     subtracts menu's address from base address to get its page
 *           ID
 */
int
menu_get_pageid ( struct menu_page const *selmenu )
{
    return selmenu - menus;
}

/* name:   menu_get_*data_size
 * return: size of relevant structures
 * desc:   calculates the sizes of the relevant structures (entrydata,
 *         getterdata or handlerdata)
 */

size_t
menu_get_entrydata_size ( void )
{
    return get_data_size(entrydata_size);
}

size_t
menu_get_getterdata_size ( void )
{
    return get_data_size(getterdata_size);
}

size_t
menu_get_handlerdata_size ( void )
{
    return get_data_size(handlerdata_size);
}

#endif // FEATURE_DEBUG
