#include "data/bitfieldmap.h"
#include "data/bitfieldrange.h"
#include "menu/menu.h"

#define MENU_HANDLERDATA_C
#include "menu/handlerdata.h"
#include "menu/handlerdata.static.c"

/* expose mappings of handler data, which includes directory menus, bulk
 * bit set menus, byte set menus, and single bit toggle menus
 */

/*                              handlerdata_* */
/* DIRECTORY                    name fragment */
GENERIC_HANDLERDATA_DIRECTORY ( main         );
GENERIC_HANDLERDATA_DIRECTORY ( quit_confirm );
GENERIC_HANDLERDATA_DIRECTORY ( edit_bytes   );
GENERIC_HANDLERDATA_DIRECTORY ( items        );
GENERIC_HANDLERDATA_DIRECTORY ( missiles     );
GENERIC_HANDLERDATA_DIRECTORY ( etanks       );
GENERIC_HANDLERDATA_DIRECTORY ( doors        );
GENERIC_HANDLERDATA_DIRECTORY ( bosses       );
GENERIC_HANDLERDATA_DIRECTORY ( unused       );

/*                         handlerdata_bulk_* bfmaps_*        ranges_*         */
/* BULK                    name fragment      bitfield        range     offset */
GENERIC_HANDLERDATA_BULK ( items_taken,       items_taken,    items,    0 );
GENERIC_HANDLERDATA_BULK ( items_owned,       items_owned,    items,    1 );
GENERIC_HANDLERDATA_BULK ( missiles_all,      missiles,       missiles, 0 );
GENERIC_HANDLERDATA_BULK ( missiles_brinstar, missiles,       missiles, 1 );
GENERIC_HANDLERDATA_BULK ( missiles_norfair,  missiles,       missiles, 2 );
GENERIC_HANDLERDATA_BULK ( missiles_kraid,    missiles,       missiles, 3 );
GENERIC_HANDLERDATA_BULK ( missiles_ridley,   missiles,       missiles, 4 );
GENERIC_HANDLERDATA_BULK ( etanks_all,        etanks,         etanks,   0 );
GENERIC_HANDLERDATA_BULK ( etanks_brinstar,   etanks,         etanks,   1 );
GENERIC_HANDLERDATA_BULK ( etanks_norfair,    etanks,         etanks,   2 );
GENERIC_HANDLERDATA_BULK ( etanks_kraid,      etanks,         etanks,   3 );
GENERIC_HANDLERDATA_BULK ( etanks_ridley,     etanks,         etanks,   4 );
GENERIC_HANDLERDATA_BULK ( doors_doors,       doors,          doors,    0 );
GENERIC_HANDLERDATA_BULK ( doors_zebetites,   doors_zebetite, doors,    1 );
GENERIC_HANDLERDATA_BULK ( doors_statues,     doors_stat,     doors,    2 );
GENERIC_HANDLERDATA_BULK ( unused,            unused,         unused,   0 );

/*                            handlerdata_setbyte_* */
/* SETBYTE                    name fragment         */
GENERIC_HANDLERDATA_SETBYTE ( 0  );
GENERIC_HANDLERDATA_SETBYTE ( 1  );
GENERIC_HANDLERDATA_SETBYTE ( 2  );
GENERIC_HANDLERDATA_SETBYTE ( 3  );
GENERIC_HANDLERDATA_SETBYTE ( 4  );
GENERIC_HANDLERDATA_SETBYTE ( 5  );
GENERIC_HANDLERDATA_SETBYTE ( 6  );
GENERIC_HANDLERDATA_SETBYTE ( 7  );
GENERIC_HANDLERDATA_SETBYTE ( 8  );
GENERIC_HANDLERDATA_SETBYTE ( 9  );
GENERIC_HANDLERDATA_SETBYTE ( 10 );
GENERIC_HANDLERDATA_SETBYTE ( 11 );
GENERIC_HANDLERDATA_SETBYTE ( 12 );
GENERIC_HANDLERDATA_SETBYTE ( 13 );
GENERIC_HANDLERDATA_SETBYTE ( 14 );
GENERIC_HANDLERDATA_SETBYTE ( 15 );
GENERIC_HANDLERDATA_SETBYTE ( 16 );
GENERIC_HANDLERDATA_SETBYTE ( 17 );

/*                              handlerdata_togglebit_*  bfmaps_*               */
/* TOGGLEBIT                    name fragment            bitfield        offset */
GENERIC_HANDLERDATA_TOGGLEBIT ( items_taken_0,           items_taken,    0  );
GENERIC_HANDLERDATA_TOGGLEBIT ( items_taken_1,           items_taken,    1  );
GENERIC_HANDLERDATA_TOGGLEBIT ( items_taken_2,           items_taken,    2  );
GENERIC_HANDLERDATA_TOGGLEBIT ( items_taken_3,           items_taken,    3  );
GENERIC_HANDLERDATA_TOGGLEBIT ( items_taken_4,           items_taken,    4  );
GENERIC_HANDLERDATA_TOGGLEBIT ( items_owned_0,           items_owned,    0  );
GENERIC_HANDLERDATA_TOGGLEBIT ( items_owned_1,           items_owned,    1  );
GENERIC_HANDLERDATA_TOGGLEBIT ( items_owned_2,           items_owned,    2  );
GENERIC_HANDLERDATA_TOGGLEBIT ( items_owned_3,           items_owned,    3  );
GENERIC_HANDLERDATA_TOGGLEBIT ( items_owned_4,           items_owned,    4  );
GENERIC_HANDLERDATA_TOGGLEBIT ( items_owned_5,           items_owned,    5  );
GENERIC_HANDLERDATA_TOGGLEBIT ( items_owned_6,           items_owned,    6  );
GENERIC_HANDLERDATA_TOGGLEBIT ( items_owned_7,           items_owned,    7  );
GENERIC_HANDLERDATA_TOGGLEBIT ( items_owned_8,           items_owned,    8  );
GENERIC_HANDLERDATA_TOGGLEBIT ( missiles_brin1,          missiles,       0  );
GENERIC_HANDLERDATA_TOGGLEBIT ( missiles_brin2,          missiles,       1  );
GENERIC_HANDLERDATA_TOGGLEBIT ( missiles_norf1,          missiles,       2  );
GENERIC_HANDLERDATA_TOGGLEBIT ( missiles_norf2,          missiles,       3  );
GENERIC_HANDLERDATA_TOGGLEBIT ( missiles_norf3,          missiles,       4  );
GENERIC_HANDLERDATA_TOGGLEBIT ( missiles_norf4,          missiles,       5  );
GENERIC_HANDLERDATA_TOGGLEBIT ( missiles_norf5,          missiles,       6  );
GENERIC_HANDLERDATA_TOGGLEBIT ( missiles_norf6,          missiles,       7  );
GENERIC_HANDLERDATA_TOGGLEBIT ( missiles_norf7,          missiles,       8  );
GENERIC_HANDLERDATA_TOGGLEBIT ( missiles_norf8,          missiles,       9  );
GENERIC_HANDLERDATA_TOGGLEBIT ( missiles_norf9,          missiles,       10 );
GENERIC_HANDLERDATA_TOGGLEBIT ( missiles_norf10,         missiles,       11 );
GENERIC_HANDLERDATA_TOGGLEBIT ( missiles_norf11,         missiles,       12 );
GENERIC_HANDLERDATA_TOGGLEBIT ( missiles_norf12,         missiles,       13 );
GENERIC_HANDLERDATA_TOGGLEBIT ( missiles_kraid1,         missiles,       14 );
GENERIC_HANDLERDATA_TOGGLEBIT ( missiles_kraid2,         missiles,       15 );
GENERIC_HANDLERDATA_TOGGLEBIT ( missiles_kraid3,         missiles,       16 );
GENERIC_HANDLERDATA_TOGGLEBIT ( missiles_kraid4,         missiles,       17 );
GENERIC_HANDLERDATA_TOGGLEBIT ( missiles_ridley1,        missiles,       18 );
GENERIC_HANDLERDATA_TOGGLEBIT ( missiles_ridley2,        missiles,       19 );
GENERIC_HANDLERDATA_TOGGLEBIT ( missiles_ridley3,        missiles,       20 );
GENERIC_HANDLERDATA_TOGGLEBIT ( etanks_brin1,            etanks,         0  );
GENERIC_HANDLERDATA_TOGGLEBIT ( etanks_brin2,            etanks,         1  );
GENERIC_HANDLERDATA_TOGGLEBIT ( etanks_brin3,            etanks,         2  );
GENERIC_HANDLERDATA_TOGGLEBIT ( etanks_norf1,            etanks,         3  );
GENERIC_HANDLERDATA_TOGGLEBIT ( etanks_kraid1,           etanks,         4  );
GENERIC_HANDLERDATA_TOGGLEBIT ( etanks_kraid2,           etanks,         5  );
GENERIC_HANDLERDATA_TOGGLEBIT ( etanks_ridley1,          etanks,         6  );
GENERIC_HANDLERDATA_TOGGLEBIT ( etanks_ridley2,          etanks,         7  );
GENERIC_HANDLERDATA_TOGGLEBIT ( doors_red_longbeam,      doors,          0  );
GENERIC_HANDLERDATA_TOGGLEBIT ( doors_red_tourianbridge, doors,          1  );
GENERIC_HANDLERDATA_TOGGLEBIT ( doors_red_bombs,         doors,          2  );
GENERIC_HANDLERDATA_TOGGLEBIT ( doors_red_icebeam1,      doors,          3  );
GENERIC_HANDLERDATA_TOGGLEBIT ( doors_red_varia,         doors,          4  );
GENERIC_HANDLERDATA_TOGGLEBIT ( doors_red_icebeam2,      doors,          5  );
GENERIC_HANDLERDATA_TOGGLEBIT ( doors_red_highjump,      doors,          6  );
GENERIC_HANDLERDATA_TOGGLEBIT ( doors_red_screwattack,   doors,          7  );
GENERIC_HANDLERDATA_TOGGLEBIT ( doors_red_wavebeam,      doors,          8  );
GENERIC_HANDLERDATA_TOGGLEBIT ( doors_red_kraid1,        doors,          9  );
GENERIC_HANDLERDATA_TOGGLEBIT ( doors_red_kraid2,        doors,          10 );
GENERIC_HANDLERDATA_TOGGLEBIT ( doors_red_kraid3,        doors,          11 );
GENERIC_HANDLERDATA_TOGGLEBIT ( doors_red_kraid4,        doors,          12 );
GENERIC_HANDLERDATA_TOGGLEBIT ( doors_red_kraid5,        doors,          13 );
GENERIC_HANDLERDATA_TOGGLEBIT ( doors_red_ridley,        doors,          14 );
GENERIC_HANDLERDATA_TOGGLEBIT ( doors_yellow_ridley,     doors,          15 );
GENERIC_HANDLERDATA_TOGGLEBIT ( doors_yellow_tourian,    doors,          16 );
GENERIC_HANDLERDATA_TOGGLEBIT ( doors_red_tourian1,      doors,          17 );
GENERIC_HANDLERDATA_TOGGLEBIT ( doors_red_tourian2,      doors,          18 );
GENERIC_HANDLERDATA_TOGGLEBIT ( doors_zebetite1,         doors_zebetite, 0  );
GENERIC_HANDLERDATA_TOGGLEBIT ( doors_zebetite2,         doors_zebetite, 1  );
GENERIC_HANDLERDATA_TOGGLEBIT ( doors_zebetite3,         doors_zebetite, 2  );
GENERIC_HANDLERDATA_TOGGLEBIT ( doors_zebetite4,         doors_zebetite, 3  );
GENERIC_HANDLERDATA_TOGGLEBIT ( doors_zebetite5,         doors_zebetite, 4  );
GENERIC_HANDLERDATA_TOGGLEBIT ( doors_stat_ridley,       doors_stat,     0  );
GENERIC_HANDLERDATA_TOGGLEBIT ( doors_stat_kraid,        doors_stat,     1  );
GENERIC_HANDLERDATA_TOGGLEBIT ( bosses_mbrain,           bosses,         0  );
GENERIC_HANDLERDATA_TOGGLEBIT ( bosses_ridley,           bosses,         1  );
GENERIC_HANDLERDATA_TOGGLEBIT ( bosses_kraid,            bosses,         2  );
GENERIC_HANDLERDATA_TOGGLEBIT ( unused_1,                unused,         0  );
GENERIC_HANDLERDATA_TOGGLEBIT ( unused_2,                unused,         1  );
GENERIC_HANDLERDATA_TOGGLEBIT ( unused_3,                unused,         2  );
GENERIC_HANDLERDATA_TOGGLEBIT ( unused_4,                unused,         3  );
GENERIC_HANDLERDATA_TOGGLEBIT ( unused_5,                unused,         4  );
GENERIC_HANDLERDATA_TOGGLEBIT ( unused_6,                unused,         5  );
GENERIC_HANDLERDATA_TOGGLEBIT ( unused_7,                unused,         6  );
GENERIC_HANDLERDATA_TOGGLEBIT ( unused_8,                unused,         7  );
GENERIC_HANDLERDATA_TOGGLEBIT ( unused_9,                unused,         8  );
GENERIC_HANDLERDATA_TOGGLEBIT ( unused_10,               unused,         9  );
GENERIC_HANDLERDATA_TOGGLEBIT ( unused_11,               unused,         10 );
GENERIC_HANDLERDATA_TOGGLEBIT ( unused_12,               unused,         11 );
