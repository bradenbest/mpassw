/* name:     get_entry_text
 * @selmenu: menu page pointer
 * return:   entrytext offset or NULL
 * context:  may be implemented as a macro depending on FEATURE_COMPACT
 * desc:     returns entry text or NULL if it doesn't exist. if compact
 *           mode is set and there is more text after the NULL
 *           terminator, it returns that text instead
 */
#if FEATURE_COMPACT
static inline char const **
get_entry_text ( struct menu_page *selmenu )
{
    char const **text = selmenu->entrydata->text;

    if (text == NULL)
        return NULL;

    if (ui_getflags() & UIFLAG_COMPACT)
        {
            while (*text != NULL)
                ++text;

            if (*(text + 1) == NULL)
                return selmenu->entrydata->text;

            return text + 1;
        }

    return text;
}
#endif // FEATURE_COMPACT
