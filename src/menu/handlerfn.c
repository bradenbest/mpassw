#include <stdio.h>
#include <stdlib.h>

#include "data/data.h"
#include "data/bitfieldmap.h"
#include "data/bitfieldrange.h"
#include "localization/handlerfn.h"
#include "menu/getterfn.h"
#include "ui/ui.h"
#include "util/util.h"

#define MENU_HANDLERFN_C
#include "menu/handlerfn.h"
#include "menu/handlerfn.static.c"

/* callback: handlerfn aka int (*) (struct menu_page *selmenu,
 *                                  union userinput *uinput)
 * name:     handlerfn_*
 * @selmenu: menu page pointer
 * @uinput:  user input to be handled
 * return:   handlerfnret (HANDLERFN_*)
 * context:  called by menu controller loop after the getter
 * desc:     handles userinput or performs a non-interactive action
 *           a handler may set the page id and return
 *           HANDLERFN_NOSETPAGEID so that the menu controller leaves it
 *           alone. In practice, the only handler that does this is
 *           handlerfn_generic_directory.
 */

int
handlerfn_generic_directory (
    struct menu_page * selmenu ,
    union userinput *  uinput  )
{
    u32 *children = selmenu->handlerdata->children;
    u32 nchildren;

    for (nchildren = 0; children[nchildren] != MENUPAGE_END; ++nchildren)
        ;

    ui_set_pageid(children[clamp(uinput->_u32, 0, nchildren - 1)]);
    return HANDLERFN_NOSETPAGEID;
}

int
handlerfn_generic_togglebit (
    struct menu_page * selmenu ,
    union userinput *  unused2 )
{
    IGNORE_VARIABLE(unused2);
    struct bitfieldmap *bitfield = selmenu->handlerdata->bitfield;

    bitfieldmap_toggle(bitfield);
    return HANDLERFN_RETURNTOPARENT;
}

int
handlerfn_generic_setbyte (
    struct menu_page * selmenu ,
    union userinput *  uinput  )
{
    u8 value = (u8)(uinput->_u32);

    data_set_bytes(selmenu->handlerdata->byteid, &value, 1);
    return HANDLERFN_RETURNTOPARENT;
}

int
handlerfn_generic_bulk_toggle (
    struct menu_page * selmenu ,
    union userinput *  unused2 )
{
    IGNORE_VARIABLE(unused2);

    return bulk_set_or_toggle(selmenu, bitfieldmap_toggle);
}

int
handlerfn_generic_bulk_set (
    struct menu_page * selmenu ,
    union userinput *  unused2 )
{
    IGNORE_VARIABLE(unused2);

    return bulk_set_or_toggle(selmenu, bitfieldmap_set);
}

int
handlerfn_generic_none (
    struct menu_page * unused  ,
    union userinput *  unused2 )
{
    IGNORE_VARIABLE(unused);
    IGNORE_VARIABLE(unused2);

    return HANDLERFN_RETURNTOPARENT;
}

int
handlerfn_input_password (
    struct menu_page * unused ,
    union userinput *  uinput )
{
    IGNORE_VARIABLE(unused);

    if (data_from_password((u8 const *)uinput->_line) == 0)
        goto exit_invalid_password;

    if (data_validate())
        ui_qputs(HANDLERFN_VALIDATION_PASSED_STR);
    else
        ui_qputs(HANDLERFN_VALIDATION_FAILED_STR);

#if FEATURE_NARPAS
#if FEATURE_COLOR
    ui_qputs(ANSIESC_COLORRED);
    ui_qputs(ui_narpas_warn_str());
    ui_qputs(ANSIESC_COLORCLR);
#else // FEATURE_COLOR
    ui_qputs(ui_narpas_warn_str());
#endif // FEATURE_COLOR
#endif // FEATURE_NARPAS
    ui_waitcontinue();
    return HANDLERFN_RETURNTOPARENT;

exit_invalid_password:
    {
        LOGERR("Password contains invalid characters. Returning to main menu.\n");
        ui_waitcontinue();
        return HANDLERFN_RETURNTOPARENT;
    }
}

int
handlerfn_input_hex (
    struct menu_page * unused ,
    union userinput *  uinput )
{
    IGNORE_VARIABLE(unused);
    char buffer[64];
    u8 u8buffer[16];

    for (int i = 0; i < 16; ++i)
        u8buffer[i] = (u8)strtou32(uinput->_line + (i * 2), 2, 16);

    data_set_bytes(0, u8buffer, 16);
    snprintf(buffer, 64, "Hexdump: %.44s\n", data_hexdump());
    ui_qputs(buffer);
#if FEATURE_NARPAS
    ui_qputs(ui_narpas_warn_str());
#endif // FEATURE_NARPAS
    ui_waitcontinue();

    return HANDLERFN_RETURNTOPARENT;
}

int
handlerfn_set_key (
    struct menu_page * unused ,
    union userinput *  uinput )
{
    IGNORE_VARIABLE(unused);

    data_set_shiftkey(uinput->_u32);
    return HANDLERFN_RETURNTOPARENT;
}

int
handlerfn_set_random_key (
    struct menu_page * unused  ,
    union userinput *  unused2 )
{
    IGNORE_VARIABLE(unused);
    IGNORE_VARIABLE(unused2);

    data_set_shiftkey(rand());
    return HANDLERFN_RETURNTOPARENT;
}

#if FEATURE_COMPACT
int
handlerfn_compact (
    struct menu_page * unused  ,
    union userinput *  unused2 )
{
    IGNORE_VARIABLE(unused);
    IGNORE_VARIABLE(unused2);

    ui_setflag(UIFLAG_COMPACT);
    ui_unsetflag(UIFLAG_WARN_NARPAS);
    return HANDLERFN_RETURNTOPARENT;
}

int
handlerfn_nocompact (
    struct menu_page * unused  ,
    union userinput *  unused2 )
{
    IGNORE_VARIABLE(unused);
    IGNORE_VARIABLE(unused2);

    ui_unsetflag(UIFLAG_COMPACT);
    ui_setflag(UIFLAG_WARN_NARPAS);
    return HANDLERFN_RETURNTOPARENT;
}
#endif // FEATURE_COMPACT

int
handlerfn_missiles_ammo (
    struct menu_page * unused ,
    union userinput *  uinput )
{
    IGNORE_VARIABLE(unused);

    data_set_missile_ammo(clamp(uinput->_u32, 0, 255));
    return HANDLERFN_RETURNTOPARENT;
}

int
handlerfn_location (
    struct menu_page * unused ,
    union userinput *  uinput )
{
    IGNORE_VARIABLE(unused);

    data_set_location(clamp(uinput->_u32, 0, 15));
    return HANDLERFN_RETURNTOPARENT;
}

int
handlerfn_age (
    struct menu_page * unused ,
    union userinput *  uinput )
{
    IGNORE_VARIABLE(unused);

    data_set_game_age(uinput->_u32);
    return HANDLERFN_RETURNTOPARENT;
}

#if FEATURE_NARPAS
int
handlerfn_toggle_narpas_warning (
    struct menu_page * unused  ,
    union userinput *  unused2 )
{
    IGNORE_VARIABLE(unused);
    IGNORE_VARIABLE(unused2);

    ui_toggleflag(UIFLAG_WARN_NARPAS);
    return HANDLERFN_RETURNTOPARENT;
}
#endif // FEATURE_NARPAS

#if FEATURE_TUTORIAL
int
handlerfn_tutorial (
    struct menu_page * unused  ,
    union userinput *  unused2 )
{
    IGNORE_VARIABLE(unused);
    IGNORE_VARIABLE(unused2);
    struct tutorial_page *selpage = tutorial_pages;

    ui_setflag(UIFLAG_TUTORIAL);

    while (tutorial_exec(selpage) != HANDLERFN_RETURNTOPARENT)
        ++selpage;

    ui_unsetflag(UIFLAG_TUTORIAL);
    return HANDLERFN_RETURNTOPARENT;
}
#endif // FEATURE_TUTORIAL
