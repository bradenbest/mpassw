#include <stddef.h>

#include "menu/menu.h"
#include "menu/entrydata.h"
#include "ui/ui.h"
#include "util/putsvfmt.h"

#define MENU_ENTRYFN_C
#include "menu/entryfn.h"
#include "menu/entryfn.static.c"

/* callback: entryfn aka int (*) (struct menu_page *selmenu)
 * name:     entryfn_*
 * @selmenu  menu page pointer
 * return:   entryfnret (ENTRYFN_*)
 * context:  called by menu controller loop at the start
 * desc:     I've managed to boil down entryfns to just putsvfmt and
 *           none. The entryfn executes what needs to be done before the
 *           user is prompted for input.
 */

int
entryfn_generic_putsvfmt ( struct menu_page *selmenu )
{
    char const **text = get_entry_text(selmenu);

    if (text != NULL)
        putsvfmt(text, selmenu);

    return ENTRYFN_CONTINUE;
}

int
entryfn_generic_none ( struct menu_page *unused )
{
    IGNORE_VARIABLE(unused);

    return ENTRYFN_CONTINUE;
}
