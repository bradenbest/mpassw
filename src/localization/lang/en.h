/* This file contains as many hard-coded english texts as possible for
 * the purposes of easy localization. It includes the following
 * sections:
 *
 * LOCALIZATION_NEED_FEATURE
 * LOCALIZATION_NEED_UI
 * LOCALIZATION_NEED_UTIL
 * LOCALIZATION_NEED_USAGETEXT
 * LOCALIZATION_NEED_TUTORIAL
 * LOCALIZATION_NEED_HANDLERFN
 * LOCALIZATION_NEED_MENUTEXTS
 */

#ifdef LOCALIZATION_NEED_FEATURE
#undef LOCALIZATION_NEED_FEATURE

#define FEATURE_STR_HEADER "Features included (+) or not (-):\n"

#endif // LOCALIZATION_NEED_FEATURE



#ifdef LOCALIZATION_NEED_UI
#undef LOCALIZATION_NEED_UI

#define UI_WAITCONTINUE_TEXT "Press Enter to continue."

#define UI_NARPAS_WARN_STR \
    "NOTICE: Detected NARPASSWORD cheat code (NARPAS SWORD0 0000.. ......)\n" \
    "https://metroid.fandom.com/wiki/Narpas_Sword\n" \
    "This is a special password that bypasses the password system. The game\n" \
    "completely ignores it, but mpassw treats it like any other password, so\n" \
    "the values shown won't match up with the game.\n" \
    "\n" \
    "If this was unintentional, you can still generate a real password:\n" \
    "\n" \
    "1. Enter the password with one of the first 16 characters changed, or\n" \
    "2. Change the encryption key (to avoid changing in-game values)\n" \
    "\n" \
    "You'll know it worked if you don't see this notice.\n"

#endif // LOCALIZATION_NEED_UI



#ifdef LOCALIZATION_NEED_UTIL
#undef LOCALIZATION_NEED_UTIL

/* Both strings must be the same length. Note that this may affect menu
 * alignment
 */
#define UTIL_BOOLSTR_TEXT { "False", "True " }

#endif // LOCALIZATION_NEED_UTIL



#ifdef LOCALIZATION_NEED_USAGETEXT
#undef LOCALIZATION_NEED_USAGETEXT

static char const *usage_text =
    "usage: mpassw [options]\n"
    "\n"
    "Options:\n"
#if FEATURE_DEBUG
    "-d --debug-size         Show size debug information and exit\n"
#endif // FEATURE_DEBUG
#if FEATURE_COMPACT
    "-c --compact            Start in compact mode\n"
#endif // FEATURE_COMPACT
    "-q --quiet              Start in quiet mode (aka noninteractive mode)\n"
    "-p --password=VALUE     Set password before launch\n"
    "-x --hex=VALUE          Set game data to 16-byte hexdump\n"
    "-m --missiles=VALUE     Set missile ammo\n"
    "-l --location=VALUE     Set location\n"
    "-a --age=VALUE          Set game age\n"
    "-h --help               Show help and exit\n"
    "-H --help-only          Show help and exit (no version information)\n"
#if FEATURE_TUTORIAL
    "-t --tutorial           Start in tutorial mode\n"
#endif // FEATURE_TUTORIAL
    "-v --version            Show version information and exit\n"
    "-e --exit               Exit immediately (for use with -q)\n"
    ;

#endif // LOCALIZATION_NEED_USAGETEXT



#ifdef LOCALIZATION_NEED_TUTORIAL
#undef LOCALIZATION_NEED_TUTORIAL

static struct tutorial_page tutorial_pages[] = {
    {
        MENUPAGE_MAIN,
        "This is the main menu. The left columns shows numbers and letters that\n"
        "can be typed to access a menu. Some menu entries even show current\n"
        "values to the right.\n"
        "\n"
        "Let's start by entering a password. Enter 'i' (or '1') to go to the\n"
        "Input Password menu (the tutorial will go there next regardless)\n"
    },

    {
        MENUPAGE_INPUT_PASSWORD,
        "Please enter the infamous password, \"ENGAGERIDLEYMOTHERFUCKER\".\n"
    },

    {
        MENUPAGE_MAIN,
        "This password is known to crash the game. In fact, you can see the\n"
        "location is a value which is known to severely glitch or crash\n"
        "the game.\n"
        "\n"
        "Unless you deviated from the instructions, in which case it could\n"
        "say something else.\n"
        "\n"
        "Anyways, enter 'l' to go to the location menu.\n"
    },

    {
        MENUPAGE_LOCATION,
        "Go ahead and set the location to Tourian.\n"
    },

    {
        MENUPAGE_MAIN,
        "Now that the location's valid, the game won't crash or reset.\n"
        "It's time to generate a password. Enter 'p' to see it.\n"
    },

    {
        MENUPAGE_SHOW_PASSWORD,
        "You probably recognize this password as the fixed version commonly\n"
        "seen in wikis and videos about the password.\n"
        "\n"
        "Now that you know how to navigate this program, the tutorial is over\n"
    },

    { MENUPAGE_QUIT, NULL }
};

#endif // LOCALIZATION_NEED_TUTORIAL



#ifdef LOCALIZATION_NEED_HANDLERFN
#undef LOCALIZATION_NEED_HANDLERFN

#define HANDLERFN_VALIDATION_PASSED_STR "Checksum validation passed.\n"
#define HANDLERFN_VALIDATION_FAILED_STR "Checksum validation failed. Generated password will differ.\n"

#endif // LOCALIZATION_NEED_HANDLERFN



#ifdef LOCALIZATION_NEED_MENUTEXTS
#undef LOCALIZATION_NEED_MENUTEXTS

/* entry text is a pair of NULL-terminated string arrays
 * in 1.10-rc1, a "compact mode" was added that gives menus more terse
 * text, and entryfn_generic_putsv and entryfn_generic_putsvfmt were
 * adjusted to check for compact mode and seek past the null pointer
 * to get to the simpler output, or return the default if it's
 * double-NULL-terminated
 */

static char const *text_intro[] = {
    "mpassw - Metroid password generator v%s0 by Braden Best",
    "",
    "This software is public domain and is distributed without warranty.",
    "If you paid money for this software, you have likely been scammed.",
    "The source code is available for free at https://gitlab.com/bradenbest/mpassw",
    "",
    FEATURE_STR,
    "",
    "=============================================================================",
    NULL,

#if FEATURE_COMPACT
    "mpassw %s0 by Braden Best",
#endif
    NULL
};

static char const *text_main[] = {
#if FEATURE_VERBOSEMENU
    "Encryption Key: %x0",
#if FEATURE_NARPAS
    "Narpas Warning: %B1",
#endif // FEATURE_NARPAS
    "",
    "-General Options-                  |-Game Values-",
    " 0  %c00  Quit                  %@6| 9  %c09  Items      (%u2t %u3o)",
    " 1  %c01  Input Password        %@6| 10 %c10  Missiles   (%u4c %u5/%u6a)",
    " 2  %c02  Show Password         %@6| 11 %c11  E-Tanks    (%u7/6)",
    " 3  %c03  Input Hexadecimal     %@6| 12 %c12  Doors etc. (%u8d %u9z %u10s)",
    " 4  %c04  Edit Bytes directly   %@6| 13 %c13  Bosses     (%u11/3)",
    " 5  %c05  Show Detailed Summary %@6| 14 %c14  Location   (%s12)",
    " 6  %c06  Set Encryption Key    %@6| 15 %c15  Game Age   (%u13)",
    " 7  %c07  Set Random Enc. Key   %@6| 16 %c16  Unused     (%u14/12)",
#if FEATURE_NARPAS
    " 8  %c08  Toggle Narpas Warning",
#endif // FEATURE_NARPAS
#if FEATURE_COMPACT
    " 19 %c19  Compact Menu",
#endif // FEATURE_COMPACT
    "",
#else // FEATURE_VERBOSEMENU
    "-General Options-",
    "%c00  Quit",
    "%c01  Input Password",
    "%c02  Show Password",
    "%c03  Input Hexadecimal",
    "%c04  Edit Bytes directly",
    "%c05  Show Detailed Summary",
    "%c06  Set Encryption Key",
    "%c07  Set Random Enc. Key",
#if FEATURE_NARPAS
    "%c08  Toggle Narpas Warning",
#endif // FEATURE_NARPAS
#if FEATURE_COMPACT
    "%c19  Compact Menu",
#endif // FEATURE_COMPACT
    "",
    "-Game Values-",
    "%c09  Items",
    "%c10  Missiles",
    "%c11  E-Tanks",
    "%c12  Doors etc.",
    "%c13  Bosses",
    "%c14  Location",
    "%c15  Game Age",
    "%c16  Unused",
    "",
#endif // FEATURE_VERBOSEMENU
    NULL,

#if FEATURE_COMPACT
    "%c20 Normal Menu",
    "%c01 Input Password",
    "%c02 Show Password",
    "%c04 Edit Bytes Directly",
    "%c05 Show Detailed Summary",
    "%c09 Items",
    "%c10 Missiles",
    "%c11 E-Tanks",
    "%c12 Doors etc.",
    "%c13 Bosses",
    "%c14 Location",
    "%c15 Game Age",
#endif // FEATURE_COMPACT
    NULL
};

static char const *text_quit_confirm[] = {
    "Confirm Quit [%c0/%c1]",
    NULL,
    NULL
};

static char const *text_input_password[] = {
#if FEATURE_VERBOSEMENU
    "",
    "Input password as an uninterrupted string of 24 characters.",
    "spaces will be interpreted the same way the game interprets spaces",
    "any missing characters will be interpreted as blanks (same as '0')",
    "",
#else // FEATURE_VERBOSEMENU
    "Enter password (no gaps)",
#endif // FEATURE_VERBOSEMENU
    NULL,
    NULL
};

static char const *text_show_password[] = {
#if FEATURE_VERBOSEMENU
    "%$iPassword generated with key %x0%@.\n",
    "%s1",
    "%$i%$r%s2",
#else // FEATURE_VERBOSEMENU
    "%s1",
#endif // FEATURE_VERBOSEMENU
    NULL,

#if FEATURE_COMPACT
    "%s1",
#endif // FEATURE_COMPACT
    NULL
};

static char const *text_input_hex[] = {
#if FEATURE_VERBOSEMENU
    "",
    "Current hexdump: %s",
    "Input data as an uninterrupted string of 32 hexadecimal characters.",
    "Valid: 0-9a-fA-F",
    "Missing characters will be interpreted as zero.",
    "",
#else // FEATURE_VERBOSEMENU
    "Input 16 bytes (32 hex chars)",
#endif // FEATURE_VERBOSEMENU
    NULL,
    NULL
};

static char const *text_edit_bytes[] = {
#if FEATURE_VERBOSEMENU
    "Enter 0 to return to the main menu or enter one of the following byte IDs",
    "BTW: you can enter hexadecimal numbers by prefixing them with '0x'",
    "",
    "Quick reference (refer to documentation for bitfields):",
    "    Byte ID     Offset  Contents",
    "    Byte 1-8    0-7     bitfield (8)",
    "    Byte 9      8       bitfield (4) | location (4)",
    "    Byte 10     9       bitfield (8)",
    "    Byte 11     10      missile ammo (8)",
    "    Byte 12-15  11-14   game age (32)",
    "    Byte 16     15      bitfield (8)",
    "    Byte 17     16      encryption key (8)",
    "    Byte 18     17      checksum (8) (pointless to modify)",
    "",
#endif // FEATURE_VERBOSEMENU
    "Byte ID: 1    3    5    7    9    11   13   15   17",
    "Hexdump: %s0",
    "           2    4    6    8    10   12   14   16   18",
    "",
    NULL,

#if FEATURE_COMPACT
    "1    3    5    7    9    11   13   15   17",
    "%s0",
    "",
#endif // FEATURE_COMPACT
    NULL
};

static char const *text_show_summary[] = {
    "Full Hexdump:   %s0",
    "Encryption Key: %x5",
    "Password:       %s1",
    "Missile Ammo:   %u22/%u23",
    "Location:       %u88 (%s2)",
    "Game Age:       %u89 (hex %X89)",
#if FEATURE_VERBOSEMENU
    "Ticks:          %u90",
    "NTSC:           %s3",
    "PAL:            %s4",
    "Ending:         %s104",
#endif // FEATURE_VERBOSEMENU
    "",
    "Items Taken:    %v6/05 %b7.11",
    "Items Owned:    %v12/09 %b13.17 %b18.21",
    "Missiles Taken: %v24/21 %b25.29 %b30.34 %b35.39 %b40.44 %b45",
    "Energy Tanks:   %v46/06 %b47.51 %b52.54",
    "Doors:          %v55/19 %b56.60 %b61.65 %b66.70 %b71.74",
    "Zebetites:      %v75/05 %b76.80",
    "Statues:        %v81/02 %b82.83",
    "Bosses Killed:  %v84/03 %b85.87",
    "Unused:         %v91/12 %b92.96 %b97.101 %b102.103",
    "",
    NULL,

#if FEATURE_COMPACT
    "Hexdump:  %s0",
    "Password: %s1",
    "Missiles: %u22/%u23",
    "Location: %s2",
    "Game Age: %X89h (%u90 ticks)",
    "",
#endif // FEATURE_COMPACT
    NULL
};

static char const *text_set_key[] = {
    "Set the encryption key to a value 0..255",
    NULL,
    NULL
};

static char const *text_items[] = {
#if FEATURE_VERBOSEMENU
    "Totals: %u0 Taken, %u1 Owned",
    "",
    "Taken items are removed from the overworld. Owned items are items",
    "Samus actually has in her inventory. Items can be taken but not",
    "owned, and this has softlock potential. There is no danger of a",
    "softlock with the Ice Beam, as there is no \"taken\" bit for it.",
    "Maru Mari is the Morph Ball, by the way.",
    "",
    "-Items Taken-            |-Items Owned-",
    " 0 %c0  Go Back       %@5| 6  %c06  Maru Mari : %B7 ",
    " 1 %c1  Maru Mari : %B2  | 7  %c07  Bombs     : %B8 ",
    " 2 %c2  Bombs     : %B3  | 8  %c08  Varia     : %B9 ",
    " 3 %c3  Varia     : %B4  | 9  %c09  High Jump : %B10",
    " 4 %c4  High Jump : %B5  | 10 %c10  Screw Atk : %B11",
    " 5 %c5  Screw Atk : %B6  | 11 %c11  Long Beam : %B12",
    "                         | 12 %c12  Wave Beam : %B13",
    "-Bulk Toggle/Set-        | 13 %c13  Ice Beam  : %B14",
    " %c15.16  All         %@8| 14 %c14  Swimsuit  : %B15",
    " %c17.18  Taken       %@8|",
    " %c19.20  Owned       %@8|",
    "",
#else // FEATURE_VERBOSEMENU
    "%c00  Go Back",
    "",
    "-Bulk Toggle/Set-",
    "%c15.16  All",
    "%c17.18  Taken",
    "%c19.20  Owned",
    "",
    "-Items Taken-",
    "%c01  Maru Mari    : %B2",
    "%c02  Bombs        : %B3",
    "%c03  Varia Suit   : %B4",
    "%c04  High Jump    : %B5",
    "%c05  Screw Attack : %B6",
    "",
    "-Items Owned-",
    "%c06  Maru Mari    : %B7",
    "%c07  Bombs        : %B8",
    "%c08  Varia Suit   : %B9",
    "%c09  High Jump    : %B10",
    "%c10  Screw Attack : %B11",
    "%c11  Long Beam    : %B12",
    "%c12  Wave Beam    : %B13",
    "%c13  Ice Beam     : %B14",
    "%c14  Swimsuit     : %B15",
    "",
#endif // FEATURE_VERBOSEMENU
    NULL,

#if FEATURE_COMPACT
    "Taken  %c1.5",
    "       %b2.6",
    "Owned  %c6.10 %c11.14",
    "       %b7.11 %b12.15",
    "",
#endif // FEATURE_COMPACT
    NULL
};

static char const *text_missiles[] = {
#if FEATURE_VERBOSEMENU
    "Totals: %u0 Containers, %u1/%u2 Ammo",
    "",
    "Missile ammo is a single 8-bit value. It can be higher than the max,",
    "but collecting a drop while over the max will clamp it down.",
    "Each missile container collected increases your max by 5.",
    "Kraid and Ridley being defeated add an additional 75 per boss.",
    "",
    "-Missile Ammo-       |-Missiles Taken-",
    " 0 %c0  Go Back   %@5| 2..3   Brinstar: %b3.4",
    " 1 %c1  Ammo      %@5| 4..15  Norfair:  %b5.9 %b10.14 %b15.16",
    "                     | 16..19 Kraid:    %b17.20",
    "-Bulk Toggle/Set-    | 20..22 Ridley:   %b21.23",
    " %c23.24 All      %@8|",
    " %c25.26 Brinstar %@8|",
    " %c27.28 Norfair  %@8|",
    " %c29.30 Kraid    %@8|",
    " %c31.32 Ridley   %@8|",
    "",
#else // FEATURE_VERBOSEMENU
    "%c00  Go Back",
    "%c01  Missile Ammo: %u1",
    "",
    "-Bulk Toggle/Set-",
    "%c23.24 All",
    "%c25.26 Brinstar",
    "%c27.28 Norfair",
    "%c29.30 Kraid",
    "%c31.32 Ridley",
    "",
    "-Missiles Taken-",
    "2..3   Brinstar: %b3.4",
    "4..15  Norfair:  %b5.9 %b10.14 %b15.16",
    "16..19 Kraid:    %b17.20",
    "20..22 Ridley:   %b21.23",
    "",
#endif // FEATURE_VERBOSEMENU
    NULL,

#if FEATURE_COMPACT
    "Ammo:  %u1/%u2",
    "Taken: 2     7     12    17    22",
    "       %b3.7 %b8.12 %b13.17 %b18.22 %b23",
#endif // FEATURE_COMPACT
    NULL
};

static char const *text_etanks[] = {
#if FEATURE_VERBOSEMENU
    "E-Tanks: %u0/6",
    "",
    "Despite there being 8 tanks, the max health is 699.",
    "Collecting more has no effect.",
    "",
    "-Energy Tanks Taken-      |-Bulk Toggle/Set-",
    " 0  q  Go Back            | %c09.10  All",
    " 1..3  Brinstar: %b1.3 %@5| %c11.12  Brinstar",
    " 4     Norfair:  %b4   %@5| %c13.14  Norfair",
    " 5..6  Kraid:    %b5.6 %@6| %c15.16  Kraid",
    " 7..8  Ridley:   %b7.8 %@6| %c17.18  Ridley",
    "",
#else // FEATURE_VERBOSEMENU
    "q     Go Back",
    "",
    "-Bulk Toggle/Set-",
    "%c09.10  All",
    "%c11.12  Brinstar",
    "%c13.14  Norfair",
    "%c15.16  Kraid",
    "%c17.18  Ridley",
    "",
    "-Energy Tanks Taken-",
    "1..3  Brinstar: %b1.3",
    "4     Norfair:  %b4",
    "5..6  Kraid:    %b5.6",
    "7..8  Ridley:   %b7.8",
    "",
#endif // FEATURE_VERBOSEMENU
    NULL,

#if FEATURE_COMPACT
    "Taken: 1     6",
    "       %b1.5 %b6.8",
    "",
#endif // FEATURE_COMPACT
    NULL
};

static char const *text_doors[] = {
#if FEATURE_VERBOSEMENU
    "Totals: %u0 Doors, %u1 Zebetites, %u2 Statues",
    "",
    "The red and yellow doors usually guard upgrades.",
    "Zebetites are the glass tubes in Mother Brain's room.",
    "The statues block the Tourian Bridge and must be raised to cross.",
    "",
    "-Doors-                       %@00|-Doors, Zebetites, Statues-",
    " 0 %c00  Go Back               %@2| 10..14 %@0   Kraid (R)     : %b12.16",
    " 1 %c01  Long Beam       : %B3 %@0| 15..16 %@0   Ridley (RY)   : %b17.18",
    " 2 %c02  Tourian Bridge  : %B4 %@0| 17..19 %@0   Tourian (YRR) : %b19.21",
    " 3 %c03  Bombs           : %B5 %@0| 20..24 %@0   Zebetites     : %b22.26",
    " 4 %c04  Ice Beam (Brin) : %B6 %@0| 25     %c25  Ridley Statue : %B27",
    " 5 %c05  Varia Suit      : %B7 %@0| 26     %c26  Kraid Statue  : %B28",
    " 6 %c06  Ice Beam (Norf) : %B8 %@0|",
    " 7 %c07  High Jump       : %B9 %@0|-Bulk Toggle/Set-",
    " 8 %c08  Screw Attack    : %B10%@1| %c27.28  All",
    " 9 %c09  Wave Beam       : %B11%@1| %c29.30  Doors",
    "                              %@00| %c31.32  Zebetites",
    "                              %@00| %c33.34  Statues",
    "",
#else // FEATURE_VERBOSEMENU
    "%c00  Go Back",
    "",
    "-Bulk Toggle/Set-",
    "%c27.28  All",
    "%c29.30  Doors",
    "%c31.32  Zebetites",
    "%c33.34  Statues",
    "",
    "-Doors, Zebetites, Statues-",
    "1..9   %@0   Other Doors   : %b3.11",
    "10..14 %@0   Kraid (R)     : %b12.16",
    "15..16 %@0   Ridley (RY)   : %b17.18",
    "17..19 %@0   Tourian (YRR) : %b19.21",
    "20..24 %@0   Zebetites     : %b22.26",
    "%c25         Ridley Statue : %B27",
    "%c26         Kraid Statue  : %B28",
#endif // FEATURE_VERBOSEMENU
    NULL,

#if FEATURE_COMPACT
    "Doors:     %c1.9 10     15  17",
    "           %b3.11 K%b12.16 R%b17.18 T%b19.21",
    "Zebetites: 20",
    "           %b22.26",
    "Statues:   %c25.26",
    "           %b27.28",
#endif // FEATURE_COMPACT
    NULL
};

static char const *text_bosses[] = {
#if FEATURE_VERBOSEMENU
    "Total: %u0/3",
    "",
    "If Mother Brain is defeated and the player dies, the game punishes",
    "the player by resetting Mother Brain, the Tourian doors and Zebetites.",
    "",
    " 0 %c0  Go Back",
    " 1 %c1  Mother Brain : %B1",
    " 2 %c2  Ridley       : %B2",
    " 3 %c3  Kraid        : %B3",
    "",
#else // FEATURE_VERBOSEMENU
    "%c0  Go Back",
    "%c1  Mother Brain : %B1",
    "%c2  Ridley       : %B2",
    "%c3  Kraid        : %B3",
    "",
#endif // FEATURE_VERBOSEMENU
    NULL,

#if FEATURE_COMPACT
    "Boss: %c1.3",
    "      %b1.3",
    "",
#endif // FEATURE_COMPACT
    NULL
};

static char const *text_location[] = {
#if FEATURE_VERBOSEMENU
    "Location: %u1 (%s0)",
    "",
    "The location Samus will spawn in.",
    "There are 16 possible values but only 5 of them are valid.",
    "The rest will reset or crash the game.",
    "",
    "Enter a number or letter for the location",
    " 0  %c0  Brinstar",
    " 1  %c1  Norfair",
    " 2  %c2  Kraid's Hideout",
    " 3  %c3  Tourian",
    " 4  %c4  Ridley's Hideout",
    " 5+ %@   Invalid",
    "",
#else // FEATURE_VERBOSEMENU
    "%c0  Brinstar",
    "%c1  Norfair",
    "%c2  Kraid's Hideout",
    "%c3  Tourian",
    "%c4  Ridley's Hideout",
#endif // FEATURE_VERBOSEMENU
    NULL,

#if FEATURE_COMPACT
    "Current: %s0",
    "Options: %c0.4",
    "",
#endif // FEATURE_COMPACT
    NULL
};

static char const *text_age[] = {
#if FEATURE_VERBOSEMENU
    "Game Age: %u2",
    "Raw Hex:  %X2",
    "Ticks:    %u3",
    "NTSC:     %s0",
    "PAL:      %s1",
    "Ending:   %s4",
    "",
    "The game age is incredibly complicated for something that plays such",
    "a small part in the game. It's a 32-bit value, but only the middle",
    "16 bits are used to decide the ending, and the low byte overflows at",
    "0xd0 (208), which is when the higher bytes increment. Because of",
    "this, the effective value range is 0x000000..0xffffcf. mpassw will",
    "let you put in any 32-bit value, but the tick value tries to be",
    "accurate to how much real time the game thinks has passed. Keyword:",
    "thinks. Note that the tick value shown by mpassw ignores the highest",
    "byte. If you really want all the details, see the source code",
    "(src/data/data.c: data_get_game_ticks, data_get_ending)",
    "",
#endif // FEATURE_VERBOSEMENU
    "Enter a number 0..4294967295 (0xffffffff)",
    "",
    NULL,

#if FEATURE_COMPACT
    "Age: %X2",
    "",
#endif // FEATURE_COMPACT
    NULL
};

static char const *text_unused[] = {
#if FEATURE_VERBOSEMENU
    "Total: %u0/12",
    "",
    "The game has been reverse engineered to death and the consensus is that",
    "these 12 values are unused or ignored by the game. They're never set.",
    "These bits cover ranges [7]&F8, [8]&70, and [15]&0F",
    "",
    " 0   q  Go Back",
    " 1..12  Unused: %b1.5 %b6.10 %b11.12",
    "",
    "-Bulk Toggle/Set-",
    " %c13.14 All",
    "",
#else // FEATURE_VERBOSEMENU
    "q     %@0 Go Back",
    "%c13  %@3 Toggle All",
    "%c14  %@3 Set All",
    "1..12 %@0 %b1.5 %b6.10 %b11.12",
    "",
#endif // FEATURE_VERBOSEMENU
    NULL,
    NULL
};

static char const *text_missiles_ammo[] = {
    "Enter a number 0..255",
    NULL,
    NULL
};

static char const *text_generic_setbyte[] = {
    "Enter new byte value (0..255)",
    NULL,
    NULL
};

#if FEATURE_TUTORIAL
static char const *text_tutorial_hint[] = {
#if FEATURE_VERBOSEMENU
    "First time users: type 't' and press Enter for a tutorial",
#else // FEATURE_VERBOSEMENU
    "There is a tutorial available ('t'), but it may be confusing.",
    "Compile with +verbose_menu for the best experience, or -tutorial",
    "to make this notice go away",
#endif // FEATURE_VERBOSEMENU
    NULL,
    NULL
};

static char const *text_tutorial[] = {
    "Welcome to the tutorial.",
    "",
    "In this tutorial, I will take you through some specific menus to walk",
    "you through a common use case: fixing a broken password.",
    "",
    "These menus are executed for real, so try to follow along or else the",
    "instructions might seem nonsensical.",
    "",
    "You can exit the whole program at any point by sending EOF",
    "(Mac and Linux: Ctrl+D, Windows: Ctrl+Z Enter)",
    "",
    "You can cancel out of menus by hitting Enter without typing anything",
    "",
    "You can enter hexadecimal at number prompts by prefixing with '0x'",
    "",
    NULL,

    "You're in compact mode. The tutorial will work, but it might",
    "be harder to follow",
    NULL
};

static char const *text_tutorial_oops[] = {
    "Oops! You took the hint literally. You were supposed to type...",
    "    > t",
    "not literally...",
    "    > 't'",
    "That's okay. I thought of that. But now you know.",
    "",
    NULL,
    NULL
};
#endif // FEATURE_TUTORIAL

#endif // LOCALIZATION_NEED_MENUTEXTS
