/* Does the actual language select import, independent of the include
 * guards and component selection (LOCALIZATION_NEED_* macros)
 *
 * Languages:
 *
 * LANGUAGE_EN - English (default)
 * LANGUAGE_RU - Russian
 */

#if defined(LANGUAGE_EN)
#include "localization/lang/en.h"
#elif defined(LANGUAGE_RU)
#include "localization/lang/ru.h"
#endif
