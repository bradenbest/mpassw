The localization unit consists of 3 parts.

# 1: component headers

Every component header is written like this

    /* description of what this component covers
     * e.g. "localizations for foo bar baz"
     */
    #ifndef LOCALIZATION_FILENAME_H
    #define LOCALIZATION_FILENAME_H

    #define LOCALIZATION_NEED_COMPONENTNAME
    #include "localization/selectlang.h"

    #endif // LOCALIZATION_FILENAME_H

The only things that differ are the include guards and the `NEED` macro

# 2: selectlang.h

This file does the actual langauge selection based on what was defined in `config.mk`

    LANGUAGE = -DLANGUAGE_EN

It includes the appropriate language file from `lang/`

The file does not have include guards, because it should only be included by the component headers, which are themselves
include guarded.

# 3: language file

These files are in `lang/` and contain the texts used in the program, sectioned off per `NEED` macro. The file should be
named after its language. For example, the Russian translation is in `lang/ru.h` and its langauge macro is
`LANGUAGE_RU`.

These files also do not have include guards.
