/* Localizations for strings in handlerfn module
 */
#ifndef LOCALIZATION_HANDLERFN_H
#define LOCALIZATION_HANDLERFN_H

#define LOCALIZATION_NEED_HANDLERFN
#include "localization/selectlang.h"

#endif // LOCALIZATION_HANDLERFN_H
