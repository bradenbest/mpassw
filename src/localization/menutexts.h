/* Localizations for menu texts (menu/entrydata.static.c)
 */
#ifndef LOCALIZATION_MENUTEXTS_H
#define LOCALIZATION_MENUTEXTS_H

#define LOCALIZATION_NEED_MENUTEXTS
#include "localization/selectlang.h"

#endif // LOCALIZATION_MENUTEXTS_H
