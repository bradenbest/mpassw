static int caught_sigint;

#if FEATURE_ARGS
static struct arg opts[] = {
    /*opt   opt_long        has_arg  callback */
#if FEATURE_DEBUG
    { "-d", "--debug-size", 0,       opt_d },
#endif
#if FEATURE_COMPACT
    { "-c", "--compact",    0,       opt_c },
#endif // FEATURE_COMPACT
    { "-q", "--quiet",      0,       opt_q },
    { "-p", "--password",   1,       opt_p },
    { "-x", "--hex",        1,       opt_x },
    { "-m", "--missiles",   1,       opt_m },
    { "-l", "--location",   1,       opt_l },
    { "-a", "--age",        1,       opt_a },
    { "-h", "--help",       0,       opt_h },
    { "-H", "--help-only",  0,       opt_H },
#if FEATURE_TUTORIAL
    { "-t", "--tutorial",   0,       opt_t },
#endif
    { "-v", "--version",    0,       opt_v },
    { "-e", "--exit",       0,       opt_e },
    OPTS_TERMINATOR
};

#include "localization/usagetext.h"

/* callback: argfn aka int (*) (char const *arg)
 * name:     opt_*
 * @arg:     the argument to this, or NULL if opts[this].hasarg is 0
 * return:   1 or 0
 * context:  the order of argument execution matters, may set flags, may
 *           set password or affect game data, called by
 *           ui/arg.c/parse_arg, return value determines whether to
 *           continue (1) or quit (0) the program.
 * desc:     see mpassw.static.c/usage_text[] for descriptions of each
 *           function
 */

#if FEATURE_DEBUG
static int
opt_d ( char const * restrict unused )
{
    IGNORE_VARIABLE(unused);
    size_t table_size = MENUPAGE_END * sizeof (struct menu_page);
    size_t entryaccum = menu_get_entrydata_size();
    size_t getteraccum = menu_get_getterdata_size();
    size_t handleraccum = menu_get_handlerdata_size();

    puts("--debug-size");
    puts("Menu table:");
    printf("  no. menu pages   = %u\n", MENUPAGE_END);
    printf("  sizeof menu_page = %lu\n", sizeof (struct menu_page));
    printf("  sizeof menus[]   = %lu\n", table_size);
    puts("");
    puts("Data (accumulated):");
    printf("  entrydata   = %lu\n", entryaccum);
    printf("  getterdata  = %lu\n", getteraccum);
    printf("  handlerdata = %lu\n", handleraccum);
    printf("  Total       = %lu\n", entryaccum + getteraccum + handleraccum);
    puts("");
    printf("Grand Total = %lu\n", table_size + entryaccum + getteraccum + handleraccum);
    return 0;
}
#endif // FEATURE_DEBUG

#if FEATURE_COMPACT
static int
opt_c ( char const * restrict unused )
{
    IGNORE_VARIABLE(unused);

    ui_setflag(UIFLAG_COMPACT);
    ui_unsetflag(UIFLAG_WARN_NARPAS);
    return 1;
}
#endif // FEATURE_COMPACT

static int
opt_q ( char const * restrict unused )
{
    IGNORE_VARIABLE(unused);

    ui_unsetflag(UIFLAG_INTERACTIVE);
    return 1;
}

static int
opt_p ( char const * restrict arg )
{
    char const *valstr[] = {"Failed", "Passed"};
    char const *default_password = "000000000000000000000000";
    char const *used_password;
    u8 buffer[24];
    size_t slen = strlen(arg);
    size_t amount = MIN(slen, 24);

    memcpy(buffer, arg, amount);
    memcpy(buffer + amount, default_password, 24 - amount);
    used_password = (char *)buffer;

    if (!data_from_password(buffer))
        {
            LOGFERR("Password `%.24s` contains invalid characters. Using default password.\n", buffer);
            used_password = default_password;
        }

    if (ui_getflags() & UIFLAG_INTERACTIVE)
        {
            opt_v(NULL);
            ui_set_pageid(MENUPAGE_SHOW_SUMMARY);
            menu_get_page(MENUPAGE_SHOW_SUMMARY)->flags |= MPFLAG_NOCLEAR_ONCE;
            printf("Loaded password `%.24s`\n", used_password);
            printf("Validation %s\n", valstr[data_validate()]);
        }

    return 1;
}

static int
opt_x ( char const * restrict arg )
{
    struct menu_page *page = menu_get_page(MENUPAGE_INPUT_HEX);
    union userinput uinput;

    memcpy(uinput._line, arg, clamp(strlen(arg), 0, 32));
    page->handler_callback(page, &uinput);
    return 1;
}

static int
opt_m ( char const * restrict arg )
{
    u32 value = clamp(strtou32(arg, strlen(arg), 10), 0, 255);

    data_set_missile_ammo(value);
    return 1;
}

static int
opt_l ( char const * restrict arg )
{
    char const *children_char = menu_get_page(MENUPAGE_LOCATION)->getterdata->children_char;
    u32 value = clamp(strtou32(arg, strlen(arg), 10), 0, 15);
    char const *match = strchr(children_char, *arg);

    if (match != NULL)
        data_set_location(match - children_char);
    else
        data_set_location(value);

    return 1;
}

static int
opt_a ( char const * restrict arg )
{
    u32 value = clamp(strtou32(arg, strlen(arg), 10), 0, 4294967247);

    data_set_game_age(value);
    return 1;
}

static int
opt_h ( char const * restrict unused )
{
    IGNORE_VARIABLE(unused);

    opt_v(NULL);
    opt_H(NULL);
    return 0;
}

static int
opt_H ( char const * restrict unused )
{
    IGNORE_VARIABLE(unused);

    ui_puts(usage_text);
    return 0;
}

#if FEATURE_TUTORIAL
static int
opt_t ( char const * restrict unused )
{
    IGNORE_VARIABLE(unused);

    ui_set_pageid(MENUPAGE_TUTORIAL);
    return 1;
}
#endif // FEATURE_TUTORIAL

static int
opt_v ( char const * restrict unused )
{
    IGNORE_VARIABLE(unused);

    ui_call_entry(menu_get_page(MENUPAGE_INTRO));
    return 0;
}

static int
opt_e ( char const * restrict unused )
{
    IGNORE_VARIABLE(unused);

    return 0;
}

#endif // FEATURE_ARGS (line 3)

/* name:    callback_sigint
 * context: registered in main via signal(2), sets caught_sigint
 * desc:    SIGINT handler, logs the event to stderr and sets a flag for
 *          the exit callback.
 */
static void
callback_sigint ( int unused )
{
    IGNORE_VARIABLE(unused);

    ui_qputs("\n");
    LOGERR("Caught SIGINT. Showing password and exiting.\n");
    caught_sigint = 1;
    exit(0);
}

/* name:    callback_exit
 * context: registered in main via atexit(3), checks EOF, checks
 *          interactive flag, checks caught_sigint (callback_sigint)
 * desc:    exit callback, checks a few things (context) and may display
 *          the current password in compact mode based on them
 *
 * 1. do not FEATURE_COMPACT this. It's preferable to not clear the
 *    screen
 */
static void
callback_exit ( void )
{
    int quietmode = !(ui_getflags() & UIFLAG_INTERACTIVE);

    if (quietmode || feof(stdin) || caught_sigint)
        {
            ui_setflag(UIFLAG_COMPACT); // 1
#if FEATURE_NARPAS
            ui_unsetflag(UIFLAG_WARN_NARPAS);
#endif // FEATURE_NARPAS
            ui_call_entry(menu_get_page(MENUPAGE_SHOW_PASSWORD));
        }
}
