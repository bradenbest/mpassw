static int escape_flags;
static char buffer[PUTSVFMT_BUFSZ];
static char snpbuf[PUTSVFMT_SNPSZ];
static size_t buflen;

/* callback: splicerfn aka void (*) (struct menu_page *selmenu,
 *                                   u32 valueid)
 * name:     splicerfn_*
 * @selmenu: menu page pointer
 * @valueid: index of valuefn or index of char or raw value
 * context:  most are defined by the GENERIC_SPLICERFN macro
 * desc:     splicers will either call a valuefn from
 *           selmenu->entrydata->fmtargs, place a char from
 *           selmenu->getterdata->children_char, or place valueid amount
 *           of spaces for padding.
 */

static void
splicerfn_str (
    struct menu_page * selmenu ,
    u32                valueid )
{
    struct valuefnret vfnret;

    selmenu->entrydata->fmtargs[valueid](&vfnret);
    buffer_append(vfnret._str, strlen(vfnret._str));
}

/* semicolons removed to comply with -pedantic */

/*                  splicerfn_*                 */
/* SPLICERFN        name fragment  fmt     expr */
GENERIC_SPLICERFN ( u32,           "%u",   vfnret._u32           )
GENERIC_SPLICERFN ( 02u32,         "%02u", vfnret._u32           )
GENERIC_SPLICERFN ( x8,            "%02x", vfnret._u32           )
GENERIC_SPLICERFN ( x32,           "%08x", vfnret._u32           )
GENERIC_SPLICERFN ( bool,          "%s",   boolstr(vfnret._u32)  )
GENERIC_SPLICERFN ( tinybool,      "%c",   "-*"[!!(vfnret._u32)] )

static void
splicerfn_idxchar (
    struct menu_page * selmenu ,
    u32                valueid )
{
    buffer_append(selmenu->getterdata->children_char + valueid, 1);
}

static void
splicerfn_padding (
    struct menu_page * unused ,
    u32                amount )
{
    IGNORE_VARIABLE(unused);
    static char const padding[] = "                                ";
    static size_t const padsz = sizeof padding;

    while (amount > padsz)
        {
            buffer_append(padding, padsz);
            amount -= padsz;
        }

    buffer_append(padding, amount);
}

/* callback: int (*) (char const * restrict lineoff, size_t len)
 * name:     escseq_*
 * @lineoff: offset of line just after escape sequence (generally +3)
 * @len:     available length of line segment (generally +3)
 * return:   number of bytes to jump forward:
 * context:  inlined in an attempt to optimize. I may decide to change
 *           this to a dispatch table in the future
 * desc:     used by parse_escapeseq to execute escape sequences.
 *
 *           i: skip whole line if not interactive
 */

static inline int
escseq_i (
    char const * restrict unused ,
    size_t                len    )
{
    IGNORE_VARIABLE(unused);

    if (ui_getflags() & UIFLAG_INTERACTIVE)
        return 0;

    escape_flags |= ESCFLAG_SKIPLINE;
    return (int)len;
}

static inline int
escseq_r (
    char const * restrict unused1 ,
    size_t                unused2 )
#if FEATURE_COLOR
{
    IGNORE_VARIABLE(unused1);
    IGNORE_VARIABLE(unused2);
    static char const colorred[] = ANSIESC_COLORRED;

    escape_flags |= ESCFLAG_REDTEXT;
    buffer_append(colorred, sizeof colorred);
    return 0;
}
#else // FEATURE_COLOR
{
    IGNORE_VARIABLE(unused1);
    IGNORE_VARIABLE(unused2);

    return 0;
}
#endif // FEATURE_COLOR

#if FEATURE_DEBUG
/* name:     debug_get_fmtargslen
 * @fmtargs: format arguments list
 * return:   length of list
 * context:  FEATURE_DEBUG only
 * desc:     calculates the length of fmtargs by looking for
 *           FMTARGS_TERMINATOR. Note that FMTARGS_TERMINATOR cannot be
 *           equal to NULL. See menu/valuefn.c and its header for more
 *           info.
 */
static inline size_t
debug_get_fmtargslen ( valuefn const *fmtargs )
{
    size_t fmtargs_len = 0;

    while (fmtargs[fmtargs_len] != FMTARGS_TERMINATOR)
        ++fmtargs_len;

    return fmtargs_len;
}

/* name:        debug_validate_idxchar
 * @getterdata: getterdata of menu page
 * @range_end:  last index of range
 * return:      exit code
 * context:     FEATURE_DEBUG only
 * desc:        extra checks for idxchar splicerfn, returns an exit code
 *              0: pass
 *              1: data missing
 *              2: range goes out of bounds
 */
static inline int
debug_validate_idxchar (
    struct getterdata const * getterdata ,
    int                       range_end  )
{
    if (getterdata == NULL || getterdata->children_char == NULL)
        return 1;

    if ((size_t)range_end >= strlen(getterdata->children_char))
        return 2;

    return 0;
}

/* name:         debug_validate_fmtargs
 * @entrydata:   entrydata of menu page
 * @range_start: first index of range
 * @range_end:   last index of range
 * return:       exit code
 * context:      FEATURE_DEBUG only
 * desc:         extra checks for regular splicerfn, returns an exit
 *               code
 *               0: pass
 *               1: data missing
 *               2: range goes out of bounds
 *               3: a null pointer is referenced in the range
 */
static inline int
debug_validate_fmtargs (
    struct entrydata const * entrydata   ,
    int                      range_start ,
    int                      range_end   )
{
    if (entrydata == NULL || entrydata->fmtargs == NULL)
        return 1;

    if ((size_t)range_end >= debug_get_fmtargslen(entrydata->fmtargs))
        return 2;

    for (int i = range_start; i <= range_end; ++i)
        if (entrydata->fmtargs[i] == NULL)
            return 3;

    return 0;
}
#endif // FEATURE_DEBUG

/* name:     call_splicer
 * @tok:     number or range token
 * @selmenu: menu page pointer
 * @splicer: splicerfn to call
 * return:   number of bytes to jump ahead
 * context:  called by parse, +debug hardening
 * desc:     phase 2 of putsvfmt involves figuring out the value (or
 *           range of values) referenced by tok and calling the passed
 *           in splicer with said value(s). This function also has
 *           +debug hardening to catch out-of-bounds
 *           fmtargs/children_char indices.
 */
static inline int
call_splicer (
    char const *       tok     ,
    struct menu_page * selmenu ,
    splicerfn          splicer )
{
    char const *tok2;
    size_t toklen;
    size_t tok2len = 0;
    int range_start;
    int range_end = 0;

    toklen = strcountdigits(tok);
    range_start = strtou32(tok, toklen, 10);

    if (tok[toklen] == '.')
        {
            tok2 = tok + toklen + 1;
            tok2len = 1 + strcountdigits(tok2);
            range_end = strtou32(tok2, tok2len - 1, 10);
        }

    if (range_end < range_start)
        range_end = range_start;

#if FEATURE_DEBUG
    if (splicer == splicerfn_padding)
        goto exit_normal;

    if (splicer == splicerfn_idxchar)
        switch (debug_validate_idxchar(selmenu->getterdata, range_end)) {
            case 0: goto exit_normal;
            case 1: goto exit_idxcharmissing;
            case 2: goto exit_outofbounds;
        }

    switch (debug_validate_fmtargs(selmenu->entrydata, range_start, range_end)) {
        case 0: goto exit_normal;
        case 1: goto exit_fmtargsmissing;
        case 2: goto exit_outofbounds;
        case 3: goto exit_nullptr;
    }
#endif // FEATURE_DEBUG

    goto exit_normal;

exit_normal:
    {
        for (int i = range_start; i <= range_end; ++i)
            splicer(selmenu, i);

        if (toklen == 0)
            return 0;

        return (int)(toklen + tok2len);
    }

#if FEATURE_DEBUG
exit_idxcharmissing:
    {
        LOGERR("Exit: exit_idxcharmissing\n");
        LOGFERR("Menu: %u (%s)\n", menu_get_pageid(selmenu), selmenu->name);
        LOGERR("getterdata.children_char is missing\n");
        return (int)(toklen + tok2len);
    }

exit_fmtargsmissing:
    {
        LOGERR("Exit: exit_fmtargsmissing\n");
        LOGFERR("Menu: %u (%s)\n", menu_get_pageid(selmenu), selmenu->name);
        LOGERR("entrydata.fmtargs is missing\n");
        return (int)(toklen + tok2len);
    }

exit_outofbounds:
    {
        LOGERR("Exit: exit_outofbounds\n");
        LOGFERR("Menu: %u (%s)\n", menu_get_pageid(selmenu), selmenu->name);
        LOGFERR("Index %u is out of bounds\n", range_end);
        return (int)(toklen + tok2len);
    }

exit_nullptr:
    {
        LOGERR("Exit: exit_nullptr\n");
        LOGFERR("Menu: %u (%s)\n", menu_get_pageid(selmenu), selmenu->name);
        LOGFERR("An index in range %u..%u references a null pointer.\n", range_start, range_end);
        LOGERR("Was a feature detect macro not accounted for?\n");
        return (int)(toklen + tok2len);
    }
#endif // FEATURE_DEBUG
}

/* name:     parse
 * @line:    line to parse (offset)
 * @selmenu: menu page pointer
 * return:   number of bytes to jump forward
 * context:  called by putsfmt, calls call_splicer, +debug hardening
 * desc:     checks first character in line, if it's not '%', the
 *           character is printed and 1 is returned. If it is '%' but
 *           the next character is not a valid specifier, and the
 *           FEATURE_DEBUG macro (+debug feature) is on, then an error
 *           is logged, the '%' is printed and 1 is returned. If +debug
 *           is not on, the behavior is undefined, so it's important to
 *           use +debug when modifying the menu texts to catch errors
 *           when they happen.  Otherwise, assuming the character
 *           matches one of the format specifiers, call_splicer is used
 *           to find and splice the value into the output. An offset of
 *           the line is fed into it where the value argument to the
 *           format specifier starts
 */
static inline int
parse (
    char const *       line    ,
    struct menu_page * selmenu )
{
    static splicerfn splicers[] = {
        splicerfn_str,
        splicerfn_u32,
        splicerfn_02u32,
        splicerfn_x8,
        splicerfn_x32,
        splicerfn_bool,
        splicerfn_tinybool,
        splicerfn_idxchar,
        splicerfn_padding,
    };
    static char const *fmt_chr = "suvxXBbc@";
    char const *match;
    splicerfn selsplicer;

    if (line[0] != '%' || (match = strchr(fmt_chr, line[1])) == NULL)
        goto exit_normalcharacter;

#if FEATURE_DEBUG
    size_t fmt_chr_len = strlen(fmt_chr);
    size_t special_len = strlen(strchr(fmt_chr, 'c'));

    if ((size_t)(match - fmt_chr) < fmt_chr_len - special_len && selmenu->entrydata->fmtargs == NULL)
        goto exit_illegalspecifier;

    if (*match == 'c' && (selmenu->getterdata == NULL || selmenu->getterdata->children_char == NULL))
        goto exit_illegalspecifier;
#endif // FEATURE_DEBUG

    selsplicer = splicers[(size_t)(match - fmt_chr)];
    return 2 + call_splicer(line + 2, selmenu, selsplicer);

#if FEATURE_DEBUG
exit_illegalspecifier:
    {
        LOGERR("Exit: exit_illegalspecifier\n");
        LOGFERR("Menu: %u (%s)\n", menu_get_pageid(selmenu), selmenu->name);
        LOGFERR("Menu wants %.2s but doesn't supply data\n", line);
        buffer_append("%", 1);
        return 1;
    }
#endif // FEATURE_DEBUG

exit_normalcharacter:
    {
        buffer_append(line, 1);
        return 1;
    }
}

/* name:     parse_escapeseq
 * @lineoff: offset of line just after "%$"
 * @len:     available length of line segment
 * return:   number of bytes to jump forward
 * desc:     handles escape sequences starting with "%$". If no match is
 *           found, the %$ is printed literally and skipped
 */
static inline int
parse_escapeseq (
    char const * restrict lineoff ,
    size_t                len     )
{
    switch (*lineoff) {
        case 'i': return 1 + escseq_i(lineoff + 1, len - 1);
        case 'r': return 1 + escseq_r(lineoff + 1, len - 1);
    }

    buffer_append("%$", 2);
    return 0;
}

/* name:     putsfmt
 * @line:    line of text to parse and print
 * @selmenu: menu page pointer (contains fmtargs and children_char)
 * context:  called by putsvfmt
 * desc:     prints a single line from putsvfmt. if the line does not
 *           contain '%', the whole line is printed, else the line is
 *           parsed character by character by feeding offsets of it into
 *           parse
 *
 * 1. There used to be a \n appended, but escape sequence are for the
 *    beginning of the line when nothing has been printed yet, so
 *    skipping the line should also mean skipping the newline
 *
 * 2. the memcmp was removed on a recommendation
 */
static inline void
putsfmt (
    char const *       line    ,
    struct menu_page * selmenu )
{
    size_t lnlen = strlen(line);

    while (line[0] == '%' && line[1] == '$')
        SEEKEXPR(line, lnlen, 2 + parse_escapeseq(line + 2, lnlen - 2));

    if (escape_flags & ESCFLAG_SKIPLINE)
        goto exit_clearflags;

    if (memchr(line, '%', lnlen) == NULL)
        {
            buffer_append(line, lnlen);
            buffer_append("\n", 1);
            goto exit_clearflags;
        }

    while (*line != '\0')
        line += parse(line, selmenu);

    buffer_append("\n", 1);
    goto exit_clearflags;
    return;

exit_clearflags:
    {
#if FEATURE_COLOR
        static char const colorclear[] = ANSIESC_COLORCLR;

        if (escape_flags & ESCFLAG_REDTEXT)
            buffer_append(colorclear, sizeof colorclear);
#endif // FEATURE_COLOR

        escape_flags = 0;
        return;
    }
}

/* name:  buffer_append
 * @text: text to push
 * @len:  length of text to push
 * desc:  part of the optimization, this function will check if the
 *        buffer has space for text. If it does, the text is appended,
 *        otherwise, the buffer contents and the text to be written are
 *        dumped to stdout. This optimization works because it saves on
 *        unnecessary system calls / context switches
 */
static inline void
buffer_append (
    char const * text ,
    size_t       len  )
{
    if (buflen + len < PUTSVFMT_BUFSZ)
        {
            memcpy(buffer + buflen, text, len);
            buflen += len;
            return;
        }

    buffer_dump();
    fwrite(text, 1, len, stdout);
}

/* name:    buffer_dump
 * context: called by buffer_append when out of space, and by putsvfmt
 *          at the end
 * desc:    dumps buffer contents to stdout
 */
static inline void
buffer_dump ( void )
{
    fwrite(buffer, 1, buflen, stdout);
    buflen = 0;
}
