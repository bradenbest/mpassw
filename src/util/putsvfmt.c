/* putsvfmt stands for put string vector formatted. It follows that
 * putsfmt is the single-line variant
 *
 * Format specifiers are:
 *     '%' + letter [+ idx]
 *     or
 *     '%' + letter + start + '.' + end
 *
 * letter  type    printf fmt  expr or effect
 * s       string  %s          vfnret._str
 * u       u32     %u          vfnret._u32
 * v       u32     %02u        vfnret._u32
 * x       u32     %02x        vfnret._u32
 * X       u32     %08x        vfnret._u32
 * B       string  %s          boolstr(vfnret._u32)
 * b       char    %c          "-*"[!!(vfnret._u32)]
 * c       char    %c          selmenu->getterdata->children_char[idx]
 * @       none    padding     (used for alignment) prints <idx> spaces
 * $       none    none        escape sequence (see below)
 *
 * vfnret is populated via `selmenu->entrydata->fmtargs[idx](&vfnret);`
 * (see the macro xputsvfmt.c:GENERIC_SPLICERFN)
 *
 * if idx is blank, it's interpreted as 0. %s is the same as %s0
 * the second format specifies an inclusive range.
 * %b1.3 is the same as %b1%b2%b3
 *
 * putsvfmt does not check bounds. It's the responsibility of .text and
 * .fmtargs to be correctly set up
 *
 * Escape sequences:
 *     '%$' + letter
 *
 * letter  effect
 * i       skip the rest of this line if not interactive. Generally only
 *         useful for callback_exit, as that is the only way that
 *         putsvfmt can be reached while in quiet mode
 */
#include <stdio.h>
#include <string.h>

#include "menu/menu.h"
#include "menu/entryfn.h"
#include "menu/entrydata.h"
#include "menu/getterfn.h"
#include "menu/getterdata.h"
#include "menu/valuefn.h"
#include "ui/ui.h"
#include "util/util.h"

#define UTIL_PUTSVFMT_C
#include "util/putsvfmt.h"
#include "util/putsvfmt.static.c"

/* name:     putsvfmt
 * @text:    "vector" of strings to parse and print
 * @selmenu: menu page pointer
 * context:  called by entryfn_generic_putsvfmt
 * desc:     optimized formatted print. This function goes through every
 *           string in text and splices in values where there are format
 *           specifiers. The format specifiers are not like printf,
 *           instead, they are a single token % followed by one of the
 *           format characters, followed by a number referencing the
 *           index of a valuefn to call from selmenu->entrydata->fmtargs
 *           See comment at top of file for format details.
 *
 *           The optimization is that the contents to write are written
 *           to a buffer and then dumped all at once when the buffer is
 *           full and/or the function is finished, which reduces the
 *           number of system calls that have to be made from
 *           potentially thousands to just 1 or a few.
 */
void
putsvfmt (
    char const **      text    ,
    struct menu_page * selmenu )
{
    for (int i = 0; text[i] != NULL; ++i)
        putsfmt(text[i], selmenu);

    buffer_dump();
}
