#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "localization/util.h"

#define UTIL_UTIL_C
#include "util/util.h"

/* name:    u32_parse_number
 * @buffer: buffer containing string repr of number
 * @length: length of buffer
 * return:  number
 * context: used by getterfn_generic_u32 and
 *          getterfn_generic_char_or_u32
 * desc:    parses number as decimal or hexadecimal if there's a 0x
 *          prefix
 */
u32
u32_parse_number (
    char * buffer ,
    size_t length )
{
    if (length >= 2 && memcmp(buffer, "0x", 2) == 0)
        return strtou32(buffer + 2, length - 2, 16);

    return strtou32(buffer, length, 10);
}

/* name:    strtou32
 * @buffer: buffer to convert to u32
 * @len:    length of buffer
 * @radix:  radix to convert to (2..16)
 * return:  number converted from buffer
 * desc:    converts string to u32 at given radix
 */
u32
strtou32 (
    char const * buffer ,
    size_t       len    ,
    int          radix  )
{
    static char const *charset = "0123456789abcdef";
    u32 accum = 0;

#if FEATURE_DEBUG
    if (radix > 16 || radix < 2)
        goto exit_radixoutofbounds;
#else // FEATURE_DEBUG
    IGNORE_LABEL(exit_radixoutofbounds);
#endif // FEATURE_DEBUG

    for (size_t i = 0; i < len; ++i)
        {
            char const *match = memchr(charset, tolower(buffer[i]), radix);
            int digitvalue = match - charset;
            u32 temp;

            if (match == NULL)
                goto exit_gibberish;

            temp = radix * accum + digitvalue;

            if ((temp - digitvalue) / radix != accum)
                goto exit_overflow;

            accum = temp;
        }

    return accum;

exit_gibberish:
    return accum;

exit_overflow:
    {
        LOGERR("Number truncated due to overflow.\n");
        return accum;
    }

exit_radixoutofbounds:
    {
        LOGERR("Radix must be between 2 and 16 (inclusive).\n");
        return 9;
    }
}

/* name:   strcountdigits
 * @tok:   null-terminated string offset from menu texts
 * return: number of uninterrupted digits
 * desc:   counts digits until the first non-digit character
 */
size_t
strcountdigits ( char const *tok )
{
    size_t ndigits = 0;

    while (isdigit(tok[ndigits]))
        ++ndigits;

    return ndigits;
}

/* name:   boolstr
 * @value: 1 or 0 (or truthy or falsy value)
 * return: "True " or "False"
 * desc:   returns a string representing a bool. True has a space at the
 *         end for alignment reasons
 */
char const *
boolstr ( int value )
{
    static char const *out[2] = UTIL_BOOLSTR_TEXT;

    return out[!!value];
}

/* name:   clamp
 * @value: value to clamp
 * @min:   min value
 * @max:   max value
 * return: clamped value
 * desc:   clamps a value between min and max (inclusive)
 */
u32
clamp (
    u32 value ,
    u32 min   ,
    u32 max   )
{
    if (value < min)
        return min;

    if (value > max)
        return max;

    return value;
}
