#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>

#include "feature.h"
#include "version.h"
#include "util/types.h"
#include "util/util.h"
#include "menu/menu.h"
#include "menu/getterfn.h"
#include "menu/getterdata.h"
#include "menu/handlerfn.h"
#include "data/data.h"
#include "ui/ui.h"
#include "ui/arg.h"

#define MPASSW_C
#include "mpassw.h"
#include "mpassw.static.c"

/* name:    main
 * @argc:   arg count
 * @argv:   arg vector
 * return:  always 0
 * context: atexit, traps SIGINT, srand, may unset interactive flag,
 *          calls menu loop
 * desc:    Program entry point. This is only documented for
 *          consistency.
 */
int
main (
    int     argc ,
    char ** argv )
{
    atexit(callback_exit);
    signal(SIGINT, callback_sigint);

    if (!ui_isatty())
        ui_unsetflag(UIFLAG_INTERACTIVE);

#if FEATURE_ARGS
    for (int i = 1; i < argc; (void)0)
        {
            int retval = parse_arg(opts, argv + i);

            if (retval == 0)
                return 0;

            i += retval;
        }
#else // FEATURE_ARGS
    IGNORE_VARIABLE(argc);
    IGNORE_VARIABLE(argv);
#endif // FEATURE_ARGS

    srand(time(NULL));
    ui_menuloop();
    return 0;
}
