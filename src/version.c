#include <stdio.h>
#include <string.h>

#include "util/util.h"

#define VERSION_C
#include "version.h"
#include "version.static.c"

/* name:    version_str
 * return:  string literal or static buffer
 * context: depends on VERSION macro
 * desc:    If the VERSION macro is not provided at compilation, this
 *          function simply returns DEFAULT_STR. Otherwise, it loads the
 *          contents into an array and builds a string representation of
 *          the version. On error, DEFAULT_STR is returned.
 *
 * 1. VERSION is a comma-separated list defined in config.mk
 * 2. rewind over the trailing '.'
 * 3. if VERSION is not defined, the default DEFAULT_STR is used instead
 */
char const *
version_str ( void )
#ifdef VERSION
{
    static char buffer[BUFSZ];
    int version_number[END] = {VERSION}; // 1
    int nwritten;
    int buflen = 0;
    int loop_limit = SUBPATCH;

    if (version_number[SUBPATCH] == 0)
        {
            loop_limit = PATCH;

            if(version_number[PATCH] == 0)
                loop_limit = MINOR;
        }

    for (int i = 0; i <= loop_limit; ++i)
        {
            nwritten = populate_version_fragment(buffer + buflen, BUFSZ - buflen, version_number[i]);
            buflen += nwritten;

            if(nwritten == 0)
                goto exit_snprintffail;
        }

    if (buflen <= 0)
        goto exit_writeerror;

    --buflen; // 2
    buffer[MIN(buflen, BUFSZ - 1)] = '\0';
    return buffer;

exit_writeerror:
    {
        LOGFERRNO("Write error (buflen = %i)\n", buflen);
        return DEFAULT_STR;
    }

exit_snprintffail:
    return DEFAULT_STR;
}
#else
{
    IGNORE_FUNCTION(populate_version_fragment);

    return DEFAULT_STR; // 3
}
#endif
