# mpassw

A powerful, user-friendly, feature-complete, cross-platform commandline password generator for Metroid (NES).

![screenshot](screenshots/showcase.gif)

# Building

simply run make from the src directory

    $ cd src/
    $ make

## To build with minGW:

    $ make mingw

## To customize build features:

`src/config.mk` contains `$(FEATURES)`, which defines all the feature macros. To disable a feature, set it to 0.

As of v1.12, the version printout in the program will list the features that are available in a style similar to Vim,
that is, `+feature -feature -feature ...`.

## To customize language:

`src/config.mk` contains `$(LANGUAGE)` which defines `LANGUAGE_EN` by default. See `src/localization/lang/` for a list
of available languages, the corresponding macro is `LANGUAGE_<code>`. For example, to compile the Russian language
version, change the macro to `LANGUAGE_RU` and run `make`.

# Pre-builts and Online Snapshots

See the [releases](https://gitlab.com/bradenbest/mpassw/-/releases) page for pre-built binaries for Windows and Linux,
and runnable online snapshots.

# Older Snapshots:

* [1.2.1](https://replit.com/@BradenBest/mpassw121)
* [Simple Metroid Password Generator](https://replit.com/@BradenBest/SimpleMetroidPasswordGenerator)

# Windows and Mac

With MinGW, this program runs great on Windows.

I haven't tested this on Mac, but OS X is closer to Linux than Windows is, so it should work just fine.

# Usage

mpassw has a interactive command-line interface. Simply run `./mpassw` and follow the menus.

    $ ./mpassw
    or
    $ ./mpassw --help

...or double-click the exe and let explorer open a cmd window for it, if you're on Windows.

# Quiet mode (aka noninteractive mode)

I refer to this mode as noninteractive mode in all documentation before v1.10.2. Quiet mode and noninteractive mode
refer to the same thing.

Unless compiled without isatty support, quiet mode is triggered automatically when the input is not coming from a human.
Quiet mode behaves the same as normally, except the only text output in non-interactive mode is in the `Show Password`
and `Show Detailed Summary` menus, and anything emitted by arguments, and waitcontinue prompts ("Press enter to
continue") are skipped. Note that the `-p` argument will be silent if quiet mode is on.

You could, for example, make a file called `engageridley` full of the commands you would use to load and fix the
`ENGAGE RIDLEY MOTHER FUCKER` password:

    i
    ENGAGERIDLEYMOTHERFUCKER
    l
    t

And then pipe it

    $ cat engageridley | mpassw -q
    E7GAGE RIDLEY
    MOTHER FUCKEJ

You could make the file more readable, too.

    i (input password)
    ENGAGERIDLEYMOTHERFUCKER
    l (Location)
    t (Tourian)

Or you can mix it with arguments:

    $ echo -ne "loc\ntourian\n" | mpassw -q -p ENGAGERIDLEYMOTHERFUCKER
    E7GAGE RIDLEY
    MOTHER FUCKEJ

This forces quiet mode, loads the engage ridley password and fixes the location.

As of v1.10.2, the p command is not necessary. If quiet mode is set while the program is exiting, it will
generate a password with the current data and display it instead of the usual "Goodbye." (which doesn't show in
non-interactive mode anyway).

The `-q / --quiet` option isn't required, but it does guarantee quiet mode whereas the automatic detection does not. If
mpassw is compiled without isatty support, the automatic detection will always return 1.

As of v1.11, that sequence can now be done entirely with arguments

    $ mpassw --quiet --password=ENGAGERIDLEYMOTHERFUCKER --location=tourian --exit
    E7GAGE RIDLEY
    MOTHER FUCKEJ

Mind the order

# Contributing

## Credit
Contributors will have their name added to the `contributors` file, unless they request to be anonymous.

## Localization
Localization is welcome, and I tried to make it easy without requiring programming knowledge. See
[src/localization/lang/](src/localization/lang) for the existing translations. To make a new translation, simply copy an
existing translation, translate the text and save it with an appropriate name. The only things that should be translated
are the strings in the file between quotes `"..."`.

If you can pay attention to little details, that's great, but don't worry about it if
it's overwhelming. I can fix minor things like alignment and line width when integrating it. Adding your name in the
version printout (`text_intro`), like `"Klingon language translation by Dorpus Kringsley"` (written in klingon (if
you're translating to klingon)) is fine. Fantasy languages and conlangs are fine. Try to stick to two-letter langauge
codes when naming the file. If unsure, create an issue thread and I'll get back to you.

If you don't know how to use git, I suggest learning it so that you can submit a proper pull request. If you can't,
that's fine. You should be able to work with gitlab's web UI to fork, make the changes, and submit the PR.

For those who are familiar with working in a C/Makefile build environment, there is a make build target
`test-localization` in `src/test/` which will generate three files showing the output of the program. The language is
defined in `src/test/config.mk`.

    $ vim config.mk
    [set -DLANGUAGE_* appropriately]
    $ make -B test-localization
    $ less tlocalization-out-verbose

If you do this, don't forget to modify `src/localization/selectlang.h` to actually check for the new language.

## Programming
I'm not really looking for programmers. If you want to fork+commit+PR an improvement, go ahead, but I am strict about
code quality and will probably close PRs with a list of things to fix if I see too many things wrong with them. Be sure
to review `src/standards.md`
